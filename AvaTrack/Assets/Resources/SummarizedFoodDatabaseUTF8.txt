Food Name (German);Food Name;g / m;Energy, with dietary fibre;Energy, without dietary fibre;;Available carbohydrate, with sugar alcohols;Protein;Total fat;
;;;kJ;kJ;kcal;g;g;g;id
Algen, gekocht, entwässert;;g;;;42,75;0,4;8,4;0,8;2002
Algen, Nori, getrocknet;;g;;;236,92;2,3;46,7;4,3;2003
Alkoholisches Getränk, 30%, gekocht;;m;;;69,26;0,3;0;0;2004
Alkoholisches Getränk, Geist, etwa 40% v / v, alle (Brandy, Gin, Rum, Wodka und Whiskey);;m;;;204,68;0,1;0;0;2005
Amaranth, Getreide, ganz, ungekocht;;g;;;343,21;55,6;15,2;6,4;2006
Ananas aus der Dose;;g;;;58,59;13,53;0,4;0,3;2007
Apfel, getrocknet;;g;;;276,81;68;1,3;0,3;2008
Apfel;;g;;;49,34;12,19;0,3;0;2009
Aprikose, getrocknet;;g;;;195,61;44,4;4,3;0,2;2010
Aprikose;;g;;;36,06;7;0,8;0,1;2011
Artischocke Herz;;g;;;33,39;5,16;2,48;0,18;2012
Aubergine;;g;;;20,94;3,13;1,33;0,37;2013
Aufstrich, Haselnuss und Schokoladengeschmack;;g;;;516,6;57,8;6,8;30,3;2014
Aufstrich, Hefe, Marmite;;g;;;126,34;16,6;13,3;0,9;2015
Aufstrich, Hefe, vegemite;;g;;;149,75;10,9;24,1;0,9;2016
Auster;;g;;;65,32;0,6;10,45;2,25;2017
Avocado, roh;;g;;;123,96;0,2;1,6;13,2;2018
Backpulver;;g;;;86,22;19,8;0,8;0,3;2019
Bacon;;g;;;390,02;0,54;21,84;33,86;2020
Bambussprossen aus der Dose;;g;;;8,12;1,3;0,8;0;2021
Banane;;g;;;96,85;22,3;1,45;0,2;2022
Müsliriegel;;g;;;391,1;61,68;5,41;14,7;2023
Basilikum;;g;;;20,3;1,7;2;0,6;2024
Basa (Fisch);;g;;;94,18;0;18,63;2,1;2025
Ackerbohne;;g;;;43,83;2,3;7,15;0,5;2026
Limabohne;;g;;;209;40;11,6;0,5;2027
Bier;;m;;;32,77;1,96;0,34;0;2028
Birne;;g;;;49,02;12,2;0,4;0,01;2029
Brombeere;;g;;;38,69;7,5;1,4;0,3;2030
Blaubeeren;;g;;;40,6;9,9;0,5;0;2031
Blumenkohl;;g;;;22,69;3,35;1,95;0,2;2032
Bockshornkleesamen, getrocknete;;g;;;166,95;4,2;23;6,4;2033
Navy Bohne, getrocknet;;g;;;251,97;34,8;21,9;2,2;2034
Navy Bohne gekocht;;g;;;90,04;12,6;8,2;0,7;2035
Kidney Bohne, gekocht;;g;;;125,39;15,8;13,8;0,4;2036
Kidney Bohne, getrocknet;;g;;;253,64;35,6;22,5;1,8;2037
Bok Choy;;g;;;16,48;0,7;2,9;0,23;2038
Brassen, Filet, roh;;g;;;124,43;0;19,6;5,1;2039
Brokkoli, gekocht;;g;;;19,11;1,2;2,9;0,3;2040
Brokkoli roh;;g;;;24,12;0,4;4,7;0,3;2041
Brot, aus Roggenmehl, Sauerteig;;g;;;243,97;46,45;10;1,75;2042
Brot, aus Vollkornmehl;;g;;;248,81;40,93;12,4;3,75;2043
Brot, aus Weißmehl;;g;;;231,67;43,8;9,6;1,8;2044
Brötchen aus Vollkornmehl;;g;;;250,3;45,2;10,1;3;2045
Brötchen, aus Weißmehl;;g;;;246,95;45;9,4;3;2046
Brötchen, Mischkorn;;g;;;234,77;40,3;11,1;3;2047
Buchweizengrütze, in Wasser gekocht, ohne Zusatz von Salz;;g;;;68,07;12,3;3;0,7;2048
Buchweizengrütze, ungekocht;;g;;;320,52;58;14;3,2;2049
Büffelsteak;;g;;;125,45;0;24,33;3,03;2050
Bulgur, ungekocht;;g;;;327,44;66,1;10,3;2;2051
Cassava, geschält, frisch, roh;;g;;;131,36;30,4;1,1;0,2;2052
Chicorée, gekocht, entwässert;;g;;;14,81;0,9;2,1;0,2;2053
Chicorée, roh;;g;;;12,42;0,8;1,8;0,2;2054
Chili (Chili);;g;;;24,3;2,75;2,23;0,35;2055
Cidre;;m;;;50,39;5,7;0;0;2056
Polenta, gekocht;;g;;;98,88;20,4;2,6;0,6;2057
Polenta ungekocht;;g;;;310,72;64,1;8,1;2;2058
Couscous, gekocht, ohne Zusatz von Fett oder Salz;;g;;;158,11;32,7;6,1;0,1;2059
Couscous, ungekocht;;g;;;355,86;73,6;13,8;0,2;2060
Cranberry;;g;;;24,6;4,3;0,5;0,1;2061
Croissant;;g;;;353;35,6;10;19,1;2062
Curry Pulver;;g;;;192,26;2,8;14,3;14;2063
Datteln;;g;;;271,08;67,2;2;0,2;2064
Dill, frisch, roh;;g;;;34,39;2,5;3,4;1,2;2065
Dinkel, gekocht, ohne Zusatz von Fett oder Salz;;g;;;130,64;24,5;5;1,2;2066
Dinkel, ungekocht;;g;;;315,02;59,2;12;3;2067
Donut süß;;g;;;365,18;46,1;6,43;17,67;2068
Ei, Huhn, Eigelb, roh;;g;;;313,11;0,2;15,6;28,2;2069
Ei, Huhn, ganz, roh;;g;;;127,3;0,3;12,6;8,5;2070
Ei, Huhn, ganz, hartgekochte;;g;;;137,09;0,7;12,4;9,5;2071
Eis Vanillegeschmack;;m;;;103,42;11,3;2;5,8;2072
Endivie, frisch, roh;;g;;;9,79;0,4;1,5;0,2;2073
Ente gebacken;;g;;;343,8;0;20,8;29,35;2074
Ente roh;;g;;;120,85;0;17,8;5,5;2075
Erbsen gekocht;;g;;;48,24;6,4;4,8;0,4;2076
Erbse, grün, ganz, getrocknet;;g;;;282,06;40,9;24,1;2,1;2077
Erdbeere;;g;;;21,26;3,9;0,7;0,2;2078
Erdnussbutter;;g;;;599,95;11,85;23,25;51,95;2079
Cola Getränk;;m;;;51,11;12,8;0;0,3;2080
Cola Getränk Diät;;m;;;0;0;0;0;2081
Limonade;;m;;;35,35;9,2;0;0;2082
Essig (außer Balsamico-Essig);;m;;;15,76;0;0,1;0;2083
Fenchel;;g;;;20,18;3,6;1,1;0,1;2084
Fett, fest, auf Pflanzenölbasis;;g;;;884,17;0;0,2;100;2085
Feige;;g;;;40,36;8,1;1,4;0,3;2086
Feige getrocknet;;g;;;231,19;54,5;3,6;0,7;2087
Fingerflosser, Filet, roh;;g;;;129,93;0;19,4;5,8;2088
Fisch, Aal, roh;;g;;;177,93;0;18,4;11,7;2089
Fisch, weißes Fleisch, paniert, gefroren verpackt, gebacken, ohne Zusatz von Fett;;g;;;203,49;16,7;11,8;10;2090
Fischstäbchen;;g;;;218,77;16,6;12,1;11,6;2091
Forelle, Ozean, Aquakultur, Filet, ohne Haut, roh;;g;;;287,8;0;17,4;24,6;2092
Apfelsaft;;m;;;40,12;10,2;0;0;2093
Orangensaft;;m;;;43,71;10,8;0,2;0;2094
Frühlingsrolle, Fleisch-Gemüse-Füllung, Handels-, frittiert;;g;;;244,81;27,5;7,5;11,7;2095
Müsli;;g;;;323,43;62,06;10,92;3,26;2096
Haferflocken;;g;;;368;58,7;13,5;7;2097
Garnele;;g;;;94,68;0;21,32;0,9;2098
Blätterteig;;g;;;362,43;38,88;6,28;20,28;2099
Baked Beans;;g;;;72,84;11,3;4,9;0,3;2100
Gelatine;;g;;;346,07;0;84,4;0,4;2101
Königs-Escolar (Gemfish), Fleisch, roh;;g;;;77,14;0,1;13,2;2,6;2102
Gemischte Trockenfrüchte;;g;;;266,3;65,1;2,5;0,6;2103
Trinkschokolade;;m;;;93,86;11,15;3,95;3,9;2104
Granatapfel, geschält, roh;;g;;;66,4;13,5;1,9;0,2;2105
Grapefruit, geschält, roh;;g;;;26,51;4,8;0,9;0,2;2106
Grieß, gekocht, ohne Zusatz von Fett oder Salz;;g;;;96,73;19,6;3,4;0,4;2107
Grieß, ungekocht;;g;;;304,04;61,6;10,7;1,2;2108
Gurke;;g;;;10,63;1,68;0,75;0,08;2109
Hamburger;;g;;;230,59;20,2;13,6;10,6;2110
Hefe, trockenes Pulver;;g;;;217,34;6,3;36,4;5;2111
Heidelbeeren;;g;;;39,65;9,6;0,5;0;2112
Himbeere;;g;;;37,97;6,8;1,1;0,2;2113
Hirse, ungekocht;;g;;;349,89;67,4;11,2;3,5;2114
Honig;;m;;;450,2;117,4;0,4;0;2115
Hot Dog, Brötchen, Frankfurt & Sauce Füllung;;g;;;225,46;26,1;10,6;8,7;2116
Huhn, Brust, gekocht oder gegrillt;;g;;;159,14;0,12;29,08;4,6;2117
Huhn, Brust, mageres Fleisch, roh;;g;;;104,61;0;22,3;1,6;2118
Huhn, Hackfleisch, roh;;g;;;135,42;1,1;19,2;6;2119
Huhn, Leber, roh;;g;;;111,3;0;16,9;4,8;2120
Huhn, Nuggets;;g;;;244,09;18,03;13,57;13,1;2121
Hummer, Southern Rock, wild, Fleisch, gekauft gedünstet oder gekocht;;g;;;98,16;0;22,5;0,8;2122
Hummer, Southern Rock, wild, Fleisch, roh;;g;;;88,37;0;20,2;0,7;2123
Ingwer;;g;;;28,9;5,35;0,9;0,45;2124
Marmelade aus Pflaumen;;g;;;248,87;64,4;0,2;0;2125
Joghurt, Aprikosenstücke oder aromatisiert, regelmäßige Fett (3% Fett);;m;;;95,77;11,5;4,4;3,3;2126
Joghurt, natürlich, regelmäßig Fett (3% Fett);;m;;;75,71;5,2;5,3;3,3;2127
Johannisbeere, getrocknet;;g;;;267,26;65,1;2,8;0,5;2128
Kabeljau oder Seehecht, geräuchert, gebrüht;;g;;;94,82;0;20,1;1,5;2129
Kabeljau, geräuchert, roh;;g;;;85,03;0;18,8;1;2130
Cappuccino;;m;;;54,69;4,9;3;2,7;2131
Espresso;;m;;;9,55;0;1,7;0,3;2132
Kakaopulver;;g;;;259,37;10,5;20,5;15,2;2133
Kalbfleisch;;g;;;134,04;0;27,49;2,54;2134
Grünkohl;;g;;;38,93;5,2;4,3;0,1;2135
Limette;;g;;;23,53;0,8;0,45;1,45;2136
Kaninchen, Zucht, ganz, roh;;g;;;112,73;0;23,2;2,1;2137
Kapern, gebeizt, in Dosen, entwässert;;g;;;23,88;1,7;2,4;0,9;2138
Kardamomsamen;;g;;;241,7;34,4;10,8;6,7;2139
Karotte;;g;;;28,66;6,22;0,76;0,1;2140
Kartoffel roh;;g;;;63,05;12,8;2,3;0,1;2141
Kartoffel gekocht;;g;;;62,81;12,8;2,5;0,1;2142
Kartoffelchips;;g;;;369,98;39,18;5,06;21,48;2143
Pommes aus Kartoffeln;;g;;;314,43;35,7;4,8;16,9;2144
Käse, Brie;;g;;;358,73;0;18,2;31,5;2145
Käse, Camembert;;g;;;299,5;0;20,4;23,8;2146
Käse, Cheddar, natürlich, regelmäßiges Fett;;g;;;308;1,46;25;22,16;2147
Käse, Edamer;;g;;;360,64;0;28;27,2;2148
Käse, fetta (Feta);;g;1136;1136;271,32;0,4;16,6;22,3;548
Käse, haloumi;;g;1050;1050;250,78;1,8;21,3;17,1;549
Käse, Hütten;;g;529;529;126,34;1,8;15,4;5,7;550
Käse, Mozzarella;;g;1213;1213;289,71;0;22,2;22,1;551
Käse, Parmesan, frisch;;g;1690;1690;403,63;0;35,1;28,8;552
Käse, Parmesan, getrocknet, fein gerieben;;g;1949;1949;465,49;0;40,6;33,3;553
Käse, Quark;;g;665;665;158,82;2,7;10,4;11,8;554
Käse, Sahne, einfach;;g;1420;1420;339,14;0,2;8,5;34,2;555
Käse, Soja;;g;1238;1234;294,72;4,7;7;28;556
Käse, Ziege, fest;;g;1502;1502;358,73;0,8;18,6;31,3;557
Käse, Ziege, weich;;g;1194;1194;285,17;0,9;21,2;21,7;558
Waffel für Eis;;g;;;346,07;72,3;7,9;2,3;2149
Keks;;g;;;442,39;66,15;8,45;16,2;2150
Kichererbse, getrocknet;;g;;;285,17;38,5;19,7;5,5;2151
Kichererbse, in Dosen, entwässert;;g;;;98,4;13,3;6,3;2,1;2152
Kirsche;;g;;;56,84;12,9;0,8;0,2;2153
Kiwifruit;;g;;;54,93;11,1;1,2;0,5;2154
Knoblauch, geschält, frisch, roh;;g;;;92,43;10,2;6,1;2,8;2155
Knollensellerie, geschält, roh;;g;;;28,42;5;1,6;0,2;2156
Himbeere;;g;;;227,13;52,15;3,32;0,95;2157
Huhn, Brust, mageres Fleisch, Backwaren, ohne Zusatz von Fett;;g;;;152,14;0;29;3,9;2158
Kohl, weiä, roh;;g;;;17,2;2,8;1,3;0,1;2159
Kohl, Wirsing, roh;;g;;;16,96;2,3;1,7;0,1;2160
Kohlrabi, geschält, frisch, roh;;g;;;32,72;4,2;3,7;0,1;2161
Kokosnuss, Milch, Konserven, regelmääiges Fett;;m;;;151,18;1,7;1,4;15,7;2162
Kokosnuss, frisch, reif, Wasser oder Saft;;m;;;20,78;4,7;0,5;0,1;2163
Kokosnuss Fruchtfleisch;;g;;;317,89;3,3;3,8;32,8;2164
Känigsdorsch, Gelbschwanz, Aquakultur, Filet, mit Haut, roh;;g;;;204,92;0;22,9;12,7;2165
Kopfsalat, Eisberg, roh;;g;38;26;6,21;0,4;0,9;0,1;615
Koriander, frisch, Blätter und Stiele;;g;167;139;33,2;3,7;3,1;0,7;616
Koriandersamen, getrocknet, gemahlen;;g;1344;1009;240,98;8,4;12,4;17,8;617
Krabbe, Fleisch, gekauft gedünstet oder gekocht;;g;370;370;88,37;0;20,2;0,7;618
Kresse, roh;;g;110;80;19,11;0,8;2,9;0,4;619
Butterkuchen;;g;;;367,39;51,83;6,33;15,63;2166
Schokoladenkuchen;;g;;;394,41;40,86;6,04;23,8;2167
Kürbis;;g;;;34,73;6,37;1,78;0,27;2168
Kurkuma, getrocknet, gemahlen;;g;;;247,67;44,4;9,7;3,2;2169
Lachs;;g;;;247,43;0,69;20,86;18,13;2170
Lakritze, schwarz;;g;;;315,74;74,9;4,9;0,8;2171
Lammfleisch roh;;g;;;220;3;31;9;2172
Lasagne tiefgefroren;;g;;;132,55;11,8;6,6;6,6;2173
Lasagne, Rindfleisch, selbst gemacht;;g;;;172,44;15,2;10,4;7,7;2174
Lauch;;g;;;26,51;3,7;2,1;0,4;2175
Linse, rot, geschält, trocken;;g;;;295,2;45,7;23;1,9;2176
Linse, grän, geschält, trocken;;g;;;283,5;40,9;25,3;1,7;2177
Linsen;;g;;;289,47;41,8;25,5;1,9;2178
Lupinen;;g;;;276,33;10;41;7,9;2179
Litschi / Lychee;;g;;;68,31;16,2;1,1;0,1;2180
Mais;;g;;;81,68;12,75;3,7;1,85;2181
Maischips;;g;;;466,92;53;7,2;25,2;2182
Majonäse, fettarm;;m;;;138,52;28,5;0,5;2,9;2183
Majonäse, mehr als 70% Fett;;m;;;688,08;2,7;0,9;75,9;2184
Makkaroni und Käse, selbst gemacht, gekocht ungefällte Pasta, hausgemachte Käsesauce;;g;;;152,85;19,8;6,4;5,3;2185
Makrele;;g;;;158,98;0;22,9;7,47;2186
Mandarin, geschält, roh;;g;;;43,47;9,8;0,9;0;2187
Mandarin, in Sirup in Dosen, entwässert;;g;;;56,76;13,67;0,4;0,1;2188
Mango, geschält, roh;;g;;;54,22;13,4;0,4;0;2189
Maracuja, roh;;g;;;46,1;5,7;3;0,3;2190
Margarine;;g;;;603,64;0;0,67;68,01;2191
Marmelade, Beeren;;g;;;259,61;67,3;0,3;0;2192
Mehl, Dinkel;;g;;;335,32;59,1;18,3;2,4;2193
Mehl, Kichererbse;;g;;;285,17;38,5;19,7;5,5;2194
Mehl, Mais;;g;;;348,7;84,25;0,5;0,5;2195
Bittermelone;;g;;;4,78;0,2;0,9;0;2196
Honigmelone;;g;;;28,78;5,75;0,75;0,3;2197
Melone, Wassermelone, geschält, roh;;g;;;31,05;7,3;0,6;0;2198
Milch oder Joghurt Dessert, Schokolade, regelmäßiges Fett;;m;;;129,69;18,4;5,3;3,8;2199
Milch, Kuh, Dosen, gesüßt, kondensiert, regelmäßige;;m;;;419,39;71;10,7;11,9;2200
Milch;;m;;;56,44;5,33;3,63;2,35;2201
Milch, Pulver, Kuh, regelmäßiges Fett, ungestärkt;;g;;;508;43;25,3;27,3;2202
Milchfisch;;g;;;189,87;0;23,4;10,7;2203
Minze, frisch, roh;;g;;;39,89;4,4;3,1;1,1;2204
Mischgemüse, gekauft gefroren, Karotten, Mais und Erbsen / Bohnen, gekocht, entwässert;;g;;;33,44;5,9;1,8;0,3;2205
Babymöhre, gebacken;;g;;;27,94;6;0,8;0,1;2206
Muffin;;g;;;279,01;43,7;8,5;7,96;2207
Maulbeere;;g;;;28,9;4,3;2,2;0,2;2208
Meeräsche, roh;;g;;;131,12;0;19,2;6;2209
Muskat, getrocknet, gemahlen;;g;;;459,52;28,5;5,8;36,3;2210
Müsli, geröstete, hinzugefügt getrocknete Früchte und Nässe, ungestärkt;;g;;;378,55;56,6;8,9;13,3;877
Schaffleisch;;g;;;258,96;0;23,86;18,33;2211
Nudel, Soba, gekocht, entwässert;;g;;;105,56;21,5;3,6;0,4;894
Nudel, Soba, trocken;;g;;;348,94;71,2;11,9;1,3;895
Nudel, Weizen, frisch, getränkt, abgetropft;;g;;;141,87;26,5;6,3;1;896
Nudel, Weizen, Instant, gewürzt, trocken, ungekocht;;g;;;430,86;54;10,6;19,1;899
Reisnudeln, gekocht, entwässert;;g;;;116,07;26,1;2,5;0;900
Cashew, Nuss;;g;;;595,41;22,9;17;49,2;904
Erdnuss;;g;;;551,95;8,9;24,7;47,1;906
Nuss, Haselnuss, roh, ungesalzene;;g;;;622,16;5,1;14,8;61,4;909
Nuss, Macadamia, roh, ungesalzene;;g;;;708,38;4,5;9,2;74;913
Nuss, Mandel, mit Haut, roh, ungesalzene;;g;;;548,84;5,4;19,7;50,5;915
Mandelmehl;;g;;;596,37;5,1;20,5;55,8;916
Pekannuss;;g;;;694,05;4,9;9,8;71,9;917
Nuss, Pistazien, roh, ungesalzene;;g;;;589,92;15,8;19,7;50,6;918
Nuss, Walnuss, roh, ungesalzene;;g;;;681,39;3;14,4;69,2;919
Obstsalat;;g;;;45,78;10,67;0,63;0,1;2212
Octopus, gekocht, ohne Zusatz von Fett;;g;;;77,38;0;16,6;1,2;925
Octopus, roh;;g;;;69,74;0;14,9;1;926
Okra, gekocht, ohne Zusatz von Fett;;g;;;21,26;1,5;3,3;0,2;927
Okra, roh;;g;;;19,58;1,4;3,1;0,2;928
Öl Palme;;m;3404;3404;812,99;0;0;92;929
Öl, Baumwollsaat;;m;3404;3404;812,99;0;0;92;930
Öl, Copha;;m;3404;3404;812,99;0;0,2;91,9;931
Öl, Erdnuss;;m;3404;3404;812,99;0;0;92;932
Öl, Gemüse;;m;3404;3404;812,99;0;0;92;933
Öl, Leinsamen oder Leinsamen;;m;3403;3403;812,75;0;0;92;934
Öl, Macadamia;;m;3404;3404;812,99;0;0;92;935
Öl, Mandel;;m;3404;3404;812,99;0;0;92;936
Öl, Mischung aus einfach ungesättigten Pflanzenölen;;m;3404;3404;812,99;0;0;92;937
Öl, Mischung aus polyungesättigten Pflanzenölen;;m;3404;3404;812,99;0;0;92;938
Öl, Oliven;;m;3399;3399;811,8;0;0;91,9;939
Öl, Raps;;m;3404;3404;812,99;0;0;92;940
Öl, Reiskleie;;m;3404;3404;812,99;0;0;92;941
Öl, Senfsamen;;m;3404;3404;812,99;0;0;92;942
Öl, Soja;;m;3404;3404;812,99;0;0;92;943
Öl, Sonnenblumen;;m;3404;3404;812,99;0;0;92;944
Öl, Traubenkern;;m;3404;3404;812,99;0;0;92;945
Olive, grün oder schwarz, abgetropft;;g;856;837;199,9;1,8;2;20,5;946
Orange, geschält, roh;;g;175;155;37,02;8,2;1;0;947
Austern;;g;;;58,4;0,6;8,95;2,2;2213
Pancake;;g;;;221,4;30,7;7,5;7,6;953
Paniermehl, weiß;;g;;;353,48;64,8;14,2;3,8;954
Pasta, Maismehl (Mai) basierte, trocken;;g;;;325,77;68,3;7,5;2,1;959
Pasta, weißer Weizenmehl & Spinat, trocken;;g;;;355,86;73,1;11,9;1,3;961
Pasta, weißer Weizenmehl und Ei, trocken;;g;;;359,21;74,4;10,7;1,6;963
Pasta, weißer Weizenmehl, trocken;;g;;;342,25;68,5;12,7;1,4;968
Paste, Garnelen;;g;;;94,1;0;21;1;971
Curry Paste;;g;;;199,79;8,25;3,4;17,15;2214
Paste, Soja;;g;;;174,83;19,7;12,9;5,1;974
Pastinake;;g;;;54,61;11;1,97;0,2;2215
Pawpaw (Papaya), geschält, roh;;g;;;29,38;6,9;0,4;0,1;978
Petersilie;;g;;;13,37;0,5;2,15;0,2;2216
Pfeffer, schwarz, gemahlen;;g;;;227,61;38,6;10,4;3,3;984
Pfirsich, gelb, ungeschält, roh;;g;;;36,3;7,8;0,8;0,1;985
Pfirsich, in Dosen in Birnensaft;;g;;;41,56;9,5;0,6;0;987
Pilz;;g;;;24,84;1,8;3,15;0,55;2217
Pizza;;g;;;236,38;25,96;12,81;8,81;2218
Popcorn, kommerziellen, Butteraroma, gesalzen;;g;;;472,65;49,6;9,1;26,5;1013
Prosciutto;;g;;;292,33;0,3;30,8;18,8;1014
Prune (getrockneter Pflaume);;g;;;185,81;43,9;2,3;0,4;1015
Pudding;;g;;;281,43;45,37;3,7;10,07;2219
Quandong, Obst, Fleisch;;g;;;41,08;8,1;2,5;0;1020
Quinoa, ungekocht;;g;;;346,79;58,6;12,9;6,5;1024
Reis, braun, ungekocht;;g;;;339,86;69,1;7,9;3,1;1031
Reis, weiß, ungekocht;;g;;;338,19;74;7,3;0,9;1035
Reis, wild, roh;;g;;;347,74;68,7;14,7;1,1;1037
Rettich, rot gehäutet, ungeschält, roh;;g;;;12,66;1,9;0,8;0,2;1040
Rhabarber, Stängel, roh;;g;;;18,15;1,7;1,5;0,2;1042
Butternut Kürbis;;g;;;44,66;7,4;2,27;0,63;2220
Rindfleisch, roh;;g;;;121;0;18,6;2;2221
Roggen, Getreide, ganz, ungekocht;;g;;;277,05;51,3;11,4;2,6;1167
Roggenmehl;;g;;;288,99;54,9;11,4;2,3;1168
Rosenkohl, frisch, roh;;g;;;27,23;2,1;3,8;0,3;1171
Rosine;;g;;;283,74;68,7;3,3;0,5;1172
Rosmarin, getrocknet;;g;;;232,39;20,4;4,9;15,2;1174
Rote Beete;;g;;;44,12;9,13;1,85;0,1;2222
weiße Rübe;;g;;;20,78;3,7;1,4;0;1179
Saft, Apfel & Schwarze Johannisbeere, kommerzieller;;m;171;171;40,84;9,7;0,2;0;1182
Saft, Apfel, kommerzieller, Zusatz von Vitamin C;;m;127;126;30,09;7,4;0,2;0;1183
Saft, Kalk;;m;128;93;22,21;1,3;0,8;0,2;1184
Saft, Orange & Mango, kommerzielle;;m;152;150;35,83;8;0,6;0;1185
Saft, orange, kommerziellen;;m;117;115;27,47;5,7;0,8;0;1186
Saft, Zitrone;;m;120;99;23,64;1,9;0,6;0,2;1187
Sage, getrocknet;;g;;;238,36;20,4;10,6;12,8;1189
Sahne, verdickt, regelmäßiges Fett (ca. 35%);;m;;;346,55;2,5;2,1;37,2;1190
Samen, Chia, getrocknet;;g;1782;1507;359,92;7,7;14;30,7;1195
Samen, Kürbis, geschält und getrocknet;;g;2424;2376;567,47;2,9;30,2;49;1196
Samen, Leinsamen;;g;2067;1849;441,6;1,6;15,5;42,2;1197
Samen, Mohn;;g;2097;1941;463,58;8,6;15,3;41,6;1198
Samen, Sonnenblume;;g;2395;2308;551,23;2,2;22,7;51;1199
Sardelle, konserviert in öl, entwässert;;g;;;181,99;0;25,4;8,9;1200
Sardine;;g;;;105,8;0;20,85;2,4;2223
Sauce, Austern, kommerzielle;;m;574;571;136,37;25,6;3,4;1,2;1202
Sauce, Butter Huhn, kommerzielle;;m;465;448;107;6,6;1,5;8,5;1203
Sauce, Fisch, kommerzielle;;m;260;248;59,23;5,4;8,3;0,4;1204
Sauce, Grill, kommerzielle;;m;834;828;197,75;50,1;0,8;0,2;1205
Sauce, hoi sin (hoisin), kommerzielle;;m;996;906;216,38;40,2;1,8;5,8;1206
Sauce, Nudeln, Tomatenbasis, kommerzielle, erhitzten;;m;249;229;54,69;8,3;1,7;1,8;1208
Sauce, Pasta oder sieden, Gewerbe, fettarm;;m;271;259;61,86;7,5;0,9;3,3;1209
Sauce, Pasta, Basilikum-Pesto, kommerzielle;;m;1838;1824;435,63;8,5;6,1;42,7;1210
Sauce, Pasta, Bolognese, selbst gemacht mit Rinderhackfleisch und hausgemachte Tomatensauce;;g;347;340;81,2;1,9;9,5;4;1211
Sauce, Pasta, Käse oder Sahne-basierte, kommerzielle;;m;429;413;98,64;5,5;1,8;7,9;1212
Sauce, Pflaumen, kommerzielle;;m;1236;1229;293,53;72,9;0,2;1;1213
Sauce, Preiselbeeren, kommerzielle;;m;759;749;178,89;46,8;0;0;1214
Sauce, Rogan Josh, kommerzielle;;m;333;316;75,47;7;1,7;4,6;1215
Sauce, Salsa, Tomatenbasis, kommerziellen;;m;160;146;34,87;6,9;1,4;0;1216
Sauce, Soja, kommerzielle;;m;170;170;40,6;2,8;5,7;0;1218
Sauce, Tabasco, kommerzielle;;m;56;51;12,18;0,1;1,3;0,7;1220
Sauce, Tomate, kommerzielle;;m;454;443;105,8;25,1;1,4;0;1221
Sauce, weiß, herzhafte, hausgemachte;;m;801;800;191,07;12;4,2;14,3;1223
Sauce, Worcestershire, kommerzielle;;m;380;380;90,76;21,5;1,5;0,2;1224
Saure Sahne, leicht (ca. 18% Fett);;m;;;211,61;4,6;3,7;20,1;1226
Schalotten;;g;;;26,03;4,05;1,95;0,15;2224
Brownie, Schoko;;g;;;432,05;49,1;6,8;24,2;2225
Schinken, Bein, mager;;g;;;108,19;2;17;2,5;1235
Schnittlauch, roh;;g;;;19,82;2,6;1,5;0,3;1239
Schokolade, dunkle, hohe Kakaoanteil;;g;;;523,76;62,6;3,9;30,1;1240
Schokolade;;g;;;512,14;54,9;6,45;31,18;2226
Schweinefleisch, roh;;g;;;0;271;27;17;2227
Scone;;g;;;313,35;52,9;7,45;8;2228
Sellerie;;g;;;12,18;2,2;0,6;0,1;1334
Senf;;g;;;86;6;6;4;2229
Silber Barsch, Aquakultur, roh;;g;;;220,68;0;19,5;16;1339
Sirup, Ahorn, rein;;m;;;343,68;89,1;0,1;0,1;1342
Soft-Drink, Energy Drink, Red Bull;;m;;;45,62;11,3;0,1;0;1347
Soft-Drink, Tonic Water;;m;;;34,87;9,1;0;0;1349
Spaghetti in Tomaten-Käse-Sauce, Konserven;;g;;;52,54;10,7;2,5;0;1356
Spargel, grün, roh;;g;;;16,96;1,4;2,5;0,1;1360
Spargel, in Kochsalzlösung in Dosen, entwässert;;g;;;16,72;1,5;1,9;0,1;1361
Spargel, grün, gekocht, entwässert;;g;;;19,82;1,6;2,9;0,1;1359
Spinat, roh;;g;;;15,76;0,53;2,7;0,27;2230
Spinat, Wasser, frisch, roh;;g;;;21,02;1;2,9;0,5;1365
Steak & Gemüse, Konserven;;g;;;59,47;6,2;4,4;1,9;1375
Suppe, Instant-Trockenmischung;;g;;;337,38;62,54;7,38;6,24;2231
Süßkartoffel;;g;;;71,57;15,5;2,07;0,1;2232
Taco Schale, aus Maismehl, einfach;;g;;;448,53;56,3;6,8;21,8;1394
Taco Würzmischung, Chili-Basis;;g;;;222,35;39,7;5,9;4,1;1395
Tahini, Sesamsamen Zellstoff;;m;;;610,46;1;17,3;60,7;1396
Tamarillo, geschält, roh;;g;;;26,75;3,4;2;0,1;1397
Tamarinde, Paste, rein;;g;;;240,51;57,4;2,8;0,6;1398
Tangerine oder tangor, geschält, roh;;g;;;40,12;8,1;1;0,2;1400
Teekuchen, aus Weißmehl, geröstet;;g;;;196,56;41,6;5,1;0,8;1409
Thunfisch, Gelb, Fleisch, roh;;g;;;103,89;0;23,4;1;1413
Thunfisch, Gelb, Fleisch, gedünstet, ohne Zusatz von Fett;;g;;;123,72;0;27,9;1,2;1412
Thunfisch, Gelb, Fleisch, Backwaren, ohne Zusatz von Fett;;g;;;142,35;0;32,1;1,4;1411
Thymian, getrocknet, gemahlen;;g;;;211,61;26,9;9,1;7,4;1415
Tilapia, Filet, roh;;g;;;113,92;0;18,5;4,4;1418
Tintenfisch oder Calamari, roh;;g;;;78,34;0;16,7;1,2;1420
Tofu (Sojabohnenquark);;g;;;113,21;0;12;7,3;1421
Tomate;;g;;;15,29;3;0,6;0;1422
Tomate, Paste, mit Zusatz von Salz;;g;;;59,47;10,4;3,1;0,3;1424
Tomate, Püree, kommerzielles;;g;;;20,54;3,8;1,2;0,1;1426
Tomaten, getrocknet;;g;;;235,01;35,2;11,2;4,6;1428
Tomaten in Tomatensaft aus der Dose;;g;;;17,91;3;0,85;0,15;2233
Traube;;g;;;65,35;15,36;1,02;0,12;2234
Triticale, Getreide, ungekocht;;g;;;296,87;58,3;9,6;2,4;1437
Truthahn, roh;;g;;;117,03;0;21,6;3,3;1441
Vanille, künstliche oder Nachahmung;;m;;;55,17;14,4;0;0;1446
Vanilleschote Extrakt;;g;;;287,32;12,6;0,1;0,1;1447
Wax jambu, roh;;g;;;22,45;4,5;0,7;0,2;1454
Wein, rot;;m;;;78,82;0,67;0,23;0;2235
Wein, weiß;;m;;;71,89;0,2;0,3;0;1465
Weinblatt, Traube, Konserven;;g;;;41,56;1,8;4,3;2;1473
Weizen, ganz, ungekocht;;g;;;304,28;58,3;11,5;2,4;1476
Weizenkeime;;g;;;268,45;32,7;20,6;6,1;1477
Weizenkleie, unverarbeitet, ungekocht;;g;;;187,25;22,6;14,8;4,1;1478
Weizenmehl, Vollkorn, einfach;;g;;;305,47;58,6;12,1;2,1;1479
Wurst und Gemüse, Konserven;;g;;;65,92;7,8;4;2,1;1483
Wurst, Chorizo, ungekocht;;g;;;281,59;1,2;20,1;22,1;1485
Wurst, Huhn, einfach, gebraten, ohne Zusatz von Fett;;g;;;395,27;4,9;11,6;37,2;1486
Wurst, Lamm;;g;;;225,22;4,9;15,6;16,1;1488
Wurst, Rindfleisch, Roh;;g;;;235,73;2,9;14,5;18,7;1492
Wurst, Schweinefleisch, gebraten, ohne Zusatz von Fett;;g;;;256,03;4,5;16,5;19,4;1493
Wurst, vegetarisch;;g;;;193,22;7,53;19,33;9,57;2236
Yam, wild geerntet, gekocht;;g;;;66,63;12,6;3,2;0,3;1499
Ziegenfleisch, roh;;g;;;149;0;19,5;7,88;2237
Zimt, getrocknet, gemahlen;;g;;;138,29;27,5;4;1,2;1514
Zitrone, geschält, roh;;g;;;22,69;1,8;0,6;0,2;1515
Zucchini;;g;;;18,11;1,68;1,87;0,37;2238
Zucker;;g;;;379,6;99,24;0,04;0;2239
Zwiebel;;g;;;35,27;6,57;2,03;0,13;2240
Paprika;;g;;;43;6,4;1,3;0,5;2241
Brezel (Laugenbrezel);;g;;;51;51;8,9;5,4;2242
