﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenTransitions : MonoBehaviour
{
    private static float TRANSITION_SPEED = 0.15f;

    public GameObject panelToDisableButtons;

    void Start()
    {
        panelToDisableButtons.SetActive(false);
    }

    public float GetTransitionSpeed()
    {
        return TRANSITION_SPEED;
    }

    #region Page transitions up, down, left, right

    public void PageSwipeUp(GameObject page)
    {
        activePages.Add(page);
        StartCoroutine(DisableButtons());
        LeanTween.moveY(page, page.transform.position.y + Screen.height, TRANSITION_SPEED);
        NotifySwipeControlObservers();
    }

    public void PageSwipeDown(GameObject page)
    {
        if(activePages.Count > 0) activePages.RemoveAt(activePages.Count - 1);
        StartCoroutine(DisableButtons());
        LeanTween.moveY(page, page.transform.position.y - Screen.height, TRANSITION_SPEED);
        NotifySwipeControlObservers();
    }

    public void PageSwipeLeft(GameObject page)
    {
        StartCoroutine(DisableButtons());
        LeanTween.moveX(page, page.transform.position.x + Screen.width, TRANSITION_SPEED);
    }

    public void PageSwipeRight(GameObject page)
    {
        StartCoroutine(DisableButtons());
        LeanTween.moveX(page, page.transform.position.x - Screen.width, TRANSITION_SPEED);
    }

    #endregion

    #region Main Menu Page Transitions
    //The transitions for the main menu are different since
    //we can move more than one page at a time through the
    //navigation bar.
    public GameObject mainMenu;


    public void MainMenuPageSwipe(int numberOfPages)
    {
        StartCoroutine(DisableButtons());
        LeanTween.moveX(mainMenu, mainMenu.transform.position.x + numberOfPages * Screen.width, TRANSITION_SPEED);
    }

    #endregion

    #region Customization Page Transitions

    public GameObject customizationPage;

    public void CustomizationPageSwipeDown()
    {
        StartCoroutine(DisableButtons());
        LeanTween.moveY(mainMenu, mainMenu.transform.position.y - Screen.height, TRANSITION_SPEED);
        LeanTween.moveY(mainMenuNavigationBar, mainMenuNavigationBar.transform.position.y - Screen.height, TRANSITION_SPEED);
        LeanTween.moveY(customizationPage, customizationPage.transform.position.y - Screen.height, TRANSITION_SPEED);
    }

    public void CustomizationPageSwipeUp()
    {
        StartCoroutine(DisableButtons());
        LeanTween.moveY(mainMenu, mainMenu.transform.position.y + Screen.height, TRANSITION_SPEED);
        LeanTween.moveY(mainMenuNavigationBar, mainMenuNavigationBar.transform.position.y + Screen.height, TRANSITION_SPEED);
        LeanTween.moveY(customizationPage, customizationPage.transform.position.y + Screen.height, TRANSITION_SPEED);
    }

    #endregion

    IEnumerator DisableButtons()
    {
        panelToDisableButtons.SetActive(true);
        yield return new WaitForSeconds(TRANSITION_SPEED);
        panelToDisableButtons.SetActive(false);
    }

    #region Swipe Controls 

    //Only one page can use the swipe controls at a time.
    //They also get deactivated whenever a page is opened
    //that does not use swipe controls.
    public GameObject mainMenuNavigationBar;
    public SwipeControls[] swipeControlsObservers;

    //List with all open pages. If the list is empty we're in the main menu.
    public List<GameObject> activePages = new List<GameObject>();

    private void NotifySwipeControlObservers()
    {
        //Are we in the main menu?
        GameObject activePage = activePages.Count == 0 ? mainMenuNavigationBar : activePages[activePages.Count - 1];

        foreach (SwipeControls observer in swipeControlsObservers)
        {
            observer.UpdateSwipeControls(activePage);
        }
    }
    #endregion
}
