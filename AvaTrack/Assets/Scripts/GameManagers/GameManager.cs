﻿using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public XMLManager fileManager;
    public UserDataModel userDataModel;
    public NotificationManager notificationManager;
    public WeatherManager weatherManager;
    public LocationManager locationManager;
    public Challenges challenges;
    public Avatar avatar;
    public WinScreen winScreen;

    private string dateOfLastUse;
    private string currentDate;

    private User user;

    private void Start()
    {
        StartSession();
    }

    public void StartSession()
    {
        if (!fileManager.CheckExistDateOfLastUse())
        {
            challenges.SaveChallenges(); //initiate the challenges
            LoaderClass.Load(LoaderClass.Scene.NewGameScene);
        }
        else
        {
            LoadSavedData();
            SetUserSettings();

            bool isNewDay = CheckIfNewDay();
            if (isNewDay)
            {
                InitiateNewDay();
            }
            else
            {
                challenges.UpdateChallenges();
            }

            userDataModel.NotifyAll();
        }
    }

    #region LoadSavedData

    private void LoadSavedData()
    {
        userDataModel.SetUser(LoadUser());
        LoadAvatar();
        LoadDateOfLastUse();
    }

    private User LoadUser()
    {
        user = new User();

        if (fileManager.CheckExistUserSettings())
            user.settings = fileManager.LoadUserSettings();

        if (fileManager.CheckExistUserFoodRecord())
            user.userFoodRecord = fileManager.LoadUserFoodRecord();

        if (fileManager.CheckExistGoals())
            user.goals = fileManager.LoadGoals();

        if (fileManager.CheckExistUserExperience())
            user.userExperience = fileManager.LoadUserExperience();

        if (fileManager.CheckExistDrinksList())
            user.hydrationLevel = fileManager.LoadDrinksList();

        if (fileManager.CheckExistConsumedFoodLists())
            user.consumedFood = fileManager.LoadConsumedFoodLists();

        return user;
    }

    private void LoadDateOfLastUse()
    {
        DateOfLastUseStorable dateOfLastUseStorable = fileManager.LoadDateOfLastUse();
        dateOfLastUse = dateOfLastUseStorable.date;
    }

    private void LoadAvatar()
    {
        AvatarStorable avatar = fileManager.LoadAvatar();
        this.avatar.LoadAvatar(avatar);
    }

    #endregion

    private void SetUserSettings()
    {
        userDataModel.SetWeatherAndLocationData(user.settings.isWeatherFunctionEnabled, user.settings.isLocationFunctionEnabled);
        userDataModel.AdaptAspectRatio(user.settings.isStretchAspectRationOn);
        if (userDataModel.GetUserSettings().isNotificationEnabled)
            notificationManager.DetermineDailyNotifications();
    }

    private bool CheckIfNewDay()
    {
        currentDate = System.DateTime.UtcNow.ToLocalTime().ToString("ddMMyyyy");
        if (!fileManager.CheckExistDateOfLastUse())
            dateOfLastUse = currentDate;
        else
            dateOfLastUse = fileManager.LoadDateOfLastUse().date;
        bool isNewDay = (!currentDate.Equals(dateOfLastUse)) ? true : false;
        return isNewDay;
    }

    private void InitiateNewDay()
    {
        userDataModel.GetUserExperience().ResetDailyXPlimit();

        //Check if challenges were completed on the previous day of usage
        challenges.UpdateChallengesOnNewDay(user, dateOfLastUse, currentDate);
        challenges.SaveChallenges();
        if (challenges.AllChallengesFulfilled())
            winScreen.ActivateWinScreen();

        ResetDrinksAndFood();
    }

    private void ResetDrinksAndFood() 
    {
        userDataModel.GetConsumedFood().ResetConsumedFoodLists();
        userDataModel.GetHydrationLevel().ClearDrinks();
        fileManager.Save(userDataModel.GetConsumedFood());
        fileManager.Save(userDataModel.GetHydrationLevel());
        fileManager.Save(currentDate);
    }

    private int currentHour = System.DateTime.UtcNow.ToLocalTime().Hour;
    //Check the current time and notify UserDataModel about changes.
    //Initiate new day on midnight
    private void Update()
    {
        DateTime currentDateTime = System.DateTime.UtcNow.ToLocalTime();
        if (currentHour != currentDateTime.Hour)
            if (currentDateTime.Hour == 0)
            {
                StartSession();
            }
            else
            {
                userDataModel.NotifyAll();
            }
        currentHour = currentDateTime.Hour;
    }

}
