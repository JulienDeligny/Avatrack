﻿using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

//Storable classes need to be simple datastructures with no constructor nor methods.
#region Storable classes
[System.Serializable]
public class UserSettingsStorable
{
    public int[] mealTime;
    public bool isNotificationEnabled;
    public bool weatherFunctionIsEnabled;
    public bool locationFunctionIsEnabled;
    public string locationName;
    public bool isStretchAspectRatioOn;
}

[System.Serializable]
public class UserFoodRecordStorable
{
    public List<Food> createdFoodList;
    public List<Food> frequentlyConsumedFoodList;
    public List<Food> recentlyConsumedFoodList;
}

[System.Serializable]
public class ConsumedFoodStorable
{
    public List<Food> breakfastFoodList;
    public List<Food> lunchFoodList;
    public List<Food> dinnerFoodList;
}

[System.Serializable]
public class DrinksListStorable
{
    public List<Drink> list;
}

[System.Serializable]
public class GoalsStorable
{
    public int age;
    public float weight;
    public bool isMale;
    public bool customNutritionGoals;
    public bool customHydrationGoals;
    public int[] mealTime;
    public float hydrationGoal;
    public float calorieGoal;
    public float carbohydrateGoal;
    public float proteinGoal;
    public float fatGoal;
    public QuestionnaireData questionnaireData;
}

[System.Serializable]
public class AvatarStorable
{
    public int hairStyle;
    public float[] hairCol;

    public int eyeStyle;
    public float[] eyeCol;

    public int eyebrowStyle;

    public int noseStyle;

    public int mouthStyle;

    public int topStyle;
    public float[] topCol;

    public int pantsStyle;
    public float[] pantsCol;

    public int shoeStyle;
    public float[] shoeCol;

    public float[] skinCol;
}

[System.Serializable]
public class UserExperienceStorable
{
    public int userXP;
    public int remainingDailyXP;
}

[System.Serializable]
public class ChallengesStorable
{
    public List<int> challengeValues;
}

[System.Serializable]
public class DateOfLastUseStorable
{
    public string date;
}
#endregion

public class XMLManager : MonoBehaviour
{
    #region UserFoodRecord

    public UserFoodRecordStorable foodRecordStorable;

    public void Save(UserFoodRecord foodRecord)
    {
        foodRecordStorable = new UserFoodRecordStorable();
        foodRecordStorable.createdFoodList = foodRecord.createdFoodList.foodList;
        foodRecordStorable.frequentlyConsumedFoodList = foodRecord.frequentlyConsumedFoodList.foodList;
        foodRecordStorable.recentlyConsumedFoodList = foodRecord.recentlyConsumedFoodList.foodList;
        
        XmlSerializer serializer = new XmlSerializer(typeof(UserFoodRecordStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserFoodRecord.xml", FileMode.Create);
        serializer.Serialize(stream, foodRecordStorable);
        stream.Close();
    }

    public bool CheckExistUserFoodRecord()
    {
        return (File.Exists(Application.persistentDataPath + "/UserFoodRecord.xml")) ? true : false;
    } 

    public UserFoodRecord LoadUserFoodRecord()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(UserFoodRecordStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserFoodRecord.xml", FileMode.Open);
        foodRecordStorable = serializer.Deserialize(stream) as UserFoodRecordStorable;
        stream.Close();

        UserFoodRecord foodRecord = new UserFoodRecord();
        foodRecord.createdFoodList.foodList = foodRecordStorable.createdFoodList;
        foodRecord.frequentlyConsumedFoodList.foodList = foodRecordStorable.frequentlyConsumedFoodList;
        foodRecord.recentlyConsumedFoodList.foodList = foodRecordStorable.recentlyConsumedFoodList;
        return foodRecord;

    }
    #endregion

    #region ConsumedFoodLists
    public ConsumedFoodStorable consumedFoodStorable;

    public void Save(ConsumedFood consumedFood)
    {
        consumedFoodStorable = new ConsumedFoodStorable();
        consumedFoodStorable.breakfastFoodList = consumedFood.breakfastFoodList.foodList;
        consumedFoodStorable.lunchFoodList = consumedFood.lunchFoodList.foodList;
        consumedFoodStorable.dinnerFoodList = consumedFood.dinnerFoodList.foodList;
        //open new xml file
        XmlSerializer serializer = new XmlSerializer(typeof(ConsumedFoodStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/EatenFoodLists.xml", FileMode.Create);
        serializer.Serialize(stream, consumedFoodStorable);
        stream.Close();
    }

    public bool CheckExistConsumedFoodLists()
    {
        return (File.Exists(Application.persistentDataPath + "/EatenFoodLists.xml")) ? true : false;
    }

    public ConsumedFood LoadConsumedFoodLists()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ConsumedFoodStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/EatenFoodLists.xml", FileMode.Open);
        consumedFoodStorable = serializer.Deserialize(stream) as ConsumedFoodStorable;
        stream.Close();
        ConsumedFood consumedFood = new ConsumedFood();
        consumedFood.SetConsumedFoodList(consumedFoodStorable.breakfastFoodList, Meal.breakfast);
        consumedFood.SetConsumedFoodList(consumedFoodStorable.lunchFoodList, Meal.lunch);
        consumedFood.SetConsumedFoodList(consumedFoodStorable.dinnerFoodList, Meal.dinner);
        return consumedFood;

    }
    #endregion

    #region DrinkList
    public DrinksListStorable drinksStorable;

    public void Save(HydrationLevel hydrationLevel)
    {
        drinksStorable = new DrinksListStorable();
        this.drinksStorable.list = hydrationLevel.drinks;

        XmlSerializer serializer = new XmlSerializer(typeof(DrinksListStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/DrinksList.xml", FileMode.Create);
        serializer.Serialize(stream, drinksStorable);
        stream.Close();
    }

    public bool CheckExistDrinksList()
    {
        return (File.Exists(Application.persistentDataPath + "/DrinksList.xml")) ? true : false;
    }

    public HydrationLevel LoadDrinksList()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(DrinksListStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/DrinksList.xml", FileMode.Open);
        drinksStorable = serializer.Deserialize(stream) as DrinksListStorable;
        stream.Close();
        HydrationLevel hydrationLevel = new HydrationLevel();
        hydrationLevel.SetDrinkList(drinksStorable.list);

        return hydrationLevel;
    }

    #endregion

    #region Goals

    public GoalsStorable goalsStorable;

    public void Save(Goals goals)
    {
        goalsStorable = new GoalsStorable();
        goalsStorable.age = goals.age;
        goalsStorable.weight = goals.weight;
        goalsStorable.isMale = goals.isMale;
        goalsStorable.customNutritionGoals = goals.customNutritionGoalsActivated;
        goalsStorable.customHydrationGoals = goals.customHydrationGoalsActivated;
        goalsStorable.mealTime = goals.mealTime;
        goalsStorable.hydrationGoal = goals.hydrationGoal;
        goalsStorable.calorieGoal = goals.calorieGoal;
        goalsStorable.carbohydrateGoal = goals.carbohydrateGoal;
        goalsStorable.proteinGoal = goals.proteinGoal;
        goalsStorable.fatGoal = goals.fatGoal;
        goalsStorable.questionnaireData = goals.questionnaireData;

        XmlSerializer serializer = new XmlSerializer(typeof(GoalsStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Goals.xml", FileMode.Create);
        serializer.Serialize(stream, goalsStorable);
        stream.Close();
    }

    public bool CheckExistGoals()
    {
        return (File.Exists(Application.persistentDataPath + "/Goals.xml")) ? true : false;
    }

    public Goals LoadGoals()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(GoalsStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Goals.xml", FileMode.Open);
        goalsStorable = serializer.Deserialize(stream) as GoalsStorable;
        stream.Close();

        Goals goals = new Goals();
        goals.age = goalsStorable.age;
        goals.weight = goalsStorable.weight;
        goals.isMale = goalsStorable.isMale;
        goals.customNutritionGoalsActivated = goalsStorable.customNutritionGoals;
        goals.customHydrationGoalsActivated = goalsStorable.customHydrationGoals;
        goals.mealTime = goalsStorable.mealTime;
        goals.hydrationGoal = goalsStorable.hydrationGoal;
        goals.calorieGoal = goalsStorable.calorieGoal;
        goals.carbohydrateGoal = goalsStorable.carbohydrateGoal;
        goals.proteinGoal = goalsStorable.proteinGoal;
        goals.fatGoal = goalsStorable.fatGoal;
        goals.questionnaireData = goalsStorable.questionnaireData;
        return goals;
    }

    #endregion

    #region Avatar

    public void Save(AvatarStorable avatar)
    {
        //open new xml file
        XmlSerializer serializer = new XmlSerializer(typeof(AvatarStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Avatar.xml", FileMode.Create);
        serializer.Serialize(stream, avatar);
        stream.Close();
    }

    public bool CheckExistAvatar()
    {
        return (File.Exists(Application.persistentDataPath + "/Avatar.xml")) ? true : false;
    }

    public AvatarStorable LoadAvatar()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(AvatarStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Avatar.xml", FileMode.Open);
        AvatarStorable avatar = serializer.Deserialize(stream) as AvatarStorable;
        stream.Close();
        return avatar;
    }

    #endregion

    #region UserSettings
    UserSettingsStorable userSettingsStorable;

    public void Save(UserSettings userSettings)
    {
        userSettingsStorable = new UserSettingsStorable();
        userSettingsStorable.isNotificationEnabled = userSettings.isNotificationEnabled;
        userSettingsStorable.weatherFunctionIsEnabled = userSettings.isWeatherFunctionEnabled;
        userSettingsStorable.locationFunctionIsEnabled = userSettings.isLocationFunctionEnabled;
        userSettingsStorable.locationName = userSettings.locationName;
        userSettingsStorable.isStretchAspectRatioOn = userSettings.isStretchAspectRationOn;

        XmlSerializer serializer = new XmlSerializer(typeof(UserSettingsStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserSettings.xml", FileMode.Create);
        serializer.Serialize(stream, userSettingsStorable);
        stream.Close();
    }

    public bool CheckExistUserSettings()
    {
        return (File.Exists(Application.persistentDataPath + "/UserSettings.xml")) ? true : false;
    }

    public UserSettings LoadUserSettings()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(UserSettingsStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserSettings.xml", FileMode.Open);
        userSettingsStorable = serializer.Deserialize(stream) as UserSettingsStorable;
        stream.Close();

        UserSettings userSettings = new UserSettings();
        userSettings.isNotificationEnabled = userSettingsStorable.isNotificationEnabled;
        userSettings.isWeatherFunctionEnabled = userSettingsStorable.weatherFunctionIsEnabled;
        userSettings.isLocationFunctionEnabled = userSettingsStorable.locationFunctionIsEnabled;
        userSettings.locationName = userSettingsStorable.locationName;
        userSettings.isStretchAspectRationOn = userSettingsStorable.isStretchAspectRatioOn;
        return userSettings;
    }

    #endregion

    #region Challenges
    public void Save(ChallengesStorable challenges)
    {
        //open new xml file
        XmlSerializer serializer = new XmlSerializer(typeof(ChallengesStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Challenges.xml", FileMode.Create);
        serializer.Serialize(stream, challenges);
        stream.Close();
    }

    public bool CheckExistChallenges()
    {
        return (File.Exists(Application.persistentDataPath + "/Challenges.xml")) ? true : false;
    }

    public List<int> LoadChallenges()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(ChallengesStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Challenges.xml", FileMode.Open);
        ChallengesStorable challenges = serializer.Deserialize(stream) as ChallengesStorable;
        stream.Close();
        return challenges.challengeValues;
    }
    #endregion

    #region Date

    public DateOfLastUseStorable dateOfLastUseStorable;

    public void Save(string date)
    {
        //open new xml file
        dateOfLastUseStorable = new DateOfLastUseStorable();
        dateOfLastUseStorable.date = date;
        XmlSerializer serializer = new XmlSerializer(typeof(DateOfLastUseStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Date.xml", FileMode.Create);
        serializer.Serialize(stream, dateOfLastUseStorable);
        stream.Close();
    }

    public bool CheckExistDateOfLastUse()
    {
        return (File.Exists(Application.persistentDataPath + "/Date.xml")) ? true : false;
    }

    public DateOfLastUseStorable LoadDateOfLastUse()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(DateOfLastUseStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/Date.xml", FileMode.Open);
        dateOfLastUseStorable = serializer.Deserialize(stream) as DateOfLastUseStorable;
        stream.Close();
        return dateOfLastUseStorable;
    }

    #endregion

    #region UserExperience

    UserExperienceStorable userExperienceStorable;
    public void Save(UserExperience userExperience)
    {
        userExperienceStorable = new UserExperienceStorable();
        userExperienceStorable.userXP = userExperience.userXP;
        userExperienceStorable.remainingDailyXP = userExperience.remainingDailyXP;

        //open new xml file
        XmlSerializer serializer = new XmlSerializer(typeof(UserExperienceStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserExperience.xml", FileMode.Create);
        serializer.Serialize(stream, userExperienceStorable);
        stream.Close();
    }

    public bool CheckExistUserExperience()
    {
        return (File.Exists(Application.persistentDataPath + "/UserExperience.xml")) ? true : false;
    }

    public UserExperience LoadUserExperience()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(UserExperienceStorable));
        FileStream stream = new FileStream(Application.persistentDataPath + "/UserExperience.xml", FileMode.Open);
        userExperienceStorable = serializer.Deserialize(stream) as UserExperienceStorable;
        stream.Close();
        UserExperience userExperience = new UserExperience();
        userExperience.SetUserExperience(userExperienceStorable.userXP);
        userExperience.remainingDailyXP = userExperienceStorable.remainingDailyXP;
        return userExperience;
    }

    #endregion
}
