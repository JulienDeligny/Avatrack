﻿using UnityEngine;
using Unity.Notifications.Android;
using System;
using System.Linq;
using UnityEngine.UI;

public class NotificationManager : MonoBehaviour, IMacrosObserver, IMealTimeObserver
{
    public UserDataModel userDataModel;
    public Text scheduledNotifications;
    bool isNewSession = true;

    #region Update functions

    public void UpdateMacroValues()
    {
        DetermineDailyNotifications();
    }

    public void UpdateMealTime()
    {
        DetermineDailyNotifications();
    }

    #endregion

    public void DetermineDailyNotifications()
    {
        AndroidNotificationCenter.CancelAllNotifications();
        scheduledNotifications.text = "Geplante Benachrichtigungen: ";
        if (userDataModel.GetUserSettings().isNotificationEnabled)
        {
            if (isNewSession)
            {
                CreateNotificationChannel();
                isNewSession = false;
            }
            DetermineNotificationFor(Meal.breakfast);
            DetermineNotificationFor(Meal.lunch);
            DetermineNotificationFor(Meal.dinner);
        }
    }

    private void DetermineNotificationFor(Meal meal)
    {
        int endMealTime = DetermineEndOfMeal(meal);
        string germanTranslation = TranslateMealToGerman(meal);
        DateTime currentDateTime = DateTime.UtcNow.ToLocalTime();
        int currentHour = DateTime.UtcNow.ToLocalTime().Hour;

        if (currentHour < endMealTime)
        {
            if (userDataModel.GetConsumedFood().dinnerFoodList.foodList.Count() == 0)
            {
                scheduledNotifications.text += "\nBenachrichtigung um " + endMealTime;
                DateTime dateTime = new DateTime(currentDateTime.Year, currentDateTime.Month, currentDateTime.Day, endMealTime, 0, 0);
                string text = "Ein " + germanTranslation + " wäre jetzt gut.";
                SendNotification("Dein Avatar ist Hungrig", text, dateTime);
            }
        }
    }

    private int DetermineEndOfMeal(Meal meal)
    {
        Goals goals = userDataModel.GetUserGoals();
        int endMealTime;
        if (meal == Meal.breakfast) endMealTime = goals.mealTime[1];
        else if (meal == Meal.lunch) endMealTime = goals.mealTime[3];
        else endMealTime = goals.mealTime[5];
        return endMealTime;
    }

    private string TranslateMealToGerman(Meal meal)
    {
        Goals goals = userDataModel.GetUserGoals();
        string germanTranslation;
        if (meal == Meal.breakfast) germanTranslation = "Frühstück";
        else if (meal == Meal.lunch) germanTranslation = "Mittagessen";
        else germanTranslation = "Abendessen";
        return germanTranslation;
    }

    public void CreateNotificationChannel()
    {
        var c = new AndroidNotificationChannel()
        {
            Id = "channel_id",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
    }

    public void SendNotification(string title, string text, DateTime dateTime)
    {
        var notification = new AndroidNotification();
        notification.Title = title;
        notification.Text = text;
        notification.FireTime = dateTime;

        AndroidNotificationCenter.SendNotification(notification, "channel_id");
    }

}
