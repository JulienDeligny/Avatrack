﻿using UnityEngine;
using UnityEngine.UI;

public class XPviewPanel : MonoBehaviour, IUserExperienceObserver 
{
    public UserDataModel userDataModel;

    private static float REDUCTION_OF_INTERVAL = 0.11f;
    //[0 , 1] -> [0.11 , 0.89] so that the round progression bar fills uniformly

    public Text dailyExperiencePointsView;
    public Text levelView;
    public Text levelExperiencePointsView;
    public Text userExperiencePointsView;
    public Image progressionBar;

    private UserExperience userExperience;

    public void UpdateUserExperience()
    {
        this.userExperience = userDataModel.GetUserExperience();

        UpdateLevelView();
        UpdateExperiencePointsView();
    }

    private void UpdateLevelView()
    {
        int ExperiencePointsForCurrentLevel = CalculateExperiencePointsForCurrentLevel();
        progressionBar.fillAmount = MapIntervals(ExperiencePointsForCurrentLevel);
        this.levelView.text = userExperience.level.ToString();
        levelExperiencePointsView.text = ExperiencePointsForCurrentLevel + " / " + (userExperience.level * 100).ToString();
    }

    private void UpdateExperiencePointsView()
    {
        this.userExperiencePointsView.text = userExperience.userXP.ToString();
        this.dailyExperiencePointsView.text = (20 - userExperience.remainingDailyXP) + " / 20";
    }

    private int CalculateExperiencePointsForCurrentLevel()
    {
        return userExperience.userXP - (((userExperience.level - 1) * (userExperience.level)) / 2) * 100;
    }

    private float MapIntervals(int ExperiencePointsForCurrentLevel)
    {
        float basis = (userExperience.level * 100) / ((1 - REDUCTION_OF_INTERVAL) - REDUCTION_OF_INTERVAL);
        return (ExperiencePointsForCurrentLevel / basis) + REDUCTION_OF_INTERVAL;
    }
}
