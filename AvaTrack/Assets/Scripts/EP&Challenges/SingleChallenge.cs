﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleChallenge : MonoBehaviour
{
    public UserDataModel userDataModel;
    public Challenges challenges;

    public int numberOfDaysToAccomplish;
    public bool isConsecutiveChallenge;
    public int challengeValue;
    public int experiencePoints;
    //A single challenge can be a combination of different goals (different challenge types)
    public List<ChallengeType> types;

    public Image trophyImage;
    public Text numberOfTrophies;
    public Sprite trophyBlank;
    public Sprite trophyColored;
    public Image progressBarImage;
    public Text progressBarText;

    bool challengeGoalReached;
    public void UpdateSingleChallenge(int challengeValue, string savedDate, string date)
    {
        this.challengeValue = challengeValue;

        challengeGoalReached = true;
        foreach (ChallengeType type in types)
        {
            if (type == ChallengeType.calorie) challengeGoalReached = challengeGoalReached && challenges.calorieGoalReached;
            else if (type == ChallengeType.carbs) challengeGoalReached = challengeGoalReached && challenges.carbohydratesGoalReached;
            else if (type == ChallengeType.protein) challengeGoalReached = challengeGoalReached && challenges.proteinGoalReached;
            else if (type == ChallengeType.fat) challengeGoalReached = challengeGoalReached && challenges.fatGoalReached;
            else if (type == ChallengeType.water) challengeGoalReached = challengeGoalReached && challenges.hydrationGoalReached;
        }

        if (isConsecutiveChallenge)
        {
            CheckGoalForConsecutiveChallenge(savedDate, date);
        }
        else if (challengeGoalReached)
        {
            this.challengeValue++;
        }

        if (CheckIfChallengeCompleted())
            userDataModel.AddUserExperience(experiencePoints);

        UpdateView(this.challengeValue);
    }

    private void CheckGoalForConsecutiveChallenge(string savedDate, string date)
    {

        if (challengeGoalReached && CheckIfConsecutiveDays(savedDate, date))
        {
            this.challengeValue++;
        }
        else
        {
            this.challengeValue = this.challengeValue - (this.challengeValue % numberOfDaysToAccomplish);
        }
    }

    private bool CheckIfChallengeCompleted()
    {
        int valueCurrentCycle = (challengeValue % numberOfDaysToAccomplish);
        return valueCurrentCycle == 0 && challengeValue > 0;
    }

    public void UpdateView(int challengeValue)
    {

        if (challengeValue >= numberOfDaysToAccomplish)
        {
            trophyImage.sprite = trophyColored;
            numberOfTrophies.gameObject.SetActive(true);
            numberOfTrophies.text = ((challengeValue - (challengeValue % numberOfDaysToAccomplish)) / numberOfDaysToAccomplish).ToString();
        }

        float basis = numberOfDaysToAccomplish / (0.89f - 0.11f);
        progressBarImage.fillAmount = ((challengeValue % numberOfDaysToAccomplish) / basis) + 0.11f;

        progressBarText.text = "" + (challengeValue % numberOfDaysToAccomplish) + " / " + numberOfDaysToAccomplish + "\n Tage";
    }

    bool CheckIfConsecutiveDays(string oldDate, string newDate)
    {
        int oldDay = Convert.ToInt32(oldDate.Substring(0, 2));
        int oldMonth = Convert.ToInt32(oldDate.Substring(2, 2));
        int newDay = Convert.ToInt32(newDate.Substring(0, 2));
        int newMonth = Convert.ToInt32(newDate.Substring(2, 2));
        if (newDay == 1 && (newMonth - oldMonth) == 1)
        {
            if ((newMonth == 5 || newMonth == 7 || newMonth == 10 || newMonth == 12) && oldDay == 30) return true;
            else if (newMonth == 3)
            {
                int year = Convert.ToInt32(oldDate.Substring(4, 4));
                if (year % 4 == 0 && oldDay == 29) return true;
                else if (oldDay == 28) return true;
            }
            else
            {
                if (oldDay == 31) return true;
            }
        }
        else
        {
            if (newDay - oldDay == 1) return true;
        }
        return false;
    }
}