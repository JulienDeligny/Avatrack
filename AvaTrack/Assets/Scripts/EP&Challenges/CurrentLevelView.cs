﻿using UnityEngine;
using UnityEngine.UI;

public class CurrentLevelView : MonoBehaviour, IUserExperienceObserver
{
     //We need to map the interval [0 , 1] to the interval of the imported round progression bars.
     //BOTTOM_BOUNDARY_OF_INTERVAL is the point where the progression bar starts filling up.
     //Our new interval is [SHIFT_OF_INTERVAL , 1 - SHIFT_OF_INTERVAL]
    private static float SHIFT_OF_INTERVAL = 0.163f;

    public UserDataModel userDataModel;

    public Text level;
    public Image progressBar;

    private UserExperience userExperience;

    public void UpdateUserExperience()
    {
        userExperience = userDataModel.GetUserExperience();
        int XPforPrevLevel = ((userExperience.level - 1) * (userExperience.level)) / 2;
        float basis = (userExperience.level * 100) / ((1- SHIFT_OF_INTERVAL) - SHIFT_OF_INTERVAL);
        progressBar.fillAmount = ((userExperience.userXP - (XPforPrevLevel * 100)) / basis) + SHIFT_OF_INTERVAL;
        level.text = userExperience.level.ToString();
    }
}
