﻿using System.Collections.Generic;
using UnityEngine;

public class Challenges : MonoBehaviour
{
    public List<SingleChallenge> challenges;
    public XMLManager fileManager;

    private User user;

    public bool hydrationGoalReached, calorieGoalReached, carbohydratesGoalReached, proteinGoalReached, fatGoalReached;

    public void UpdateChallenges()
    {
        List<int> challengeValues = fileManager.LoadChallenges();
        int indexInChallengeValues = 0;
        foreach (SingleChallenge challenge in challenges)
        {
            challenge.UpdateView(challengeValues[indexInChallengeValues]);
            indexInChallengeValues++;
        }
    }

    public void UpdateChallengesOnNewDay(User user, string savedDate, string currentDate)
    {
        this.user = user;
        CheckHydrationGoalsOfPreviousDay();
        CheckNutritionGoalsOfPreviousDay();
        List<int> challengeValues = fileManager.LoadChallenges();
        int indexInChallengeValues = 0;
        foreach (SingleChallenge challenge in challenges)
        {
            challenge.UpdateSingleChallenge(challengeValues[indexInChallengeValues], savedDate, currentDate);
            indexInChallengeValues++;
        }
    }

    private void CheckHydrationGoalsOfPreviousDay()
    {
        float totalWaterDrank = 0;
        foreach (Drink drink in user.hydrationLevel.drinks)
            totalWaterDrank += drink.quantity;
        hydrationGoalReached = user.goals.CheckDailyWaterGoalReached(totalWaterDrank);
    }

    private void CheckNutritionGoalsOfPreviousDay()
    {
        float caloriesConsumed = user.consumedFood.GetConsumed(NutritionalValueType.calories);
        float carbohydratesConsumed = user.consumedFood.GetConsumed(NutritionalValueType.carbohydrates);
        float proteinConsumed = user.consumedFood.GetConsumed(NutritionalValueType.protein);
        float fatConsumed = user.consumedFood.GetConsumed(NutritionalValueType.fat);

        calorieGoalReached = user.goals.CheckDailyCalorieGoalReached(caloriesConsumed);
        carbohydratesGoalReached = user.goals.CheckDailyMacroGoalReached(carbohydratesConsumed, Macronutrient.carbohydrates);
        proteinGoalReached = user.goals.CheckDailyMacroGoalReached(proteinConsumed, Macronutrient.protein);
        fatGoalReached = user.goals.CheckDailyMacroGoalReached(fatConsumed, Macronutrient.fat);
    }

    public bool AllChallengesFulfilled()
    {
        return hydrationGoalReached && calorieGoalReached && carbohydratesGoalReached && proteinGoalReached && fatGoalReached;
    }

    public void SaveChallenges()
    {
        List<int> challengeValues = new List<int>();
        foreach (SingleChallenge challenge in this.challenges)
        {
            challengeValues.Add(challenge.challengeValue);
        }
        ChallengesStorable challenges = new ChallengesStorable();
        challenges.challengeValues = challengeValues;
        fileManager.Save(challenges);
    }
}

public enum ChallengeType
{
    calorie,
    carbs,
    protein,
    fat,
    water
}
