﻿public interface IGenderObserver
{
    void UpdateGender();
}

public interface IHydrationLevelObserver
{
    void UpdateHydrationLevel();
}

public interface IMacrosObserver
{
    void UpdateMacroValues();
}

public interface ISingleMacroObserver
{
    void UpdateScreenValues(float newNutritionalValue, float goal);
}

public interface IMealTimeObserver
{
    void UpdateMealTime();
}

public interface IObserver
{
    void UpdateView();
}

public interface IWeatherAndLocationObserver
{
    void UpdateWeatherAndLocation();
}

public interface IUserExperienceObserver
{
    void UpdateUserExperience();
}