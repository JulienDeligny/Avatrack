﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodListObserver : MonoBehaviour
{
    public List<Food> foodList;

    public virtual void UpdateList(List<Food> foodList) {
        this.foodList = foodList;
    }
}
