﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserDataModel : MonoBehaviour
{
    private User user;
    public XMLManager fileManager;
    public LocationManager locationManager;
    public WeatherManager weatherManager;
    public NotificationManager notificationManager;

    #region Observers
    //The following observers implement one or more Observer interfaces interface
    public GameObject[] macrosObservers;
    public GameObject[] genderObservers;
    public GameObject[] hydrationLevelObservers;
    public GameObject[] mealTimeObservers;
    public GameObject[] weatherAndLocationObservers;
    public GameObject[] userExperienceObservers;
    public Avatar avatar;

    //The following observers get the corresponding foodList when changes occur.
    //A lot of objects implementing the FoodListObservers use the same scripts 
    //so it is the job of the UserDataModel to provide them the right foodlist.
    public FoodListObserver createdFoodListObservers;
    public FoodListObserver breakfastFoodListObservers;
    public FoodListObserver lunchFoodListObservers;
    public FoodListObserver dinnerFoodListObservers;
    public FoodListObserver frequentlyUsedFoodListObservers;
    public FoodListObserver recentlyUsedFoodListObservers;

    #endregion

    #region Get Data

    public UserSettings GetUserSettings()
    {
        return user.settings;
    }

    public Goals GetUserGoals()
    {
        return user.goals;
    }

    public ConsumedFood GetConsumedFood()
    {
        return user.consumedFood;
    }

    public UserExperience GetUserExperience()
    {
        return user.userExperience;
    }

    public HydrationLevel GetHydrationLevel()
    {
        return user.hydrationLevel;
    }

    public UserFoodRecord GetUserFoodRecord()
    {
        return user.userFoodRecord;
    }

    public float GetTemperature()
    {
        return weatherManager.GetTemperature();
    }

    #endregion

    #region Set Data

    public void SetUser(User user)
    {
        this.user = user;
    }

    public void SetUserGoals(int age, float weight, bool isMale)
    {
        GetUserGoals().SetAgeWeightGender(age, weight, isMale);

        NotifyGenderObservers();
        NotifyMacrosObservers();
        NotifyHydrationListObservers();

        fileManager.Save(user.goals);
        fileManager.Save(user.settings);
    }

    public void SetUserGoals(QuestionnaireData questionnaireData)
    {
        GetUserGoals().SetQuestionnaireData(questionnaireData);

        NotifyMacrosObservers();
        NotifyHydrationListObservers();

        fileManager.Save(user.goals);
    }

    public void SetCustomNutritionGoals(float calorieGoal, float carbsGoal, float proteinGoal, float fatGoal)
    {
        GetUserGoals().SetCustomNutritionGoals(calorieGoal, carbsGoal, proteinGoal, fatGoal);

        fileManager.Save(user.goals);

        NotifyMacrosObservers();
    }

    public void SetHydrationGoal()
    {
        GetUserGoals().CalculateHydrationGoal();

        fileManager.Save(user.goals);

        NotifyHydrationListObservers();
    }

    public void SetHydrationGoal(bool isPregnant, bool isBreastFeeding)
    {
        GetUserGoals().CalculateHydrationGoal(isPregnant, isBreastFeeding);

        fileManager.Save(user.goals);

        NotifyHydrationListObservers();
    }

    public void SetCustomHydrationGoal(int hydrationGoal)
    {
        GetUserGoals().SetCustomHydrationGoals(hydrationGoal);

        fileManager.Save(user.goals);

        NotifyHydrationListObservers();
    }

    public void SetMealTime(int[] mealtime)
    {
        GetUserGoals().SetMealTime(mealtime);

        fileManager.Save(user.goals);

        NotifyMealTimeObservers();
        NotifyMacrosObservers();
    }

    public void SetWeatherAndLocationData(bool weatherFunctionIsEnabled, bool locationFunctionIsEnabled)
    {
        user.settings.isWeatherFunctionEnabled = weatherFunctionIsEnabled;
        user.settings.isLocationFunctionEnabled = locationFunctionIsEnabled;
        if (SceneManager.GetActiveScene().name.Equals("NewGameScene"))
        {
            if (weatherManager.locationName == null) weatherManager.locationName = "";
            fileManager.Save(user.settings);
        }
        else
        {
            if (weatherFunctionIsEnabled && locationFunctionIsEnabled)
            {
                StartCoroutine(FetchLocationAndWeather());
            }
            else if (weatherFunctionIsEnabled && !locationFunctionIsEnabled)
            {
                weatherManager.locationName = user.settings.locationName;
                weatherManager.FetchWeatherData(false);
            }
            NotifyWeatherAndLocationData();
            if (!weatherFunctionIsEnabled || (weatherFunctionIsEnabled && locationFunctionIsEnabled && locationManager.LocationDetectionWorked()) || (weatherFunctionIsEnabled && !locationFunctionIsEnabled && weatherManager.weatherAPICallWorked))
            {
                fileManager.Save(user.settings);
            }
            NotifyWeatherAndLocationData();
        }
    }

    private IEnumerator FetchLocationAndWeather()
    {
        yield return StartCoroutine(locationManager.StartLocationService());
        weatherManager.FetchWeatherData(true);
        NotifyWeatherAndLocationData();
    }

    public void SetNotificationSettings(bool isActivated)
    {
        GetUserSettings().isNotificationEnabled = isActivated;

        notificationManager.DetermineDailyNotifications();

        fileManager.Save(user.settings);
    }

    public void AddUserExperience(int points)
    {
        GetUserExperience().ProcessExperiencePoints(points, false);

        fileManager.Save(user.userExperience);

        NotifyXPmanagerObservers();
    }

    public void SetAvatar(AvatarStorable avatar)
    {
        fileManager.Save(avatar);
    }

    public void AdaptAspectRatio(bool isStretchOn)
    {
        GetUserSettings().AdaptAspectRatio(isStretchOn);

        fileManager.Save(user.settings);
    }

    #endregion

    #region Modify Lists
    public void AddToCreatedFoodList(Food food)
    {
        user.userFoodRecord.createdFoodList.Add(food);
        fileManager.Save(user.userFoodRecord);
        NotifyFoodListObservers(createdFoodListObservers, user.userFoodRecord.createdFoodList.foodList);
    }

    public void DeleteCreatedFood(Food food, GameObject foodComponent)
    {
        Destroy(foodComponent);
        user.DeleteCreatedFood(food);

        NotifyConsumedFoodObservers();
        NotifyMacrosObservers();

        fileManager.Save(user.userFoodRecord);
        fileManager.Save(user.consumedFood);
    }

    public void AddToConsumedFoodList(Food food, Meal meal)
    {
        user.userFoodRecord.RecordFood(food);
        user.Consume(food, meal);
        user.userExperience.ProcessExperiencePoints(2, true);

        NotifyMacrosObservers();
        NotifyConsumedFoodObservers();
        NotifyFoodListObservers(frequentlyUsedFoodListObservers, user.userFoodRecord.frequentlyConsumedFoodList.foodList);
        NotifyFoodListObservers(recentlyUsedFoodListObservers, user.userFoodRecord.recentlyConsumedFoodList.foodList);
        NotifyXPmanagerObservers();

        fileManager.Save(user.consumedFood);
        fileManager.Save(user.userFoodRecord);
        fileManager.Save(user.userExperience);
    }

    public void DeleteFromEatenFoodList(Food food, Meal meal, GameObject foodComponent)
    {
        Destroy(foodComponent);
        user.RemoveConsumed(food, meal);

        NotifyConsumedFoodObservers();
        NotifyMacrosObservers();

        fileManager.Save(user.consumedFood);
    }

    public void ClearConsumedFoodList(Meal meal)
    {
        user.consumedFood.ClearConsumedFoodList(meal);
        SaveAndNotifyConsumedFoodList();
    }

    public void SaveAndNotifyConsumedFoodList()
    {
        fileManager.Save(user.consumedFood);

        NotifyConsumedFoodObservers();
        NotifyMacrosObservers();
    }

    public void ModifyFood(Food food, Meal meal, float newQuantity)
    {
        GetConsumedFood().ModifyFood(food, meal, newQuantity);

        NotifyConsumedFoodObservers();
        NotifyMacrosObservers();

        fileManager.Save(user.consumedFood);
    }

    public void AddDrink(Drink drink)
    {
        user.Consume(drink);

        NotifyHydrationListObservers();

        fileManager.Save(user.hydrationLevel);
    }

    public void DeleteFromWaterList(Drink drink, GameObject drinkComponent)
    {
        Destroy(drinkComponent);
        user.RemoveConsumed(drink);

        NotifyHydrationListObservers();

        fileManager.Save(user.hydrationLevel);
    }

    #endregion

    #region Notifiers

    public void NotifyAll()
    {
        NotifyXPmanagerObservers();
        NotifyGenderObservers();
        NotifyMealTimeObservers();

        foreach (Food food in user.userFoodRecord.createdFoodList.foodList)
        {
            NotifyFoodListObservers(createdFoodListObservers, user.userFoodRecord.createdFoodList.foodList);
        }

        NotifyFoodListObservers(frequentlyUsedFoodListObservers, user.userFoodRecord.frequentlyConsumedFoodList.foodList);

        NotifyFoodListObservers(recentlyUsedFoodListObservers, user.userFoodRecord.recentlyConsumedFoodList.foodList);

        NotifyMacrosObservers();
        NotifyConsumedFoodObservers();

        NotifyMacrosObservers();
        NotifyHydrationListObservers();
        NotifyWeatherAndLocationData();
    }


    private void NotifyMacrosObservers()
    {
        foreach (GameObject observer in macrosObservers)
        {
            observer.GetComponent<IMacrosObserver>().UpdateMacroValues();
        }
    }

    private void NotifyConsumedFoodObservers()
    {
        NotifyFoodListObservers(breakfastFoodListObservers, user.consumedFood.breakfastFoodList.foodList);
        NotifyFoodListObservers(lunchFoodListObservers, user.consumedFood.lunchFoodList.foodList);
        NotifyFoodListObservers(dinnerFoodListObservers, user.consumedFood.dinnerFoodList.foodList);
    }

    private void NotifyFoodListObservers(FoodListObserver foodListObservers, List<Food> foodList)
    {
        foodListObservers.UpdateList(foodList);
    }

    private void NotifyHydrationListObservers()
    {
        foreach (GameObject elem in hydrationLevelObservers)
        {
            elem.GetComponent<IHydrationLevelObserver>().UpdateHydrationLevel();
        }
    }

    private void NotifyGenderObservers()
    {
        foreach (GameObject observer in genderObservers)
        {
            observer.GetComponent<IGenderObserver>().UpdateGender();
        }
    }

    private void NotifyXPmanagerObservers()
    {
        foreach (GameObject observer in userExperienceObservers)
        {
            observer.GetComponent<IUserExperienceObserver>().UpdateUserExperience();
        }
    }

    private void NotifyMealTimeObservers()
    {
        foreach (GameObject observer in mealTimeObservers)
        {
            observer.GetComponent<IMealTimeObserver>().UpdateMealTime();
        }
    }

    private void NotifyWeatherAndLocationData()
    {
        foreach (GameObject observer in weatherAndLocationObservers)
        {
            observer.GetComponent<IWeatherAndLocationObserver>().UpdateWeatherAndLocation();
        }
    }

    #endregion
}

