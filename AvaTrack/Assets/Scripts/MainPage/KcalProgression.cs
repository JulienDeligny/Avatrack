﻿using System;
using UnityEngine;
using UnityEngine.UI;

//View of the progess bars of the calories
public class KcalProgression : MonoBehaviour, ISingleMacroObserver
{
    public UserDataModel userDataModel;

    private static float REDUCTION_OF_INTERVAL = 0.11f;
    //[0 , 1] -> [0.11 , 0.89]

    public Image progressBar;
    public Text caloriesCounter;
    public Sprite[] progressionIndicatorSprite;
    public Image progressIndicator;
    public Color lightRed;
    public Color lightGreen;

    private float newNutritionalValue;
    private float goal;

    public void UpdateScreenValues(float newNutritionalValue, float goal)
    {
        this.newNutritionalValue = newNutritionalValue;
        this.goal = goal;

        caloriesCounter.text = RoundAndFormatRemainingCalories();
        progressBar.fillAmount = CalculateFillAmount();

        SetProgressIndicator();
    }

    private string RoundAndFormatRemainingCalories()
    {
        int remainingCaloriesRounded = Convert.ToInt32(goal - newNutritionalValue);
        return remainingCaloriesRounded.ToString("N0");
    }

    private float CalculateFillAmount()
    {
        float basis = goal / ((1 - REDUCTION_OF_INTERVAL) - REDUCTION_OF_INTERVAL);
        return (newNutritionalValue / basis) + REDUCTION_OF_INTERVAL;
    }

    private void SetProgressIndicator()
    {
        int progressIndicator = userDataModel.GetUserGoals().CheckCurrentCalorieGoalReached(newNutritionalValue);
        if (progressIndicator < 0)
        {
            this.progressIndicator.color = lightRed;
            this.progressIndicator.sprite = progressionIndicatorSprite[0];
        }
        else if (progressIndicator > 0)
        {
            this.progressIndicator.color = lightRed;
            this.progressIndicator.sprite = progressionIndicatorSprite[2];
        }
        else
        {
            this.progressIndicator.color = lightGreen;
            this.progressIndicator.sprite = progressionIndicatorSprite[1];
        }
    }
}
