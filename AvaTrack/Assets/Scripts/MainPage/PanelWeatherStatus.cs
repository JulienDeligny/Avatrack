﻿using UnityEngine;
using UnityEngine.UI;

public class PanelWeatherStatus : MonoBehaviour, IWeatherAndLocationObserver
{
    public WeatherManager weatherManager;
    public LocationManager locationManager;
    public UserDataModel userDataModel;
    public WeatherPanel weatherPanel;

    public Text statusText;

    public void UpdateWeatherAndLocation()
    {
        bool weatherEnabled = (userDataModel.GetUserSettings().isWeatherFunctionEnabled);
        bool localizationFailed = (userDataModel.GetUserSettings().isLocationFunctionEnabled && !locationManager.LocationDetectionWorked());
        bool fetchWeatherDataFailed = (!userDataModel.GetUserSettings().isLocationFunctionEnabled && !weatherManager.weatherAPICallWorked);
        if (weatherEnabled)
        {
            if (localizationFailed || fetchWeatherDataFailed)
            {
                weatherPanel.gameObject.SetActive(false);
                gameObject.SetActive(true);
                statusText.text = weatherManager.weatherStatusText;
            }
            else
            {
                weatherPanel.gameObject.SetActive(true);
                gameObject.SetActive(false);
                statusText.text = "";
            }
        } 
        else
        {
            weatherPanel.gameObject.SetActive(false);
            gameObject.SetActive(false);
            statusText.text = "";
        }
    }
}
