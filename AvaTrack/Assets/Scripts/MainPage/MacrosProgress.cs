﻿using UnityEngine;
using UnityEngine.UI;

//View of the progess bars of the macronutrients
public class MacrosProgress : MonoBehaviour, ISingleMacroObserver
{
    public UserDataModel userDataModel;

    public int indexInKcalAndMacroList;
    public Macronutrient macronutrient;
    public bool addNewLine;

    public Image progressBar;
    public Text macroCounter;

    public Sprite arrowUp;
    public Sprite checkMark;
    public Sprite arrowDown;
    public Image progressIndicator;

    public Color lightRed;
    public Color lightGreen;

    private float newNutritionalValue;
    private float goal;

    public void UpdateScreenValues(float newNutritionalValue, float goal)
    {
        this.newNutritionalValue = newNutritionalValue;
        this.goal = goal;

        macroCounter.text = FormatProgressString();
        progressBar.fillAmount = newNutritionalValue / goal;
        SetProgressIndicator();
    }

    private string FormatProgressString()
    {
        string nutritionalValueRounded = Mathf.Floor(newNutritionalValue).ToString();
        string goalRounded = Mathf.Floor(goal).ToString();
        string newLine = addNewLine ? "\n " : "";
        return nutritionalValueRounded + " / " + newLine + goalRounded + " g";
    }

    private void SetProgressIndicator()
    {
        float currentlyConsumedOfMacronutrient = userDataModel.GetConsumedFood().totalBalanceOfNutritionalValues[indexInKcalAndMacroList];
        int progressIndicator = userDataModel.GetUserGoals().CheckCurrentMacroGoalReached(currentlyConsumedOfMacronutrient, macronutrient);
        if (progressIndicator < 0)
        {
            this.progressIndicator.color = lightRed;
            this.progressIndicator.sprite = arrowDown;
        }
        else if (progressIndicator > 0)
        {
            this.progressIndicator.color = lightRed;
            this.progressIndicator.sprite = arrowUp;
        }
        else
        {
            this.progressIndicator.color = lightGreen;
            this.progressIndicator.sprite = checkMark;
        }
    }
}
