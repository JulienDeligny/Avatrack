﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIColorButton : MonoBehaviour , IPointerClickHandler
{

    public EyeColorPicker colorPicker;

    public void OnPointerClick(PointerEventData eventData)
    {
        colorPicker.ChangeEyeColor(this.GetComponent<Image>().color);
    }
}
