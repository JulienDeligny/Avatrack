﻿using UnityEngine;

public class CustomizationPage : MonoBehaviour
{
    public ScreenTransitions transitions;
    public Avatar avatar;
    public TabMan_Custom tabMan;

    public void OnPageEnter()
    {
        avatar.ZoomIn();
        avatar.FaceCamera();
        transitions.CustomizationPageSwipeDown();
        tabMan.SelectFirstTab();

    }

    public void OnPageExit()
    {
        avatar.ZoomOut();
        avatar.UpdateCondition();
        transitions.CustomizationPageSwipeUp();
        avatar.SaveAvatar();
    }
}
