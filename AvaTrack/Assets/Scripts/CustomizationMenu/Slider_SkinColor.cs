﻿using UnityEngine;
using UnityEngine.UI;

public class Slider_SkinColor : MonoBehaviour
{
    private static float RED_VALUE_MIN_VALUE = 0.494f;
    private static float RED_VALUE_MAX_VALUE = 1;
    private static float BLUE_VALUE_MIN_VALUE = 0.372f;
    private static float BLUE_VALUE_MAX_VALUE = 0.878f;
    private static float GREEN_VALUE_MIN_VALUE = 0.301f;
    private static float GREEN_VALUE_MAX_VALUE = 0.6f;

    public Avatar character;
    public Color col;
    public Slider slider;

    public void ChangeSkinColor()
    {
        //The red, blue and green values for the skin color are reduced to the following intervals
        float red = (slider.value / (1 / (RED_VALUE_MAX_VALUE - RED_VALUE_MIN_VALUE))) + RED_VALUE_MIN_VALUE;
        float blue = (slider.value / (1 / (BLUE_VALUE_MAX_VALUE - BLUE_VALUE_MIN_VALUE))) + BLUE_VALUE_MIN_VALUE;
        float green = (slider.value / (1 / (GREEN_VALUE_MAX_VALUE - GREEN_VALUE_MIN_VALUE))) + GREEN_VALUE_MIN_VALUE;
        col = new Color(red, blue, green, 1);

        character.ChangeSkinColor(col);
    }
}
