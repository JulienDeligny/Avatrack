﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyeColorPicker : MonoBehaviour
{
    public EyesController eyes;
    public List<Color> eyeColors;
    public List<Image> UIEyeColors;

    void Start()
    {
        for (int i = 0; i < eyeColors.Count; i++)
        {
            UIEyeColors[i].color = eyeColors[i];
        } 
    }

    public void ChangeEyeColor(Color color)
    {
        eyes.ChangeColor(color);
    }
}
