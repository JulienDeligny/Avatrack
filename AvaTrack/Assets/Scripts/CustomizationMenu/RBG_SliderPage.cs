﻿using UnityEngine;
using UnityEngine.UI;

public class RBG_SliderPage : MonoBehaviour
{
    public ChangebleColor objectWithChangeableColor;

    public Slider[] rbg;

    public void UpdateColor()
    {
        Color newCol = new Color(rbg[0].value, rbg[1].value, rbg[2].value, 1);
        objectWithChangeableColor.ChangeColor(newCol);
    }
}
