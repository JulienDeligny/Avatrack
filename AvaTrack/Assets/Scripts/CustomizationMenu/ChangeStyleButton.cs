﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeStyleButton : MonoBehaviour, IPointerClickHandler
{
    public int direction;

    public GameObject swapableObject;

    public void OnPointerClick(PointerEventData eventData)
    {
        swapableObject.GetComponent<ISwapable>().ChangeStyle(direction);
    }
}
