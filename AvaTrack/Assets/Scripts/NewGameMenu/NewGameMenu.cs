﻿using UnityEngine;
using UnityEngine.UI;

public class NewGameMenu : MonoBehaviour
{
    public UserDataModel userDataModel;
    public XMLManager fileManager;

    public int pageNumber;
    public Button previousPage;

    public ScreenTransitions transitions;
    public InputPage userDataPage;

    #region New Game Scene Pages
    public TabMan_TopBar tabManagerForNutritionalGoals;
    public QuestionnaireGoalsPage questionnairePage;
    public TabButton questionnairePageButton;
    public HydrationOptionPage hydrationPage;
    public CustomGoalsPage customGoalsPage;
    public ChangeMealTimePage mealTimePage;
    public WeatherAndLocationOptionPage optionPage;
    public TabMan_Custom tabMan_CustomizationMenu;
    #endregion

    public Avatar avatar;

    public void Start()
    {
        userDataModel.SetUser(new User());
        avatar.ChangeSkinColor(avatar.skinColor);
        pageNumber = 1;
        previousPage.gameObject.SetActive(false);
    }

    //Changes page if the user input is valid
    public void OnChangePage(bool nextPageButtonPressed)
    {
        if (pageNumber == 2 && nextPageButtonPressed)
        {
            userDataPage.OnSubmit();
            if (userDataPage.isValid)
            {
                OnChangePageHelper(nextPageButtonPressed);
            }
        } else if (pageNumber == 3 && nextPageButtonPressed)
        {
            if (tabManagerForNutritionalGoals.selectedTab.Equals(questionnairePageButton))
            {
                questionnairePage.OnSubmit();
                if (questionnairePage.isValid)
                {
                    OnChangePageHelper(nextPageButtonPressed);
                }
            } else
            {
                customGoalsPage.OnSubmit();
                if (customGoalsPage.isValid)
                {
                    OnChangePageHelper(nextPageButtonPressed);
                }
            }
        } else if (pageNumber == 4 && nextPageButtonPressed)
        {
            hydrationPage.OnSubmit();
            if (hydrationPage.isValid)
            {
                OnChangePageHelper(nextPageButtonPressed);
            }
        }
        else if (pageNumber == 6 && nextPageButtonPressed)
        {
            mealTimePage.OnSubmit();
            if (mealTimePage.isValid)
            {
                OnChangePageHelper(nextPageButtonPressed);
            }
        } else if (pageNumber == 7 && nextPageButtonPressed)
        {
            optionPage.OnSubmit();
            OnChangePageHelper(nextPageButtonPressed);
        }
        else if (pageNumber == 9 && nextPageButtonPressed)
        {
            avatar.SaveAvatar();
            string currentDate = System.DateTime.UtcNow.ToLocalTime().ToString("ddMMyyyy");
            fileManager.Save(currentDate);
            LoaderClass.Load(LoaderClass.Scene.MainScene);
        }
        else
        {
            OnChangePageHelper(nextPageButtonPressed);
        }
    }

    void OnChangePageHelper(bool nextPage)
    {
        if (nextPage)
        {
            transitions.PageSwipeRight(gameObject);
            pageNumber++;
        }
        else
        {
            transitions.PageSwipeLeft(gameObject);
            pageNumber--;
        }
        if (pageNumber == 1)
        {
            previousPage.gameObject.SetActive(false);
        }
        else
        {
            previousPage.gameObject.SetActive(true);
        }
        if (pageNumber == 4)
        {
            hydrationPage.OnPageOpen();
        }
        if (pageNumber == 9)
        {
            avatar.ZoomIn();
            tabMan_CustomizationMenu.SelectFirstTab();
        }
    }
}
