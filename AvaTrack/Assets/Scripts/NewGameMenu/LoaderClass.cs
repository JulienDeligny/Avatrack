﻿using UnityEngine.SceneManagement;

public static class LoaderClass
{
    public enum Scene
    {
        MainScene,
        LoadScene,
        NewGameScene
    }

    public static void Load(Scene scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }
}
