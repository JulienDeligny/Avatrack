﻿[System.Serializable]
public class Food
{
    public string name;
    public float defaultQuantity;
    public float quantity;
    public string unit;
    //Order: Kcal, Kohlenhyrate, Eiweiß, Fett
    public float[] nutritionalValues = new float[4];
    public string id;
    public int frequence;
    public bool isDeletable;
}
