﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserExperience
{
    private static int DAILY_EXPERIENCE_POINTS_LIMIT = 20;

    public int level;
    public int userXP;
    public int remainingDailyXP;

    public UserExperience()
    {
        level = 1;
        userXP = 0;
        remainingDailyXP = 20;
    }

    public void SetUserExperience(int userXP)
    {
        this.userXP = userXP;
        CalculateLevel();
    }

    public void ProcessExperiencePoints(int addedXP, bool considerDailyLimit)
    {
        if (considerDailyLimit)
        {
            if (remainingDailyXP > 0)
            {
                remainingDailyXP -= addedXP;
                userXP += addedXP;
                CalculateLevel();
            }
        }
        else
        {
            userXP += addedXP;
            CalculateLevel();
        }
    }

    void CalculateLevel()
    {
        //To level up, the number of experience points (EP) required correspond to the level * 100.
        //Based on this, the current level can be calculated using the sum of EP.
        int sum = 0;
        for (int i = 1; i <= 50; i++)
        {
            sum = (i * (i + 1)) / 2;
            if (((sum - i) * 100) <= userXP && userXP < (sum * 100))
            {
                level = i;
                break;
            }
        }
    }

    public void ResetDailyXPlimit()
    {
        remainingDailyXP = DAILY_EXPERIENCE_POINTS_LIMIT;
    }

}
