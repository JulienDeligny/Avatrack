﻿using System;
using UnityEngine;

[System.Serializable]
public class Goals
{
    #region static variables
    private static int MAX_DEVIATION_HYDRATION_GOAL = 200;
    private static int UPPER_LIMIT_OF_HYDRATION_GOAL = 8000;
    private static int MAX_DEVIATION_OF_CALORIE_GOAL = 200;
    private static float MAX_DEVIATION_OF_MACRO_GOAL_PERCENT = 0.05f;

    private static float CARBOHYDRATES_TO_CALORIES = 4.1f;
    private static float PROTEIN_TO_CALORIES = 4.1f;
    private static float FAT_TO_CALORIES = 9.3f;

    private static float CALORIC_DEFICIT = -500;
    private static float CALORIC_SURPLUS = 300;

    private static float CARBOHYDRATES_MIN_PERCENT = 0.45f;
    private static float CARBOHYDRATES_MAX_PERCENT = 0.65f;

    private static float PROTEIN_MIN_PERCENT = 0.09f;
    private static float PROTEIN_MAX_PERCENT = 0.23f;

    private static float FAT_MIN_PERCENT = 0.2f;
    private static float FAT_MAX_PERCENT = 0.35f;
    #endregion

    public int age;
    public float weight;
    public bool isMale;

    public bool customNutritionGoalsActivated;
    public bool customHydrationGoalsActivated;
    public int[] mealTime;

    public float hydrationGoal;
    public float calorieGoal;
    public float carbohydrateGoal;
    public float proteinGoal;
    public float fatGoal;

    public QuestionnaireData questionnaireData;

    public Goals()
    {
        questionnaireData = new QuestionnaireData();
        customNutritionGoalsActivated = false;
        customHydrationGoalsActivated = false;
        mealTime = new int[] { 6, 10, 11, 14, 18, 21 };
    }

    #region Setters

    public void SetAgeWeightGender(int age, float weight, bool isMale)
    {
        this.age = age;
        this.weight = weight;
        this.isMale = isMale;
        if (isMale)
        {
            questionnaireData.pregnancyFactor = 0;
            questionnaireData.isBreastFeeding = false;
        }
        CalculateGoals();
    }

    public void SetQuestionnaireData(QuestionnaireData questionnaireData)
    {
        customNutritionGoalsActivated = false;

        this.questionnaireData = questionnaireData;
        if (isMale)
        {
            questionnaireData.pregnancyFactor = 0;
            questionnaireData.isBreastFeeding = false;
        }
        CalculateGoals();
    }

    public void SetCustomNutritionGoals(float calorieGoal, float carbsGoalPercent, float proteinGoalPercent, float fatGoalPercent)
    {
        customNutritionGoalsActivated = true;

        this.calorieGoal = calorieGoal;
        carbohydrateGoal = ((carbsGoalPercent / 100) * calorieGoal) / CARBOHYDRATES_TO_CALORIES;
        proteinGoal = ((proteinGoalPercent / 100) * calorieGoal) / PROTEIN_TO_CALORIES;
        fatGoal = ((fatGoalPercent / 100) * calorieGoal) / FAT_TO_CALORIES;
    }

    public void SetCustomHydrationGoals(int goalInMililiters)
    {
        customHydrationGoalsActivated = true;
        hydrationGoal = goalInMililiters;
    }

    public void SetMealTime(int[] mealTime)
    {
        this.mealTime = mealTime;
    }

    #endregion

    #region CalculateGoals

    public void CalculateGoals()
    {
        if (!customNutritionGoalsActivated)
        {
            CalculateNutritionGoals();
        }
        if (!customHydrationGoalsActivated)
        {
            CalculateHydrationGoal();
        }
        
    }

    private void CalculateNutritionGoals()
    {
        float weightGoalCalories = CaloriesDependingOnWeightGoal();

        float BMR = CalculateBMR();
        float PAL = CalculatePAL();
        calorieGoal = ((BMR * PAL) + weightGoalCalories) + Convert.ToInt32(questionnaireData.isBreastFeeding) * 500 + questionnaireData.pregnancyFactor * 250;

        float fatPercent = 0.3f + Convert.ToInt32(questionnaireData.isAthlete) * 0.05f;
        float proteinPercent = 0.11f + (Convert.ToInt32(questionnaireData.increaseProteinIntake) * 0.04f);
        float carbohydratesPercent = 1 - fatPercent - proteinPercent;
        fatGoal = (fatPercent * calorieGoal) / 9.3f;
        proteinGoal = (proteinPercent * calorieGoal) / 4.1f;
        carbohydrateGoal = (carbohydratesPercent * calorieGoal) / 4.1f;
    }

    private float CaloriesDependingOnWeightGoal()
    {
        if (questionnaireData.weightGoalFactor < 0)
            return CALORIC_DEFICIT;
        else if (questionnaireData.weightGoalFactor == 0)
            return 0;
        else 
            return CALORIC_SURPLUS;
    }

    public float CalculateBMR()
    {
        return isMale ? (0.047f * weight + 1.009f - 0.01452f * age + 3.21f) * 239 : (0.047f * weight - 0.01452f * age + 3.21f) * 239;
    }

    public float CalculatePAL()
    {
        return questionnaireData.physicalActivityLevel + 0.3f * Convert.ToInt32(questionnaireData.isAthlete);
    }

    public void CalculateHydrationGoal(bool isPregnant, bool isBreastFeeding)
    {
        if (isPregnant)
            questionnaireData.pregnancyFactor = 1;
        questionnaireData.isBreastFeeding = isBreastFeeding;
        CalculateHydrationGoal();
    }

    public void CalculateHydrationGoal()
    {
        customHydrationGoalsActivated = false;
        float goal = 0;
        if (age < 1) goal = 1000;
        else if (1 <= age && age < 2) goal = 1150;
        else if (2 <= age && age < 4) goal = 1300;
        else if (4 <= age && age < 9) goal = 1600;
        else if (9 <= age && age < 14)
        {
            if (isMale) goal = 2100;
            else goal = 1900;
        }
        else if (14 <= age)
        {
            if (isMale) goal = 2500;
            else goal = 2000;
        }
        bool isPregnant = questionnaireData.pregnancyFactor > 0 ? true : false;
        hydrationGoal = ( goal + 300 * Convert.ToInt32(isPregnant) + 700 * Convert.ToInt32(questionnaireData.isBreastFeeding) );
    }

    #endregion

    #region Check if daily goals are reached
    public bool CheckDailyWaterGoalReached(float currentHydrationLevel)
    {
        float lowerLimit = hydrationGoal - MAX_DEVIATION_HYDRATION_GOAL;
        bool isAboveLowerLimit = lowerLimit <= currentHydrationLevel;
        return isAboveLowerLimit;
    }

    public bool CheckDailyCalorieGoalReached(float currentlyConsumedCalories)
    {
        float lowerLimit = calorieGoal - MAX_DEVIATION_OF_CALORIE_GOAL;
        float upperLimit = calorieGoal + MAX_DEVIATION_OF_CALORIE_GOAL;
        bool isAboveLowerLimit = lowerLimit <= currentlyConsumedCalories;
        bool isBelowUpperLimit = currentlyConsumedCalories <= upperLimit;
        bool isWithinInterval = (isAboveLowerLimit && isBelowUpperLimit);
        return isWithinInterval;
    }

    public bool CheckDailyMacroGoalReached(float currentlyConsumed, Macronutrient macronutrient)
    {
        float minMacroIntake = DetermineMinMaxOfMacroNutrient(macronutrient).Item1;
        float maxMacroIntake = DetermineMinMaxOfMacroNutrient(macronutrient).Item2;
        float macronutrientGoal = DetermineDailyGoalOfMacroNutrient(macronutrient);
        bool isAboveLowerLimit = minMacroIntake <= currentlyConsumed;
        bool isBelowUpperLimit = currentlyConsumed <= maxMacroIntake;
        bool isWithinInterval = (isAboveLowerLimit && isBelowUpperLimit);
        return isWithinInterval;
    }

    private float DetermineDailyGoalOfMacroNutrient(Macronutrient macronutrient)
    {
        float goal = 0;
        if (macronutrient == Macronutrient.carbohydrates)
            goal = carbohydrateGoal;
        else if (macronutrient == Macronutrient.protein)
            goal = proteinGoal;
        else if (macronutrient == Macronutrient.fat)
            goal = fatGoal;
        return goal;
    }
    #endregion

    #region Check if current goals are reached 
    public int CheckCurrentCalorieGoalReached(float currentlyConsumedCalories)
    {
        float currentCalorieGoal = CalculateCurrentCalorieGoal();

        float lowerLimit = currentCalorieGoal - MAX_DEVIATION_OF_CALORIE_GOAL;
        float upperLimit = currentCalorieGoal + MAX_DEVIATION_OF_CALORIE_GOAL;
        bool isAboveLowerLimit = lowerLimit <= currentlyConsumedCalories;
        bool isBelowUpperLimit = currentlyConsumedCalories <= upperLimit;

        int returnValue = 0;
        if (isAboveLowerLimit && isBelowUpperLimit)
        {
        }
        else if (!isAboveLowerLimit)
        {
            returnValue = -1;
        }
        else
        {
            returnValue = 1;
        }
        return returnValue;
    }

    private float CalculateCurrentCalorieGoal()
    {
        int hour = System.DateTime.UtcNow.ToLocalTime().Hour;
        float currentCalorieGoal = calorieGoal;

        if (mealTime[0] <= hour && hour < mealTime[2])
        {
            currentCalorieGoal /= 3;
        }
        else if (mealTime[2] <= hour && hour < mealTime[4])
        {
            currentCalorieGoal = (currentCalorieGoal / 3) * 2;
        }
        else if (mealTime[4] <= hour && hour < 24)
        {
        }
        else
        {
            currentCalorieGoal = 0;
        }
        return currentCalorieGoal;
    }

    public int CheckCurrentMacroGoalReached(float currentlyConsumed, Macronutrient macronutrient)
    {
        float goal = DetermineDailyGoalOfMacroNutrient(macronutrient);
        (float, float) minMax = DetermineMinMaxOfMacroNutrient(macronutrient);


        float deviationMin = goal - minMax.Item1;
        float deviationMax = minMax.Item2 - goal;

        int hour = System.DateTime.UtcNow.ToLocalTime().Hour;
        float currentGoal = goal;
        if (mealTime[0] <= hour && hour < mealTime[2])
        {
            currentGoal /= 3;
        }
        else if (mealTime[2] <= hour && hour < mealTime[4])
        {
            currentGoal = (goal / 3) * 2;
        }
        else if (mealTime[4] <= hour && hour < 24)
        {
        }
        else
        {
            currentGoal = 0;
        }

        float currentMin = currentGoal - deviationMin;
        float currentMax = currentGoal + deviationMax;

        bool isAboveLowerLimit = currentMin <= currentlyConsumed;
        bool isBelowUpperLimit = currentlyConsumed <= currentMax;

        int returnValue = 0;
        if (isAboveLowerLimit && isBelowUpperLimit)
        {
        }
        else if (!isAboveLowerLimit)
        {
            returnValue = -1;
        }
        else
        {
            returnValue = 1;
        }
        return returnValue;
    }

    public int CheckCurrentWaterGoal(float currentHydrationLevel)
    {
        int hour = System.DateTime.UtcNow.ToLocalTime().Hour;

        int timeAwake = (mealTime[5] - mealTime[0]);
        float mililitersPerHour = (hydrationGoal / timeAwake);
        int passedHoursSinceWakingUp = (hour - mealTime[0]);

        float currentHydrationGoal;
        float lowerLimit;
        float upperLimit;
        if (hour < mealTime[0])
        {
            lowerLimit = 0;
            currentHydrationGoal = 0;
            upperLimit = (UPPER_LIMIT_OF_HYDRATION_GOAL / timeAwake);
        }
        else if (hour >= mealTime[5])
        {
            currentHydrationGoal = hydrationGoal;
            lowerLimit = hydrationGoal - MAX_DEVIATION_HYDRATION_GOAL;
            upperLimit = 8000;
        }
        else
        {
            currentHydrationGoal = mililitersPerHour * passedHoursSinceWakingUp;
            lowerLimit = currentHydrationGoal - MAX_DEVIATION_HYDRATION_GOAL;
            upperLimit = (UPPER_LIMIT_OF_HYDRATION_GOAL / timeAwake) * passedHoursSinceWakingUp;
        }

        bool isAboveLowerLimit = currentHydrationLevel >= lowerLimit;
        bool isBelowUpperLimit = currentHydrationLevel <= upperLimit;

        int returnValue = 0;
        if (isAboveLowerLimit && isBelowUpperLimit)
        {
        }
        else if (!isAboveLowerLimit)
        {
            returnValue = -1;
        }
        else
        {
            returnValue = 1;
        }
        return returnValue;
    }
    #endregion

    #region CalculateMinMax
    public (float, float) CalculateMinMaxCalorieGoal()
    {
        float min = calorieGoal - MAX_DEVIATION_OF_CALORIE_GOAL;
        float max = calorieGoal + MAX_DEVIATION_OF_CALORIE_GOAL;
        return (min, max);
    }

    public (float, float) CalculateMinMaxCarbohydrateGoal()
    {
        float min = (CARBOHYDRATES_MIN_PERCENT * calorieGoal) / CARBOHYDRATES_TO_CALORIES;
        float max = (CARBOHYDRATES_MAX_PERCENT * calorieGoal) / CARBOHYDRATES_TO_CALORIES;
        if (min > carbohydrateGoal)
            min = carbohydrateGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < carbohydrateGoal)
            max = carbohydrateGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT); ;
        return (min, max);
    }

    public (float, float) CalculateMinMaxProteinGoal()
    {
        float min = (PROTEIN_MIN_PERCENT * calorieGoal) / PROTEIN_TO_CALORIES;
        float max = (PROTEIN_MAX_PERCENT * calorieGoal) / PROTEIN_TO_CALORIES;
        if (min > proteinGoal)
            min = proteinGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < proteinGoal)
            max = proteinGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        return (min, max);
    }

    public (float, float) CalculateMinMaxFatGoal()
    {
        float min = (FAT_MIN_PERCENT * calorieGoal) / FAT_TO_CALORIES;
        float max;
        if (customNutritionGoalsActivated)
        {
            max = (FAT_MAX_PERCENT * calorieGoal) / FAT_TO_CALORIES;
        }
        else
        {
            max = (float)((0.35f + Convert.ToInt32(questionnaireData.isAthlete) * 0.05) * calorieGoal) / FAT_TO_CALORIES;
        }
        if (min > fatGoal)
            min = fatGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < fatGoal)
            max = fatGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        return (min, max);
    }

    public (float, float) CalculateMinMaxCarbohydrateGoal(float calorieGoal)
    {
        float min = (CARBOHYDRATES_MIN_PERCENT * calorieGoal) / CARBOHYDRATES_TO_CALORIES;
        float max = (CARBOHYDRATES_MAX_PERCENT * calorieGoal) / CARBOHYDRATES_TO_CALORIES;
        if (min > carbohydrateGoal)
            min = carbohydrateGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < carbohydrateGoal)
            max = carbohydrateGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT); ;
        return (min, max);
    }

    public (float, float) CalculateMinMaxProteinGoal(float calorieGoal)
    {
        float min = (PROTEIN_MIN_PERCENT * calorieGoal) / PROTEIN_TO_CALORIES;
        float max = (PROTEIN_MAX_PERCENT * calorieGoal) / PROTEIN_TO_CALORIES;
        if (min > proteinGoal)
            min = proteinGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < proteinGoal)
            max = proteinGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        return (min, max);
    }

    public (float, float) CalculateMinMaxFatGoal(float calorieGoal)
    {
        float min = (FAT_MIN_PERCENT * calorieGoal) / FAT_TO_CALORIES;
        float max;
        if (customNutritionGoalsActivated)
        {
            max = (FAT_MAX_PERCENT * calorieGoal) / FAT_TO_CALORIES;
        }
        else
        {
            max = (float)((0.35f + Convert.ToInt32(questionnaireData.isAthlete) * 0.03) * calorieGoal) / FAT_TO_CALORIES;
        }
        if (min > fatGoal)
            min = fatGoal - (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        if (max < fatGoal)
            max = fatGoal + (carbohydrateGoal * MAX_DEVIATION_OF_MACRO_GOAL_PERCENT);
        return (min, max);
    }

    private (float, float) DetermineMinMaxOfMacroNutrient(Macronutrient macronutrient)
    {
        float min;
        float max;
        if (macronutrient == Macronutrient.carbohydrates)
        {
            min = CalculateMinMaxCarbohydrateGoal().Item1;
            max = CalculateMinMaxCarbohydrateGoal().Item2;
        }
        else if (macronutrient == Macronutrient.protein)
        {
            min = CalculateMinMaxProteinGoal().Item1;
            max = CalculateMinMaxProteinGoal().Item2;
        }
        else 
        {
            min = CalculateMinMaxFatGoal().Item1;
            max = CalculateMinMaxFatGoal().Item2;
        }
        return (min, max);
    }

    #endregion
}

public enum Macronutrient
{
    carbohydrates,
    protein,
    fat
}
