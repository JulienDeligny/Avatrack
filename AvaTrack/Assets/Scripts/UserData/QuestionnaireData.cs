﻿using UnityEngine;

[System.Serializable]
public class QuestionnaireData
{
    public float physicalActivityLevel;
    public bool isAthlete;
    public int pregnancyFactor;
    public bool isBreastFeeding;
    public float weightGoalFactor;
    public bool increaseProteinIntake;
}
