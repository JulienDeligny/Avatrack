﻿using System;
using System.Collections.Generic;

[System.Serializable]
public class HydrationLevel
{
    public List<Drink> drinks;
    public float currentHydrationLevel;

    public HydrationLevel()
    {
        ClearDrinks();
    }

    public void SetDrinkList(List<Drink> drinks)
    {
        this.drinks = drinks;
        CalculateHydrationLevel();
    }

    public Drink GetLastAddedDrink()
    {
        int indexOfLastElement = drinks.Count - 1;
        return drinks[indexOfLastElement];
    }

    public void ClearDrinks()
    {
        drinks = new List<Drink>();
        currentHydrationLevel = 0;
    }

    public void AddDrink(Drink drink)
    {
        drinks.Insert(0, drink);
        CalculateHydrationLevel();
    }

    public void RemoveDrink(Drink drink)
    {
        for (int i = 0; i < drinks.Count; i++)
        {
            if (Convert.ToInt64(drinks[i].id.Replace("\n", "")) == Convert.ToInt64(drink.id))
            {
                drinks.RemoveAt(i);
                break;
            }
        }
        CalculateHydrationLevel();
    }

    private void CalculateHydrationLevel()
    {
        currentHydrationLevel = 0;
        foreach(Drink drink in drinks)
        {
            currentHydrationLevel += drink.quantity;
        }
    }
}
