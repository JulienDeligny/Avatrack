﻿using UnityEngine;

public class UserFoodRecord
{
    public FoodList createdFoodList;
    public FrequentlyConsumedFoodList frequentlyConsumedFoodList;
    public RecentlyConsumedFoodList recentlyConsumedFoodList;

    public UserFoodRecord()
    {
        createdFoodList = new FoodList();
        frequentlyConsumedFoodList = new FrequentlyConsumedFoodList();
        recentlyConsumedFoodList = new RecentlyConsumedFoodList();
    }


    public void DeleteCreatedFood(Food food)
    {
        createdFoodList.RemoveFirstOccurrenceByID(food);
        frequentlyConsumedFoodList.RemoveFirstOccurrenceByID(food);
        recentlyConsumedFoodList.RemoveFirstOccurrenceByID(food);
    }

    public void RecordFood(Food food)
    {
        frequentlyConsumedFoodList.Add(food);
        recentlyConsumedFoodList.Add(food);
    }
}

