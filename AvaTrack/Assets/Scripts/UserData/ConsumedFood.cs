﻿using System.Collections.Generic;
using System.Linq;

public enum Meal
{
    breakfast,
    lunch,
    dinner
}

public enum NutritionalValueType
{
    calories,
    carbohydrates,
    protein,
    fat
}

[System.Serializable]
public class ConsumedFood
{
    private FoodList _breakfastFoodList;
    private FoodList _lunchFoodList;
    private FoodList _dinnerFoodList;

    public FoodList breakfastFoodList { get => _breakfastFoodList; }
    public FoodList lunchFoodList { get => _lunchFoodList; }
    public FoodList dinnerFoodList { get => _dinnerFoodList; }

    // Calories, Carbohydrates, Protein, Fat 
    public float[] totalBalanceOfNutritionalValues;

    public ConsumedFood()
    {
        totalBalanceOfNutritionalValues = new float[4] { 0, 0, 0, 0 };
        ResetConsumedFoodLists();
    }

    public void SetConsumedFoodList(List<Food> _foodList, Meal meal)
    {
        FoodList foodListObject = GetFoodListCorrespondingTo(meal);
        foodListObject.foodList = _foodList;
        float[] nutritionalValue = foodListObject.SumOfNutritionalValues();
        CalculateBalance();
    }

    #region Getters
    public float GetConsumed(NutritionalValueType type)
    {
        if (type == NutritionalValueType.calories)
            return totalBalanceOfNutritionalValues[0];
        else if (type == NutritionalValueType.carbohydrates)
            return totalBalanceOfNutritionalValues[1];
        else if (type == NutritionalValueType.protein)
            return totalBalanceOfNutritionalValues[2];
        else
            return totalBalanceOfNutritionalValues[3];
    }

    public List<Food> GetConsumedFoodInSingleList()
    {
        return (_breakfastFoodList.foodList.Concat(_lunchFoodList.foodList)).Concat(_dinnerFoodList.foodList).ToList();
    }

    public float[] GetSumOfNutritionalValuesFor(Meal meal)
    {
        FoodList foodList = GetFoodListCorrespondingTo(meal);
        return foodList.SumOfNutritionalValues();
    }

    private FoodList GetFoodListCorrespondingTo(Meal meal)
    {
        if (meal == Meal.breakfast)
        {
            return _breakfastFoodList;
        }
        else if (meal == Meal.lunch)
        {
            return _lunchFoodList;
        }
        else
        {
            return _dinnerFoodList;
        }
    }
    #endregion

    #region Modify Food Lists

    public void ResetConsumedFoodLists()
    {
        _breakfastFoodList = new FoodList();
        _lunchFoodList = new FoodList();
        _dinnerFoodList = new FoodList();
        totalBalanceOfNutritionalValues = new float[4] { 0, 0, 0, 0 };
    }

    public void ClearConsumedFoodList(Meal meal)
    {
        FoodList foodList = GetFoodListCorrespondingTo(meal);
        foodList.foodList.Clear();
        CalculateBalance();
    }

    public void DeleteCreatedFood(Food food)
    {
        _breakfastFoodList.RemoveAllOccurrencesByID(food);
        _lunchFoodList.RemoveAllOccurrencesByID(food);
        _dinnerFoodList.RemoveAllOccurrencesByID(food);
        CalculateBalance();
    }

    public void AddToConsumedFood(Food food, Meal meal)
    {
        FoodList foodList = GetFoodListCorrespondingTo(meal);
        Food newFood = CopyPropertiesIntoNewObject(food);
        foodList.Add(newFood);
        CalculateBalance();
    }

    public void RemoveFromConsumedFood(Food food, Meal meal)
    {
        FoodList foodList = GetFoodListCorrespondingTo(meal);
        foodList.RemoveFirstOccurrenceByIDAndQuantity(food);
        CalculateBalance();
    }

    public void ModifyFood(Food food, Meal meal, float newQuantity)
    {
        FoodList foodList = GetFoodListCorrespondingTo(meal);
        int index = foodList.GetFoodIndexByID(food);
        foodList.foodList[index].quantity = newQuantity;
        CalculateBalance();
    }

    #endregion

    private void CalculateBalance()
    {
        totalBalanceOfNutritionalValues = new float[4] { 0, 0, 0, 0 };

        List<Food> list = GetConsumedFoodInSingleList();
        foreach (Food food in list)
        {
            float multiplier = food.quantity / food.defaultQuantity;
            for (int i = 0; i < 4; i++)
            {
                totalBalanceOfNutritionalValues[i] += multiplier * food.nutritionalValues[i];
            }
        }
    }

    private Food CopyPropertiesIntoNewObject(Food food)
    {
        Food newFood = new Food();
        newFood.name = food.name;
        newFood.defaultQuantity = food.defaultQuantity;
        newFood.quantity = food.quantity;
        newFood.unit = food.unit;
        newFood.nutritionalValues = food.nutritionalValues;
        newFood.id = food.id;
        newFood.frequence = food.frequence;
        newFood.isDeletable = food.isDeletable;
        return newFood;
    }
}
