﻿using System.Collections.Generic;
using System;

//FoodList datastructure and inheriting classes
[System.Serializable]
public class FoodList
{
    protected static int MAX_LENGTH_OF_RECORD = 30;

    public List<Food> foodList;

    public FoodList()
    {
        foodList = new List<Food>();
    }

    protected bool CompareID(Food firstFood, Food secondFood)
    {
        long firstID = Convert.ToInt64(firstFood.id.Replace("\n", ""));
        long secondID = Convert.ToInt64(secondFood.id.Replace("\n", ""));
        return (Convert.ToInt64(firstFood.id.Replace("\n", "")) == Convert.ToInt64(secondFood.id.Replace("\n", "")));
    }

    public int GetFoodIndexByID(Food food)
    {
        if (foodList.Count > 0)
        {
            for (int i = 0; i < foodList.Count; i++)
            {
                if (CompareID(foodList[i], food))
                    return i;
            }
        }
        return -1;
    }

    public void RemoveFirstOccurrenceByID(Food food)
    {
        int index = GetFoodIndexByID(food);
        if (index != -1)
            foodList.RemoveAt(index);
    }

    public void RemoveAllOccurrencesByID(Food foodToDelete)
    {
        List<Food> newFoodList = new List<Food>();
        foreach(Food food in foodList)
        {
            if (!CompareID(food, foodToDelete))
            {
                newFoodList.Add(food);
            }
        }
        foodList = newFoodList;
    }

    public void RemoveFirstOccurrenceByIDAndQuantity(Food foodToDelete)
    {
        int indexToRemove = -1;
        for (int i = 0; i < foodList.Count; i++)
        {
            if (CompareID(foodList[i], foodToDelete) && foodList[i].quantity == foodToDelete.quantity)
            {
                indexToRemove = i;
                break;
            }
        }
        if (indexToRemove != -1)
            foodList.RemoveAt(indexToRemove);

    }

    public virtual void Add(Food food)
    {
        foodList.Add(food);
    }

    public float[] SumOfNutritionalValues()
    {
        float[] nutritionalValues = new float[4] { 0, 0, 0, 0 };
        foreach (Food food in foodList)
        {
            float multiplier = food.quantity / food.defaultQuantity;
            nutritionalValues[0] += multiplier * food.nutritionalValues[0];
            nutritionalValues[1] += multiplier * food.nutritionalValues[1];
            nutritionalValues[2] += multiplier * food.nutritionalValues[2];
            nutritionalValues[3] += multiplier * food.nutritionalValues[3];
        }
        return nutritionalValues;
    }
}

public class FrequentlyConsumedFoodList : FoodList
{
    public FrequentlyConsumedFoodList()
    {
        foodList = new List<Food>();
    }

    public override void Add(Food food)
    {
        food.frequence++;
        if (foodList.Count == 0)
        {
            foodList.Add(food);
        }
        else
        {
            int index = GetFoodIndexByID(food);
            if (index == -1)
            {
                foodList.Insert(foodList.Count - 1, food);
            }
            InsertionSort();
            if (foodList.Count > MAX_LENGTH_OF_RECORD)
                foodList.RemoveAt(foodList.Count - 1);
        }
    }

    private void InsertionSort()
    {
        for (int i = 1; i < foodList.Count; i++)
        {
            Food foodToSort = foodList[i];
            int j = i;
            while (j > 0 && foodList[j - 1].frequence < foodToSort.frequence)
            {
                foodList[j] = foodList[j - 1];
                j--; 
            }
            foodList[j] = foodToSort;
        }
    }
}

public class RecentlyConsumedFoodList : FoodList
{
    public RecentlyConsumedFoodList()
    {
        foodList = new List<Food>();
    }

    public override void Add(Food food)
    {
        int index = GetFoodIndexByID(food);

        if (index != -1) foodList.RemoveAt(index);

        foodList.Insert(0, food);

        if (foodList.Count > MAX_LENGTH_OF_RECORD)
            foodList.RemoveAt(foodList.Count - 1);
    }
}
