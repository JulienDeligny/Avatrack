﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[System.Serializable]
public class User
{
    #region UserData

    public UserSettings settings;

    public Goals goals;

    public ConsumedFood consumedFood;

    public UserFoodRecord userFoodRecord;

    public HydrationLevel hydrationLevel;

    public UserExperience userExperience;

    #endregion

    public User ()
    {
        settings = new UserSettings();
        goals = new Goals();
        consumedFood = new ConsumedFood();
        hydrationLevel = new HydrationLevel();
        userFoodRecord = new UserFoodRecord();
        userExperience = new UserExperience();
    }

    public void DeleteCreatedFood(Food food)
    {
        consumedFood.DeleteCreatedFood(food);
        userFoodRecord.DeleteCreatedFood(food);
    }

    public void Consume(Food food, Meal meal)
    {
        consumedFood.AddToConsumedFood(food, meal);
    }

    public void Consume(Drink drink)
    {
        hydrationLevel.AddDrink(drink);
    }

    public void RemoveConsumed(Food food, Meal meal)
    {
        consumedFood.RemoveFromConsumedFood(food, meal);
    }

    public void RemoveConsumed(Drink drink)
    {
        hydrationLevel.RemoveDrink(drink);
    }

}
