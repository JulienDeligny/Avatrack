﻿using UnityEngine;

[System.Serializable]
public class UserSettings
{
    public bool isNotificationEnabled;
    public bool isWeatherFunctionEnabled;
    public bool isLocationFunctionEnabled;
    public string locationName;
    public bool isStretchAspectRationOn;

    public UserSettings()
    {
    }

    public void AdaptAspectRatio(bool isStretching)
    {
        isStretchAspectRationOn = isStretching;
        if (isStretching)
            Screen.SetResolution(1080, 1920, FullScreenMode.ExclusiveFullScreen);
        else
            Screen.SetResolution(Screen.width, Screen.height, true);
    }
}
