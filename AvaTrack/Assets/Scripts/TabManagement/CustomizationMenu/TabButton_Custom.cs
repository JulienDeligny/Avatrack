﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TabButton_Custom : TabButton, IPointerClickHandler, IPointerExitHandler
{

    public GameObject shadow;

    protected override void Start()
    {
        shadow = transform.GetChild(1).gameObject;
        imageComp = GetComponent<Image>();
        tabManagement.Subscribe(this);
    } 

    public new void OnPointerClick(PointerEventData eventData)
    {
        tabManagement.OnTabSelected(this);
    }
}
