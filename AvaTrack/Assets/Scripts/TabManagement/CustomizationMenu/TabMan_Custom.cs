﻿using System.Collections.Generic;
using UnityEngine;

//This is an implementation of the tab system for the customization page
public class TabMan_Custom: TabManagement
{
    public Color tabIdle;
    public Color tabActive;

    public TabButton_Custom selectedTab;
    public TabButton_Custom defaultSelectedTab;

    public List<GameObject> pages;

    public void Subscribe(TabButton_Custom button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }
        tabButtons.Add(button);
    }

    public override void OnTabSelected(TabButton button)
    {
        TabButton_Custom tabButton = (TabButton_Custom)button;
        OnTabSelected(tabButton);
    }

    public void OnTabSelected(TabButton_Custom button)
    {
        selectedTab = button;
        ResetTabs();
        button.imageComp.color = tabActive;
        button.shadow.SetActive(false);
        
        int index = button.transform.GetSiblingIndex();
        for (int i = 0; i < pages.Count; i++)
        {
            if (i == index)
            {
                pages[i].SetActive(true);
            }
            else
            {
                pages[i].SetActive(false);
            }
        }
    }

    public override void ResetTabs()
    {
        foreach (TabButton_Custom elem in tabButtons)
        {
            if (selectedTab != null && elem == selectedTab) continue;
            elem.imageComp.color = tabIdle;
            elem.shadow.SetActive(true);
        }
    }

    public void SelectFirstTab()
    {
        OnTabSelected(defaultSelectedTab);
    }
}
