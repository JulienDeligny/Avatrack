﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TabButton : MonoBehaviour, IPointerClickHandler, IPointerExitHandler
{
    public TabManagement tabManagement;

    public Image imageComp;

    public void OnPointerClick(PointerEventData eventData)
    {
        tabManagement.OnTabSelected(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tabManagement.OnTabExit(this);
    }

    protected virtual void Start()
    {
        imageComp = GetComponent<Image>();
        tabManagement.Subscribe(this);
    }
}
