﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TabManagement : MonoBehaviour
{
    public List<TabButton> tabButtons;

    public void Subscribe(TabButton button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }
        tabButtons.Add(button);
    }

    public void OnTabExit(TabButton button)
    {
        ResetTabs();
    }

    public abstract void OnTabSelected(TabButton button);

    public abstract void ResetTabs();
}
