﻿using System.Collections.Generic;
using UnityEngine;

//This is an implementation of the tab system with a bar underlining the active page.
public class TabMan_TopBar : TabManagement
{
    public ScreenTransitions transitions;

    public Color tabIdle;
    public Color tabActive;

    public TabButton defaultTab;
    public TabButton selectedTab;

    public List<GameObject> pages;

    public GameObject showActiveTabBar;

    public float parentWidth;


    void Start()
    {
        SetStartingPositionTopBar();
    }

    public int nbrOfButtons; //nbrOfButtons needs to be set in the inspector bec the Buttons get linked to this class at runtime.
    public float currentPosition;
    public float startingPosition; //starting position varies depending on the number of buttons.
    public void SetStartingPositionTopBar()
    {
        parentWidth = Screen.width;
        showActiveTabBar.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1080 / nbrOfButtons);
        currentPosition = parentWidth / (2 * nbrOfButtons);
        LeanTween.moveLocalX(showActiveTabBar, startingPosition, 0);
    }

    public override void OnTabSelected(TabButton button)
    {
        if (button != selectedTab)
        {
            
            ResetTabs();
            button.imageComp.color = tabActive;
            int indexNewTab = button.transform.GetSiblingIndex();
            int indexOldTab = selectedTab.transform.GetSiblingIndex();
            MoveBar(indexOldTab, indexNewTab);
            selectedTab = button;
            for (int i = 0; i < pages.Count; i++)
            {
                if (i == indexNewTab)
                {
                    pages[i].SetActive(true);
                }
                else
                {
                    pages[i].SetActive(false);
                }
            }
        }
    }

    void MoveBar(int indexOldTab, int indexNewTab)
    {
        int diff = indexNewTab - indexOldTab;
        currentPosition = currentPosition - diff * parentWidth / nbrOfButtons;
        LeanTween.moveX(showActiveTabBar, -1 * currentPosition + parentWidth / nbrOfButtons, transitions.GetTransitionSpeed());
    }

    public override void ResetTabs()
    {
    }

    public void OnFoodMenuExit()
    {
        OnTabSelected(defaultTab);
    }

    public void UpdateSelectedFoodPage()
    {
        int indexTab = selectedTab.transform.GetSiblingIndex();
        pages[indexTab].GetComponent<FoodPage>().OnPageClose();

        pages[indexTab].GetComponent<FoodPage>().OnPageOpen();
    }
}
