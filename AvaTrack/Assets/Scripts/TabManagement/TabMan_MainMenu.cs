﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This is an implementation of the tab system for the main menu
public class TabMan_MainMenu : TabManagement
{
    public ScreenTransitions transitions;

    public Color tabIdle;
    public Color tabActive;

    public TabButton selectedTab;

    #region Obervers
    //When the mainFoodPage is loaded the OnPageOpen() function loads all the "eatenFoodComponents"
    public TabButton foodButton;
    public FoodPage mainFoodPage;

    public TabButton hyrationButton;
    public WaterMenuMainPage hydrationPage;
    #endregion

    //Activating the character collider when the home page is active
    public TabButton homePageButton;

    public List<GameObject> pages;


     void Start()
    {
        selectedTab = homePageButton;
        selectedTab.GetComponent<Image>().color = tabActive;
    }

    public override void OnTabSelected(TabButton button)
    {

        if (selectedTab == foodButton) mainFoodPage.OnPageClose();

        //transition to the new selected page
        int lastTabIndex = selectedTab.transform.GetSiblingIndex();
        selectedTab = button;
        int newTabIndex = selectedTab.transform.GetSiblingIndex();
        transitions.MainMenuPageSwipe(lastTabIndex - newTabIndex);

        ResetTabs();
        button.imageComp.color = tabActive;

        NotifyHydrationPage();
        if(selectedTab == foodButton) mainFoodPage.OnPageOpen();
    }

    public override void ResetTabs()
    {
        foreach (TabButton elem in tabButtons)
        {
            if (selectedTab != null && elem == selectedTab) continue;
            elem.imageComp.color = tabIdle;
        }
    }

    public void NotifyHydrationPage()
    {
        if (selectedTab == hyrationButton)
        {
            hydrationPage.UpdateView(true);
        }
        else
        {
            hydrationPage.UpdateView(false);
        }

    }
}
