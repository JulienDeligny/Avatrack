﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FoodMenu_UpdateValues : MonoBehaviour, ISingleMacroObserver
{
    public bool isCaloriesView;

    public void  UpdateScreenValues(float newNutritionalValue, float goal)
    {
        string unit = isCaloriesView ? "kcal" : "g"; 
        gameObject.GetComponent<Text>().text = Convert.ToInt32(newNutritionalValue).ToString() + " " + unit;
    }
}
