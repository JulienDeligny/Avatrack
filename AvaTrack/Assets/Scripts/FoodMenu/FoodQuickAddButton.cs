﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FoodQuickAddButton : MonoBehaviour, IMealTimeObserver
{
    private static string BREAKFAST = "Frühstück";
    private static string LUNCH = "Mittagessen";
    private static string DINNER = "Abendessen";

    public UserDataModel userDataModel;
    public ScreenTransitions transitions;

    private int[] mealtimes;

    public MealComponent breakfast;
    public MealComponent lunch;
    public MealComponent dinner;

    public Text pageTitle;

    public FoodPage frequentlyUserPage;
    public GameObject UserFoodRecordPage;

    public void UpdateMealTime()
    {
        this.mealtimes = userDataModel.GetUserGoals().mealTime;
    }

    //The food is added to the meal which is closest to the current time.
    public void OnClick()
    {
        DetermineMealDependingOnDayTime();
        frequentlyUserPage.OnPageOpen();
        transitions.PageSwipeUp(UserFoodRecordPage);
    }

    public void DetermineMealDependingOnDayTime()
    {
        int time = Convert.ToInt32(System.DateTime.UtcNow.ToLocalTime().ToString("HH"));
        int mid = 0;
        if (mealtimes[0] <= time && time < mealtimes[1])
        {
            breakfast.OnClick();
            pageTitle.text = BREAKFAST;
            
        }
        else if (mealtimes[1] <= time && time < mealtimes[2])
        {
            mid = (Math.Abs(mealtimes[2]) - Math.Abs(mealtimes[1])) / 2;
            if (time - mealtimes[1] <= mid)
            {
                breakfast.OnClick();
                pageTitle.text = BREAKFAST;
            }
            else
            {
                lunch.OnClick();
                pageTitle.text = LUNCH;
            }

        }
        else if (mealtimes[2] <= time && time < mealtimes[3])
        {
            lunch.OnClick();
            pageTitle.text = BREAKFAST;
        }
        else if (mealtimes[3] <= time && time < mealtimes[4])
        {
            mid = (Math.Abs(mealtimes[4]) - Math.Abs(mealtimes[3])) / 2;
            if (time - mealtimes[3] <= mid)
            {
                lunch.OnClick();
                pageTitle.text = LUNCH;
            }
            else
            {
                dinner.OnClick();
                pageTitle.text = DINNER;
            }
        }
        else if (mealtimes[4] <= time && time < mealtimes[5])
        {
            dinner.OnClick();
            pageTitle.text = DINNER;
        }
        else //between dinner and breakfast
        {
            int normalizedDinnerTime = Math.Abs(mealtimes[5] - 24);
            int normalizedBreakfastTime = mealtimes[0] + normalizedDinnerTime;
            int oldTime = time;
            if (time > mealtimes[0]) time = time - 24;
            time += normalizedDinnerTime;
            mid = (mealtimes[0] + normalizedDinnerTime) / 2;
            if (time <= mid)
            {
                dinner.OnClick();
                pageTitle.text = DINNER;
            }
            else
            {
                breakfast.OnClick();
                pageTitle.text = BREAKFAST;
            }
        }
    }
}
