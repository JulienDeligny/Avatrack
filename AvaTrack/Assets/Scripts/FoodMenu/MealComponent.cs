﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This 
public class MealComponent : FoodListObserver
{
    public Text kcalText;
    public UserDataModel userDataModel;
    public ScreenTransitions transitions;

    public FoodMenuPages foodMenuPagesScript;

    //Observer pages that need to be informed about the 
    //selected meal so that food gets added to the right list
    public AddFoodPage addFoodPageScript;
    public EditFoodPage editFoodPageScript;
    public Meal meal;

    public GameObject mealPage;
    FoodPage mealPageScript;
    public KcalAndMacrosView mealValues; //View of the MealPage
    float[] mealKcalAndMacro = new float[4];

    public Text mealName;
    public Text mealName_mealPage;


    void Start()
    {
        mealPageScript = mealPage.GetComponent<FoodPage>();

        gameObject.GetComponent<Button>().onClick.AddListener(() => OnClick());
        gameObject.GetComponent<Button>().onClick.AddListener(() => transitions.PageSwipeUp(mealPage));
    }

    public override void UpdateList(List<Food> foodList)
    {
        this.foodList = foodList;
        mealKcalAndMacro = userDataModel.GetConsumedFood().GetSumOfNutritionalValuesFor(meal);
        this.kcalText.text = Convert.ToInt32(mealKcalAndMacro[0]).ToString() + " kcal";
    }

    public void OnClick()
    {
        //Notify meal observers
        addFoodPageScript.meal = meal;
        editFoodPageScript.meal = meal;
        //Update MealPage
        mealPageScript.foodList = this.foodList;
        mealValues.UpdateView(meal);
        mealName_mealPage.text = mealName.text;
        foodMenuPagesScript.OnPageEnter(mealPageScript);
    }

}
