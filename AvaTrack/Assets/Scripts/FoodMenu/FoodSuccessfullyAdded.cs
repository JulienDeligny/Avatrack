﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodSuccessfullyAdded : MonoBehaviour
{
    public CanvasGroup group;
    public Text text;

    public void Show(bool isAdding)
    {
        if (isAdding)
            text.text = "hinzugefügt";
        else
            text.text = "verändert";
        gameObject.SetActive(true);

        StartCoroutine(PlayAnimation());
    }

    public IEnumerator PlayAnimation()
    {
        LeanTween.alphaCanvas(group, 1, 0.2f);
        yield return new WaitForSeconds(1);
        LeanTween.alphaCanvas(group, 0, 0.2f);
        yield return new WaitForSeconds(0.2f);

        gameObject.SetActive(false);
    }
}
