﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

[System.Serializable]
public class PremadeFoodDatabase
{
    private static string SPECIFIC_FOOD_LIST_FILE_NAME = "SpecificFoodDatabaseUTF8";
    private static string SUMMARIZED_FOOD_LIST_FILE_NAME = "SummarizedFoodDatabaseUTF8";

    public List<Food> generalPremadeFoodDatabase;
    public List<Food> specificPremadeFoodDatabase;

    public void LoadPremadeFoodDatabase()
    {
        LoadSummarizedPremadeFoodList();
        LoadSpecificPremadeFoodList();
    }

    private void LoadSummarizedPremadeFoodList()
    {
        TextAsset fileData = Resources.Load<TextAsset>(SUMMARIZED_FOOD_LIST_FILE_NAME);
        generalPremadeFoodDatabase = ConvertFileToFoodList(fileData);
    }

    private void LoadSpecificPremadeFoodList()
    {
        TextAsset fileData = Resources.Load<TextAsset>(SPECIFIC_FOOD_LIST_FILE_NAME);
        specificPremadeFoodDatabase = ConvertFileToFoodList(fileData);
        
    }

    private List<Food> ConvertFileToFoodList(TextAsset fileData)
    {
        //The files are in the CSV format
        //columns are seperated by semicolons 
        //rows are seperated by newlines 
        string[] dataGeneral = fileData.text.Split(new char[] { '\n' });

        List<Food> foodList = new List<Food>();
        for (int i = 2; i < dataGeneral.Length - 1; i++)
        {
            string[] singleRow = dataGeneral[i].Split(new char[] { ';' });
            Food food = ConvertRowToFoodObject(singleRow);
            foodList.Add(food);
        }
        return foodList;
    }

    private Food ConvertRowToFoodObject(string[] row)
    {
        Food food = InstatiateDefaultFood();

        food.name = row[0];
        //the culture argument is important.
        //It determines that commas get interpreted as decimal points on any system
        food.nutritionalValues[0] = float.Parse(row[5], CultureInfo.GetCultureInfo("de-DE"));
        food.nutritionalValues[1] = float.Parse(row[6], CultureInfo.GetCultureInfo("de-DE"));
        food.nutritionalValues[2] = float.Parse(row[7], CultureInfo.GetCultureInfo("de-DE"));
        food.nutritionalValues[3] = float.Parse(row[8], CultureInfo.GetCultureInfo("de-DE"));
        food.unit = row[2] == "g" ? "Gramm" : "Milliliter";
        food.id = row[9].Replace("\n", "");
        return food;
    }

    private Food InstatiateDefaultFood()
    {
        Food food = new Food();
        food.defaultQuantity = 100;
        food.frequence = 0;
        food.isDeletable = false;
        return food;
    }

}
