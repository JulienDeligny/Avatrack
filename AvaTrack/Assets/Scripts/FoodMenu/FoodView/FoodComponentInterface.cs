﻿public interface IFoodComponent
{
    void AssignProperties(float quantity, Food food);
}
