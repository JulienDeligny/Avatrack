﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EatenFoodComponentPrefab : MonoBehaviour, IFoodComponent
{
    public Text foodName;
    public Text quantity;
    public Text kcal;

    public Food food;

    GameObject editFoodPage;

    public void AssignProperties(float quantity, Food food)
    {
        this.food = food;
        foodName.text = food.name;
        this.quantity.text = food.quantity.ToString() + " " + food.unit;
        float multiplier = food.quantity / food.defaultQuantity;
        kcal.text = Convert.ToInt32(food.nutritionalValues[0] * multiplier).ToString() + "\nkcal";

        editFoodPage = GameObject.Find("EditFoodPage");
        this.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("GameManager").GetComponent<ScreenTransitions>().PageSwipeUp(editFoodPage));
        this.GetComponent<Button>().onClick.AddListener(() => editFoodPage.GetComponent<EditFoodPage>().OnFoodComponentClick(this.food, this.gameObject));
    }
}
