﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KcalAndMacrosView : MonoBehaviour, IMacrosObserver
{
    public UserDataModel userDataModel;

    public GameObject calorieView;
    public GameObject carbohydrateView;
    public GameObject proteinView;
    public GameObject fatView;

    public void UpdateView(Meal meal)
    {
        float[] mealValues = userDataModel.GetConsumedFood().GetSumOfNutritionalValuesFor(meal);
        calorieView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(mealValues[0], 0);
        carbohydrateView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(mealValues[1], 0);
        proteinView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(mealValues[2], 0);
        fatView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(mealValues[3], 0);
    }

    public void UpdateMacroValues()
    {
        UpdateCalorieView();
        UpdateCarbohydratesView();
        UpdateProteinView();
        UpdateFatView();
    }

    private void UpdateCalorieView()
    {
        float consumed = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.calories);
        float goal = userDataModel.GetUserGoals().calorieGoal;
        calorieView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(consumed, goal);
    }

    private void UpdateCarbohydratesView()
    {
        float consumed = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.carbohydrates);
        float goal = userDataModel.GetUserGoals().carbohydrateGoal;
        carbohydrateView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(consumed, goal);
    }

    private void UpdateProteinView()
    {
        float consumed = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.protein);
        float goal = userDataModel.GetUserGoals().proteinGoal;
        proteinView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(consumed, goal);
    }

    private void UpdateFatView()
    {
        float consumed = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.fat);
        float goal = userDataModel.GetUserGoals().fatGoal;
        fatView.GetComponent<ISingleMacroObserver>().UpdateScreenValues(consumed, goal);
    }

}
