﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class CreatedFoodComponentPrefab : MonoBehaviour, IFoodComponent
{
    public Text foodName;
    public Text quantity;
    public Text kcal;

    public Food food;

    GameObject addFoodPage;

    public void AssignProperties(float quantity, Food food)
    {
        this.food = food;
        foodName.text = food.name;
        this.quantity.text = quantity.ToString() + " " + food.unit;
        kcal.text = Convert.ToInt32(food.nutritionalValues[0]).ToString() + "\nkcal";

        addFoodPage = GameObject.Find("AddFoodPage");
        this.GetComponent<Button>().onClick.AddListener(() => GameObject.Find("GameManager").GetComponent<ScreenTransitions>().PageSwipeUp(addFoodPage));
        this.GetComponent<Button>().onClick.AddListener(() => addFoodPage.GetComponent<AddFoodPage>().OnFoodComponentClick(this.food, this.gameObject));
    }
}
