﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EatenCaloriesText : MonoBehaviour, IMacrosObserver
{
    public UserDataModel userDataModel;
    public Text text;

    public void UpdateMacroValues()
    {
        text.text = Convert.ToInt32(userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.calories)) + " kcal gegessen";
    }
}
