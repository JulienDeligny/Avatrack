﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class SearchPage : MonoBehaviour
{
    public static int MAX_NUMBER_OF_ELEMENTS_DISPLAYED = 30;

    public UserDataModel userDataModel;

    public InputField input;
    public Toggle isSpecific;
    public GameObject foodComponentPrefab;
    public GameObject componentList;
    public PremadeFoodDatabase premadeFoodDatabase;

    public TabMan_TopBar userFoodRecordTabs;

    List<Food> result;

    private void Start()
    {
        premadeFoodDatabase.LoadPremadeFoodDatabase();
    }

    public void Search()
    {
        List<Food> foodListDatabase = (isSpecific.isOn) ? premadeFoodDatabase.specificPremadeFoodDatabase : premadeFoodDatabase.generalPremadeFoodDatabase;
        List<Food> createdFoodLlist = userDataModel.GetUserFoodRecord().createdFoodList.foodList;
        List<Food> foodList = foodListDatabase.Concat(createdFoodLlist).ToList();
        int length = input.text.Length;
        result = new List<Food>();
        foreach(Food food in foodList)
        {
            if (length <= food.name.Length && food.name.ToLower().Contains(input.text.ToLower())) 
            {
                result.Add(food);
            }
        }

        EmptyList();
        PrintResultList();
    }

    void PrintResultList()
    {
        int length = result.Count < MAX_NUMBER_OF_ELEMENTS_DISPLAYED ? result.Count : MAX_NUMBER_OF_ELEMENTS_DISPLAYED;
        for (int i = 0; i < length; i++)
        {
            GameObject newFoodObject = Instantiate(foodComponentPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
            newFoodObject.transform.SetParent(componentList.transform, false);
            newFoodObject.GetComponent<IFoodComponent>().AssignProperties(result[i].defaultQuantity, result[i]);
        }
    } 

    void EmptyList()
    {
        foreach(Transform child in componentList.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void OnExitPage()
    {
        EmptyList();
        input.text = "";
        userFoodRecordTabs.UpdateSelectedFoodPage();
    }

}
