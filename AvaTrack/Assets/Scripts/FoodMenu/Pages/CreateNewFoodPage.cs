﻿using UnityEngine;
using UnityEngine.UI;

public class CreateNewFoodPage : InputPage
{
    public UserDataModel userDataModel;
    public ScreenTransitions transition;

    public InputField foodName;
    public InputField defaultQuantity;
    public Text unit;
    public InputField Kilokalorien;
    public InputField Kohlenhydrate;
    public InputField Eiweiß;
    public InputField Fett;
    public Text api_information;

    public void OnPageOpen()
    {
        api_information.text = "";
        foodName.text = "";
        defaultQuantity.text = "";
        unit.text = "Gramm";
        Kilokalorien.text = "";
        Kohlenhydrate.text = "";
        Eiweiß.text = "";
        Fett.text = "";
    }

    public override void OnSubmit()
    {
        isValid = true;

        Food food = new Food();
        food.name = foodName.text;

        food.defaultQuantity = CheckInputPositiveFloat(defaultQuantity);
        
        food.unit = unit.text;
        food.nutritionalValues[0] = CheckInputPositiveFloat(Kilokalorien);
        food.nutritionalValues[1] = CheckInputPositiveFloat(Kohlenhydrate);
        food.nutritionalValues[2] = CheckInputPositiveFloat(Eiweiß);
        food.nutritionalValues[3] = CheckInputPositiveFloat(Fett);
        food.frequence = 0;

        if (isValid)
        {
            food.id = CreateUID();
            food.isDeletable = true;
            userDataModel.AddToCreatedFoodList(food);
            transition.PageSwipeDown(gameObject);
        }
        CreateUID();
    }

    //for testing purposes
    public void addPerfectFood()
    {
        Food food = new Food();
        food.name = "Lembasbrot";
        food.defaultQuantity = 100;
        food.unit = "Gramm";
        Goals userGoals = userDataModel.GetUserGoals();
        float[] arr = { userGoals.calorieGoal, userGoals.carbohydrateGoal, userGoals.proteinGoal, userGoals.fatGoal };
        food.nutritionalValues = arr;
        food.id = CreateUID();
        food.isDeletable = true;
        userDataModel.AddToCreatedFoodList(food);
        CreateUID();
    }

    string CreateUID()
    {
        string time = System.DateTime.UtcNow.ToLocalTime().ToString("ddMMyyHHmmss");
        return time = time + Random.Range(0, 100).ToString();
    }
}
