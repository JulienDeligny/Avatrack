﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class EditFoodPage : InputPage
{
    public ScreenTransitions transitionsScript;
    public MealPage mealPageScript;

    //reference to objects needed to add food to eaten food list 
    public UserDataModel userDataModel;
    public Button hinzufuegenButton;
    public InputField inputField;
    public Meal meal;

    //reference to view objects that need to be informed
    public Text foodName;
    public Text unitText;
    public Text[] nutritionalValues;
    public Text defaultQuantity;
    public FoodSuccessfullyAdded successfullyAddedPanel;

    //corresponding foodComponent
    Food food;
    GameObject foodComponent;

    public void OnFoodComponentClick(Food food, GameObject foodComponent)
    {
        this.food = food;
        this.foodComponent = foodComponent;
        foodName.text = food.name;
        string unit;
        if (food.unit == "Gramm") {
            unit = "g";
        } else
        {
            unit = "ml";
        }
        this.unitText.text = unit;
        defaultQuantity.text = "Nährwerte für " + Convert.ToInt32(food.defaultQuantity).ToString() + " " + unit + " :";
        inputField.text = food.quantity.ToString();

        for(int i = 0; i < 4; i++)
        {
            nutritionalValues[i].text = Convert.ToInt32(food.nutritionalValues[i]).ToString();
        }
    }   
    
    public override void OnSubmit()
    {
        isValid = true;

        float quantity = CheckInputPositiveFloat(inputField);
        if (isValid)
        {
            userDataModel.ModifyFood(this.food, meal, quantity);
            transitionsScript.PageSwipeDown(gameObject);
            mealPageScript.UpdateMealPage();
            successfullyAddedPanel.Show(false);

        }
    }

    public void OnDeletePressed()
    {
        userDataModel.DeleteFromEatenFoodList(this.food, meal, foodComponent);
    }
}
