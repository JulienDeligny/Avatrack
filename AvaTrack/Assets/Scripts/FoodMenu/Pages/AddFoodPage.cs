﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class AddFoodPage : InputPage
{
    //reference to objects needed to add food to eaten food list 
    public UserDataModel userDataModel;
    public Button hinzufuegenButton;
    public GameObject deleteButton;
    public Meal meal;
    public TabMan_TopBar userFoodRecordTabMan;

    //reference to view objects that need to be informed
    public ScreenTransitions transitions;
    public Text foodName;
    public Text unitText;
    public Text[] nutritionalValues;
    public Text defaultQuantity;
    public FoodSuccessfullyAdded successfullyAddedPanel;
    public InputField inputField;

    //corresponding foodComponent
    Food food;
    GameObject foodComponent;

    public void OnFoodComponentClick(Food food, GameObject foodComponent)
    {
        this.food = food;
        this.foodComponent = foodComponent;
        foodName.text = food.name;
        if (food.isDeletable)
        {
            deleteButton.SetActive(true);
        } else
        {
            deleteButton.SetActive(false);
        }
        string unit;
        if (food.unit == "Gramm")
        {
            unit = "g";
        }
        else
        {
            unit = "ml";
        }
        this.unitText.text = unit;
        defaultQuantity.text = "Nährwerte für " + food.defaultQuantity.ToString() + " " + unit + " :";

        inputField.text = "";
        for (int i = 0; i < 4; i++)
        {
            nutritionalValues[i].text = Convert.ToInt32(food.nutritionalValues[i]).ToString();
        }
    }

    public override void OnSubmit()
    {
        isValid = true;
        float quantity = CheckInputPositiveFloat(floatInputFields[0]);
        if (isValid)
        {
            userFoodRecordTabMan.UpdateSelectedFoodPage();
            food.quantity = quantity;
            transitions.PageSwipeDown(this.gameObject);
            userDataModel.AddToConsumedFoodList(food, meal);
            successfullyAddedPanel.Show(true);
        }
    }

    public void OnDeletePressed()
    {
        userDataModel.DeleteCreatedFood(food, foodComponent);
    }
}
