﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodMenuPages : MonoBehaviour
{
    //This class makes sure that only the selected page displays the foodComponents.
    //Displaying all lists at a time slows down the frame rate. 

    public List<FoodPage> pages;
    public FoodPage activeFoodPage;

    public void Subscribe(FoodPage page)
    {
        if (pages == null)
        {
            pages = new List<FoodPage>();
        }
        pages.Add(page);
    }

    public void OnPageEnter(FoodPage page)
    {
        ResetPages();
        activeFoodPage = page;
        page.OnPageOpen();
    }

    public void UpdateActiveFoodPage()
    {
        activeFoodPage.OnPageOpen();
    }

    public void ResetPages()
    {
        foreach (FoodPage page in pages)
        {
            page.OnPageClose();
        }
    }
}
