﻿using UnityEngine;

public class MealPage : MonoBehaviour
{
    public AddFoodPage addFoodPageScript;
    public UserDataModel userDataModel;
    public GameObject foodList;

    public MealComponent[] mealComponents;
    //0 -> Breakfast, 1 -> Lunch, 2 -> Dinner

    public void UpdateMealPage()
    {
        if (addFoodPageScript.meal.Equals(Meal.breakfast))
        {
            mealComponents[0].OnClick();
        } else if (addFoodPageScript.meal.Equals(Meal.lunch))
        {
            mealComponents[1].OnClick();
        } else
        {
            mealComponents[2].OnClick();
        }
    }

    public void DeleteFoodList()
    {
        userDataModel.ClearConsumedFoodList(addFoodPageScript.meal);
        foreach (Transform child in foodList.transform)
        {
            Destroy(child.gameObject);
        }
        UpdateMealPage();
    }
}
        
       
