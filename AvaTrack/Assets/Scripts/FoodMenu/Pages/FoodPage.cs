﻿using System.Collections.Generic;
using UnityEngine;

public class FoodPage : FoodListObserver
{
    public FoodMenuPages pagesScript;
    public GameObject componentList;
    public GameObject foodComponentPrefab;

    public bool showDefaultQuantity; 
    //if the food is added as "consumed food", the added quantity is shown, otherwise the default quantity (usually 100 g / ml) is shown.

    void Start()
    {
        pagesScript.Subscribe(this);
    }

    public void OnPageOpen()
    {
        if (showDefaultQuantity)
        {
            foreach (Food food in foodList)
            {
                CreateFoodComponentPrefab(food.defaultQuantity, food);
            }
        } else 
        {
            foreach (Food food in foodList)
            {
                CreateFoodComponentPrefab(food.quantity, food);
            }
        }
    }

    public void OnPageClose()
    {
        foreach (Transform child in componentList.transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void CreateFoodComponentPrefab(float quantity, Food food)
    {
        GameObject newFoodObject = Instantiate(foodComponentPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
        newFoodObject.transform.SetParent(componentList.transform, false);
        newFoodObject.GetComponent<IFoodComponent>().AssignProperties(food.defaultQuantity, food);
    }

    public override void UpdateList(List<Food> foodList)
    {
        base.UpdateList(foodList);
        if (pagesScript.activeFoodPage != null)
        {
            if (pagesScript.activeFoodPage.name.Equals(gameObject.name))
            {
                pagesScript.OnPageEnter(this);
            }
        }
    }
        
}
