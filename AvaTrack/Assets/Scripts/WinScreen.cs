﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreen : MonoBehaviour
{
    public GameObject MainCanvas;
    public ParticleSystem particles;
    public Avatar avatar;

    public void ActivateWinScreen()
    {
        avatar.PlayWinAnimation();
        MainCanvas.SetActive(false);
        this.gameObject.SetActive(true);
        particles.Play();
    }

    public void DisactivateWinScreen()
    {
        avatar.StopWinAnimation();
        MainCanvas.SetActive(true);
        this.gameObject.SetActive(false);
        particles.Stop();
    }
}
