﻿using UnityEngine;

public class PlayParticleSystem : MonoBehaviour
{
    public ParticleSystem particle;

    public void OnPressed()
    {
            particle.Clear();
            particle.Play();
    }
}
