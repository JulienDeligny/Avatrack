﻿using UnityEngine;

public class HotWeatherWarning : MonoBehaviour, IWeatherAndLocationObserver
{
    public UserDataModel userDataModel;

    public void UpdateWeatherAndLocation()
    {
        float temperature = userDataModel.GetTemperature();
        if (userDataModel.GetUserSettings().isWeatherFunctionEnabled && temperature >= 30) 
        {
            gameObject.SetActive(true);
        } else
        {
            gameObject.SetActive(false);
        }
    }
}
