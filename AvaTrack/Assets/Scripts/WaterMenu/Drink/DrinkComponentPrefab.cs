﻿using UnityEngine;
using UnityEngine.UI;

public class DrinkComponentPrefab : MonoBehaviour
{
    public Text quantity;
    public Text time;
    public Sprite[] sprites;
    public Image image;
    public Color color;
    public Button deleteButton;

    public Drink drink;

    UserDataModel userDataModel;

    public void AssignProperties(Drink drink)
    {
        this.drink = drink;
        this.quantity.text = drink.quantity.ToString() + " ml";
        time.text ="um " + drink.time;

        if(drink.quantity <= 150)
        {
            image.sprite = sprites[0];
            image.color = color;
        } else if (150 < drink.quantity && drink.quantity <= 350)
        {
            image.sprite = sprites[1];
            image.color = color;
        }
        else if (350 < drink.quantity)
        {
            image.sprite = sprites[2];
            image.color = color;
        }

        userDataModel = GameObject.FindObjectOfType<UserDataModel>();
        deleteButton.onClick.AddListener(() => userDataModel.DeleteFromWaterList(this.drink, this.gameObject));
    }
}
