﻿using UnityEngine;
using UnityEngine.UI;

public class WaterLevel : MonoBehaviour, IHydrationLevelObserver
{
    public UserDataModel userDataModel;

    private static float REDUCTION_OF_INTERVAL = 0.11f;
    //[0 , 1] -> [0.11 , 0.89] for the round progression bar

    public Image progressBar;
    public Text waterCounter;
    public Sprite arrowDown;
    public Sprite arrowUp;
    public Sprite checkMark;
    public Image progressionIndicator;
    public Color lightRed;
    public Color lightGreen;

    private float goal;
    private float waterLevel;
    
    public void UpdateHydrationLevel()
    {
        this.goal = userDataModel.GetUserGoals().hydrationGoal;
        this.waterLevel = userDataModel.GetHydrationLevel().currentHydrationLevel;

        progressBar.fillAmount = CalculateFillAmount();
        waterCounter.text = RoundAndFormatRemainingWater();
        float currentHydrationLevel = userDataModel.GetHydrationLevel().currentHydrationLevel;
        Goals userGoals = userDataModel.GetUserGoals();

        SetProgessIndicator(currentHydrationLevel);
    }

    private void SetProgessIndicator(float currentHydrationLevel)
    {
        int waterConsumptionIndex = userDataModel.GetUserGoals().CheckCurrentWaterGoal(currentHydrationLevel);
        if (waterConsumptionIndex < 0)
        {
            progressionIndicator.color = lightRed;
            progressionIndicator.sprite = arrowDown;
        }
        else if (waterConsumptionIndex == 0)
        {
            progressionIndicator.color = lightGreen;
            progressionIndicator.sprite = checkMark;
        }
        else
        {
            progressionIndicator.color = lightRed;
            progressionIndicator.sprite = arrowUp;
        }
    }

    private float CalculateFillAmount()
    {
        float basis = goal / ((1 - REDUCTION_OF_INTERVAL) - REDUCTION_OF_INTERVAL);
        return (waterLevel / basis) + REDUCTION_OF_INTERVAL;
    }

    private string RoundAndFormatRemainingWater()
    {
        return Mathf.Floor(goal - waterLevel).ToString();
    }
}
