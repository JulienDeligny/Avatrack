﻿using UnityEngine;
using UnityEngine.UI;

public class WaterMenuMainPage : InputPage, IHydrationLevelObserver
{
    public UserDataModel userDataModel;

    public GameObject DrinkList;
    public GameObject DrinkComponentPrefab;

    public InputField quantity;

    public bool isPageActive = false;

    public override void OnSubmit()
    {
        isValid = true;

        float quantity = CheckInputPositiveInt(this.quantity);

        if (isValid)
        {
            AddDrink(quantity);
        }
    }

    public void AddDrink(float quantity)
    {
        //Also replaces constructor of Drink since drink class is not allowed to have a constructor for serialization 
        Drink drink = new Drink();
        drink.quantity = quantity;
        drink.time = System.DateTime.UtcNow.ToLocalTime().ToString("HH:mm");
        drink.id = CreateUID();
        userDataModel.AddDrink(drink);
    }

    public void UpdateView(bool isSelected)
    {
        isPageActive = isSelected;
        UpdateHydrationLevel();
    }

    public void UpdateHydrationLevel()
    {
        if (isPageActive)
        {
            ClearComponents();
            PrintList();
        } else
        {
            ClearComponents();
        }
    }

    public void PrintList()
    {
        foreach (Drink drink in userDataModel.GetHydrationLevel().drinks)
        {
            GameObject newFoodObject = Instantiate(DrinkComponentPrefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0)) as GameObject;
            newFoodObject.transform.SetParent(DrinkList.transform, false);
            newFoodObject.GetComponent<DrinkComponentPrefab>().AssignProperties(drink);
        }
    }

    public void ClearComponents()
    {
        foreach(Transform child in DrinkList.transform)
        {
            Destroy(child.gameObject);
        }
    }

    string CreateUID()
    {
        string time = System.DateTime.UtcNow.ToLocalTime().ToString("ddMMyyHHmmss");
        return time = time + Random.Range(0, 100).ToString();
    }
}
