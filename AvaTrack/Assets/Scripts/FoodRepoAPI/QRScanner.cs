﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using ZXing;

//Activate the camera of the device and using the ZXing library to read the bar code
public class QRScanner : MonoBehaviour
{
    public FoodRepoAPI api;

    private WebCamTexture backCam;
    private Texture defaultBackground;
    private bool camAvailable;

    public GameObject exitButton; 
    public GameObject backgroundObj;
    public RawImage background;
    public AspectRatioFitter fit;

    public void ActivateCamera()
    {
        gameObject.SetActive(true);
        backgroundObj.SetActive(true);
        exitButton.SetActive(true);

        defaultBackground = background.texture;
        //check if there are cameras to use.
        WebCamDevice[] devices = WebCamTexture.devices;
        if (devices.Length == 0)
        {
            Debug.Log("no camera available.");
            StartCoroutine(Disactivate());
            camAvailable = false;
        }

        //setup camera.
        backCam = new WebCamTexture();
        backCam.requestedHeight = Screen.height;
        backCam.requestedWidth = Screen.width;
        if (backCam != null)
        {
            backCam.Play();
            background.texture = backCam;

            camAvailable = true;
        } else camAvailable = false;
    }

    void Update()
    {

        if (!camAvailable) return;

        float ratio = (float)backCam.width / (float)backCam.height;
        fit.aspectRatio = ratio;

        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        fit.aspectRatio = ratio;

        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(backCam.GetPixels32(), backCam.width, backCam.height);
            if (result != null)
            {
                api.MakeHTTPRequest(result.Text);
                Debug.Log(result.Text);
                StartCoroutine(Disactivate());
            }
        } catch (Exception ex) { 
            Debug.LogWarning(ex.Message); 
        }
    }

    //this fuction is bound to the exit button. 
    public void CallDisactivate()
    {
        StartCoroutine(Disactivate());
    }

    IEnumerator Disactivate()
    {
        yield return new WaitForEndOfFrame();
        gameObject.SetActive(false);
        backgroundObj.SetActive(false);
        exitButton.SetActive(false);
        backCam.Stop();
    }
}
