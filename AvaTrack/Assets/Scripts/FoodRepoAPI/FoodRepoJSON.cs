﻿    using System;
    using Newtonsoft.Json;

    //Class correponding to the JSON response of the FoodRepo API call.
    [System.Serializable]
    public partial class FoodRepoJsonResponse
    {
        [JsonProperty("data")]
        public Data[] Data { get; set; }

        [JsonProperty("links")]
        public Links Links { get; set; }

        [JsonProperty("meta")]
        public Meta Meta { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("name_translations")]
        public Translations NameTranslations { get; set; }

        [JsonProperty("display_name_translations")]
        public Translations DisplayNameTranslations { get; set; }

        [JsonProperty("ingredients_translations")]
        public Translations IngredientsTranslations { get; set; }

        [JsonProperty("origin_translations")]
        public Translations OriginTranslations { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("portion_quantity")]
        public long PortionQuantity { get; set; }

        [JsonProperty("portion_unit")]
        public string PortionUnit { get; set; }

        [JsonProperty("alcohol_by_volume")]
        public long AlcoholByVolume { get; set; }

        [JsonProperty("nutrients")]
        public Nutrients Nutrients { get; set; }

        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
    }

    public partial class Translations
    {
        [JsonProperty("de")]
        public string De { get; set; }

        [JsonProperty("fr")]
        public string Fr { get; set; }

        [JsonProperty("it")]
        public string It { get; set; }

        [JsonProperty("en", NullValueHandling = NullValueHandling.Ignore)]
        public string En { get; set; }
    }

    public partial class Nutrients
    {
        [JsonProperty("salt")]
        public Substance Salt { get; set; }

        [JsonProperty("protein")]
        public Substance Protein { get; set; }
    
        [JsonProperty("fiber")]
        public Substance Fiber { get; set; }

        [JsonProperty("sugars")]
        public Substance Sugars { get; set; }

        [JsonProperty("carbohydrates")]
        public Substance Carbohydrates { get; set; }

        [JsonProperty("saturated_fat")]
        public Substance SaturatedFat { get; set; }

        [JsonProperty("fat")]
        public Substance Fat { get; set; }

        [JsonProperty("energy_kcal")]
        public Substance EnergyKcal { get; set; }

        [JsonProperty("energy")]
        public Substance Energy { get; set; }
    }

    public partial class Substance
    {
        [JsonProperty("name_translations")]
        public Translations NameTranslations { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }

        [JsonProperty("per_hundred")]
        public double? PerHundred { get; set; }

        [JsonProperty("per_portion")]
        public double? PerPortion { get; set; }

        [JsonProperty("per_day")]
        public double? PerDay { get; set; }
    }

    public partial class Links
    {
    }

    public partial class Meta
    {
        [JsonProperty("api_version")]
        public string ApiVersion { get; set; }

        [JsonProperty("generated_in")]
        public long GeneratedIn { get; set; }
    }
