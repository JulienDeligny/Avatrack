﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;

public class FoodRepoAPI : MonoBehaviour
{
    [SerializeField]
    private static string API_KEY = "e42639b5f421e8786527ee8fabbceb08";

    public InputField[] FoodInfo_InputFields;
    public Text Unit;

    public Text API_CallFeedback;
    public Color positiveColor;

    public void MakeHTTPRequest(string barcode)
    {
        ClearInputFields();

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            API_CallFeedback.text = "Keine Internet Verbindung.";
            return;
        }

        string foodRepoUrl = "https://www.foodrepo.org/api/v3/products?excludes=images%2C&barcodes=" + barcode + "%2C";
        try
        {
            var webRequest = System.Net.WebRequest.Create(foodRepoUrl);
            if (webRequest != null)
            {
                webRequest.Method = "GET";
                webRequest.Timeout = 12000;
                webRequest.ContentType = "application/json";
                string apiToken = "Token " + API_KEY;
                webRequest.Headers.Add("Authorization", apiToken);

                using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                    {
                        var jsonResponse = sr.ReadToEnd();
                        ExtractFoodInfo(jsonResponse);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log (ex);
        }
    }

    private void ClearInputFields()
    {
        foreach (InputField elem in FoodInfo_InputFields)
        {
            elem.text = "";
        }
    }

    private void ExtractFoodInfo(string jsonResponse)
    {
        var data = JsonConvert.DeserializeObject<FoodRepoJsonResponse>(jsonResponse);
        if (data.Data.Length == 0)
        {
            API_CallFeedback.color = Color.red;
            API_CallFeedback.text = "Produkt Nicht verfügbar";
        }
        else
        {
            API_CallFeedback.color = positiveColor;
            API_CallFeedback.text = "Produkt gefunden";

            if (data.Data[0].NameTranslations.De != null) FoodInfo_InputFields[0].text = data.Data[0].NameTranslations.De;
            if (data.Data[0].Nutrients.EnergyKcal != null) FoodInfo_InputFields[1].text = "100";
            if (data.Data[0].Nutrients.EnergyKcal != null) FoodInfo_InputFields[2].text = data.Data[0].Nutrients.EnergyKcal.PerHundred.ToString();
            if (data.Data[0].Nutrients.Carbohydrates != null) FoodInfo_InputFields[3].text = data.Data[0].Nutrients.Carbohydrates.PerHundred.ToString();
            if (data.Data[0].Nutrients.Protein != null) FoodInfo_InputFields[4].text = data.Data[0].Nutrients.Protein.PerHundred.ToString();
            if (data.Data[0].Nutrients.Fat != null) FoodInfo_InputFields[5].text = data.Data[0].Nutrients.Fat.PerHundred.ToString();
        }
    }
}
