﻿using System.Collections;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    public float latitude;
    public float longitude;

    public WeatherManager weatherManager;

    public void GetDeviceLocation()
    {
        StartCoroutine(StartLocationService());
    }

    public IEnumerator StartLocationService()
    {

        if (!Input.location.isEnabledByUser)
        {
            weatherManager.weatherStatusText = "Standort Funktion nicht aktiviert oder für die App nicht zugelassen.";
            yield break;
        }

        Input.location.Start(); //this changes the status of LocationServiceStatus to Initilizing
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        if (maxWait <= 0)
        {
            weatherManager.weatherStatusText = "Ermittlung des Standorts hat Zeitgrenze überschritten.";
            yield break;
        }

        if(Input.location.status == LocationServiceStatus.Failed)
        {
            weatherManager.weatherStatusText = "Ermittlung des Standorts fehlgeschlagen.";

            yield break;
        }

        weatherManager.weatherStatusText = "Standort Ermittlung erfolgreich.";
        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        yield break;
    }

    public bool LocationDetectionWorked()
    {
        if (latitude != 0 && longitude != 0)
            return true;
        else
            return false;
    }
}
