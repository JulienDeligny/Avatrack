﻿using UnityEngine;
using UnityEngine.UI;

public class WeatherAndLocationOptionPage : InputPage, IWeatherAndLocationObserver
{
    public Color green;
    public Color red;

    public UserDataModel userDataModel;
    public LocationManager locationManager;
    public WeatherManager weatherManager;

    public GameObject disableFunctionsPanel;
    public Toggle toggleWeatherOption;

    public InputField cityInputField;
    public Text statusText;

    public Toggle[] chooseLocationMethod;

    public void OnPageOpen()
    {
        toggleWeatherOption.isOn = userDataModel.GetUserSettings().isWeatherFunctionEnabled;

        if (userDataModel.GetUserSettings().isWeatherFunctionEnabled && userDataModel.GetUserSettings().isLocationFunctionEnabled)
        {
            chooseLocationMethod[0].isOn = true;
            chooseLocationMethod[1].isOn = false;
        } else if (userDataModel.GetUserSettings().isWeatherFunctionEnabled && userDataModel.GetUserSettings().isLocationFunctionEnabled)
        {
            chooseLocationMethod[0].isOn = false;
            chooseLocationMethod[1].isOn = true;
        } else
        {
            chooseLocationMethod[0].isOn = false;
            chooseLocationMethod[1].isOn = false;
        }
    }

    public void EnableWeatherOptions()
    {
        if(toggleWeatherOption.isOn)
        {
            disableFunctionsPanel.SetActive(false);
        } else
        {
            disableFunctionsPanel.SetActive(true);
        }
    }

    public void EnableLocationOptions()
    {
        if (chooseLocationMethod[0].isOn)
        {
            cityInputField.interactable = false; //city is determined by the location of the device
        }
        else
        {
            cityInputField.interactable = true;
        }
    }

    public override void OnSubmit()
    {
        isValid = true;

        int index = getIndexOfToggled(chooseLocationMethod, false);

        if (!toggleWeatherOption.isOn)
        {
            userDataModel.SetWeatherAndLocationData(false, false);
        }
        else
        {
            if (index == 1)
            {
                userDataModel.SetWeatherAndLocationData(true, true);
            } else if (index == 2)
            {
                userDataModel.GetUserSettings().locationName = cityInputField.text;
                userDataModel.SetWeatherAndLocationData(true, false);
            }
        }
    }

    public void UpdateWeatherAndLocation()
    {
        statusText.color = green;
        if (userDataModel.GetUserSettings().isWeatherFunctionEnabled)
        {
            bool localizationFailed = (userDataModel.GetUserSettings().isLocationFunctionEnabled && !locationManager.LocationDetectionWorked());
            bool fetchWeatherDataFailed = (!userDataModel.GetUserSettings().isLocationFunctionEnabled && !weatherManager.weatherAPICallWorked);
            if (localizationFailed || fetchWeatherDataFailed)
            {
                statusText.color = red;
            } else
            {
                cityInputField.text = weatherManager.locationName;
            }
        }
        statusText.text = weatherManager.weatherStatusText;
    }

    
}
