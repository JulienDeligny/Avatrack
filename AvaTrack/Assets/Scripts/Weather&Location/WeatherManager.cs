﻿using Newtonsoft.Json;
using System.IO;
using System;
using System.Net;
using UnityEngine;

public class WeatherManager : MonoBehaviour
{
    public string apiKey = "3fb55b2e68ccc04ec30d6ee11f4e6907";

    public LocationManager locationManager;
    public bool weatherAPICallWorked;

    public string locationName;
    public string temperature;
    public string description;
    public string weatherStatusText;

    public void FetchWeatherData(bool searchWithCoord)
    {
        weatherAPICallWorked = false; //will be set to TRUE if the call was successful

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            weatherStatusText = "Keine Internet Verbindung.";
            return;
        }

        if (searchWithCoord)
        {
            GetWeatherBasedOnDeviceLocation();
        }
        else
        {
            GetWeatherBasedOnUserInput();
        }
    }

    private void GetWeatherBasedOnDeviceLocation()
    {
        locationManager.GetDeviceLocation();
        if (locationManager.LocationDetectionWorked())
        {
            try
            {
                string url = "http://api.openweathermap.org/data/2.5/weather?lat=" + locationManager.latitude + "&lon=" + locationManager.longitude + "&appid=" + apiKey + "&units=metric&lang=de";
                string jsonResponse = MakeApiCall(url);
                ExtractInfo(jsonResponse);
                weatherStatusText = "Suche erfolgreich.";
            }
            catch
            {
                weatherStatusText = "Wetterdaten für Standort nicht gefunden.";
            }
        }
    }

    private void GetWeatherBasedOnUserInput()
    {
        try
        {
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + locationName + "&appid=" + apiKey + "&units=metric&lang=de";
            string jsonResponse = MakeApiCall(url);
            ExtractInfo(jsonResponse);
            weatherStatusText = "Suche erfolgreich.";
        }
        catch
        {
            weatherStatusText = "Der von Ihnen eingegebene Stadtname steht leider nicht zur Verfügung.";
        }
    }

    private string MakeApiCall(string url)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        weatherAPICallWorked = true;
        return reader.ReadToEnd();
    }

    private void ExtractInfo(string jsonResponse)
    {
        var data = JsonConvert.DeserializeObject<Temperatures>(jsonResponse);
        locationName = data.Name;
        temperature = Convert.ToInt32(data.Main.Temp).ToString() + "°";
        description = data.Weather[0].Description;
    }

    public float GetTemperature()
    {
        string tmp;
        if (temperature != "")
        {
            tmp = temperature.TrimEnd('°');
            return float.Parse(tmp);
        }
        else
        {
            return 10;
        }
    }
}
