﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeatherPanel : MonoBehaviour, IWeatherAndLocationObserver
{
    public UserDataModel userDataModel;

    public WeatherManager weatherManager;
    public Text locationName;
    public Text temperature;
    public Text description;


    public void UpdateWeatherAndLocation()
    {
        if (userDataModel.GetUserSettings().isWeatherFunctionEnabled) 
            gameObject.SetActive(true);
        else 
            gameObject.SetActive(false);
        locationName.text = weatherManager.locationName;
        temperature.text = weatherManager.temperature;
        description.text = weatherManager.description;
    }
}
