﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControlsFoodMenu : SwipeControls
{
    public TabMan_TopBar tabMan;

    public List<TabButton> tabButtons;

    public override void SwipeDown()
    {
    }
    public override void SwipeUp()
    {
    }

    public override void SwipeLeft()
    {
        int indexOfActiveTab = tabMan.selectedTab.transform.GetSiblingIndex();
        if (indexOfActiveTab < tabMan.pages.Count - 1)
        {
            tabMan.pages[indexOfActiveTab].GetComponent<FoodPage>().OnPageClose();
            tabMan.OnTabSelected(tabButtons[indexOfActiveTab + 1]);
            tabMan.pages[indexOfActiveTab + 1].GetComponent<FoodPage>().OnPageOpen();
        }
    }

    public override void SwipeRight()
    {
        int indexOfActiveTab = tabMan.selectedTab.transform.GetSiblingIndex();
        if (0 < indexOfActiveTab)
        {
            tabMan.pages[indexOfActiveTab].GetComponent<FoodPage>().OnPageClose();
            tabMan.OnTabSelected(tabButtons[indexOfActiveTab - 1]);
            tabMan.pages[indexOfActiveTab - 1].GetComponent<FoodPage>().OnPageOpen();
        }
    }

}
