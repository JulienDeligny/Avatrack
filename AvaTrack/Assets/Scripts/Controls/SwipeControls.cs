﻿using UnityEngine;

public abstract class SwipeControls : MonoBehaviour
{
    private static int DEADZONE = 150;

    public UserDataModel userDataModel;

    //Only one page can use the swipe controls at a time.
    //Swipe controls get deactivated whenever a page is opened
    //that does not use them.
    //The SceenTansitions script notifies the scripts 
    //implementing SwipeControls.
    public GameObject pageUsingSwipeControls;

    private bool isDraging = false;
    private Vector2 startTouch, swipeDelta;

    private void Update()
    {

        CheckStandaloneInput(); //for PC 
        CheckMobileInput();

        CalculateSwipedDistance();
        // Did we cross the deadzone?
        if (swipeDelta.magnitude > DEADZONE)
        {
            DetermineDirection();
        }
    }
    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }


    private void CheckStandaloneInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isDraging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            isDraging = false;
            Reset();
        }
    }

    private void CheckMobileInput()
    {
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                isDraging = true;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;
                Reset();
            }
        }
    }

    private void CalculateSwipedDistance()
    {
        swipeDelta = Vector2.zero;
        if (isDraging)
        {
            if (Input.touches.Length > 0)
                swipeDelta = Input.touches[0].position - startTouch;
            else if (Input.GetMouseButton(0))
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
        }
    }

    private void DetermineDirection()
    {
        float x = swipeDelta.x;
        float y = swipeDelta.y;
        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (x < 0)
            {
                SwipeLeft();
            }
            else
            {
                SwipeRight();
            }
        }
        else
        {
            if (y < 0)
            {
                SwipeDown();
            }
            else
            {
                SwipeUp();
            }
        }
        Reset();
    }

    public abstract void SwipeLeft();
    public abstract void SwipeRight();
    public abstract void SwipeDown();
    public abstract void SwipeUp();

    public void UpdateSwipeControls(GameObject activeSwipeControlObject)
    {
        if (activeSwipeControlObject != pageUsingSwipeControls) this.enabled = false;
        else this.enabled = true;
    }
}
