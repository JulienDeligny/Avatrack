﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControlsMainMenu : SwipeControls
{
    public TabMan_MainMenu tabManMainMenu;

    public List<TabButton> tabButtons;

    public override void SwipeUp()
    {
    }
    public override void SwipeDown()
    {
    }

    public override void SwipeLeft()
    {
        int indexOfActiveTab = tabManMainMenu.selectedTab.transform.GetSiblingIndex();
        if (indexOfActiveTab < tabManMainMenu.pages.Count-1) tabManMainMenu.OnTabSelected(tabButtons[indexOfActiveTab + 1]);
    }

    public override void SwipeRight()
    {
        int indexOfActiveTab = tabManMainMenu.selectedTab.transform.GetSiblingIndex();
        if (0 < indexOfActiveTab) tabManMainMenu.OnTabSelected(tabButtons[indexOfActiveTab - 1]);
    }

}
