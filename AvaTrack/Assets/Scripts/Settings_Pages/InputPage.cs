﻿using UnityEngine;
using UnityEngine.UI;

public abstract class InputPage : MonoBehaviour
{
    public InputField[] intInputFields;
    public InputField[] floatInputFields;
    public ToggleGroup[] toggleGroup;

    public bool isValid;
    public Color lightRed;

    public abstract void OnSubmit();

    public int CheckInputPositiveInt(InputField input)
    {
        int output;
        if (!int.TryParse(input.text, out output) || output < 0)
        {
            isValid = false;
            input.image.color = lightRed;
        } else
        {
            input.image.color = Color.white;
        }
        return output;
    }

    public float CheckInputPositiveFloat(InputField input)
    {
        float output;
        if (!(float.TryParse(input.text, out output)) || output < 0)
        {
            isValid = false;
            input.image.color = lightRed;
        } else
        {
            input.image.color = Color.white;
        }
        return output;
    }

    public int getIndexOfToggled(Toggle[] toggleGroup, bool allowNoSelection)
    {
        bool atLeastOneEnabled = allowNoSelection;
        int index = 0;
        foreach (Toggle elem in toggleGroup)
        {
            if (elem.isOn)
            {
                index = elem.transform.parent.GetSiblingIndex();
                atLeastOneEnabled = true;
                break;
            }
        }
        isValid = isValid && atLeastOneEnabled;
        return index;
    }
}
