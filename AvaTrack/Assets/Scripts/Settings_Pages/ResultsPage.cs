﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ResultsPage : MonoBehaviour, IMacrosObserver, IHydrationLevelObserver
{
    public Text waterGoal;
    public Text foodGoal;
    public Text minMaxDeviation;
    public GameObject questionnaireTitle;
    public Text questionnaireResults;

    public UserDataModel userDataModel;

    public void UpdateHydrationLevel()
    {
        waterGoal.text = userDataModel.GetUserGoals().hydrationGoal.ToString() + " ml";
    }

    public void UpdateMacroValues()
    {
        UpdateHydrationLevel();
        Goals goals = userDataModel.GetUserGoals();

        foodGoal.text = "\nKalorien: " + Convert.ToInt32(goals.calorieGoal) + " kcal\n" +
            "Kohlenhydrate: " + Convert.ToInt32(goals.carbohydrateGoal) + " g\n" +
            "Eiweiß: " + Convert.ToInt32(goals.proteinGoal) + " g\n" +
            "Fett:" + Convert.ToInt32(goals.fatGoal) + " g\n";
        if (goals.customNutritionGoalsActivated)
        {
            questionnaireTitle.SetActive(false);
            questionnaireResults.gameObject.SetActive(false);
        } else
        {
            questionnaireTitle.SetActive(true);
            questionnaireResults.gameObject.SetActive(true);
            float PAL = goals.CalculatePAL();
            questionnaireResults.text = "Leistungsumsatz (PAL): " + PAL +
            "\nGrundumsatz (BMR): " + Convert.ToInt32(userDataModel.GetUserGoals().CalculateBMR()) + " kcal";
        }

        minMaxDeviation.text = "Min / Max Kalorien: \n " +
            "             " + Convert.ToInt32(goals.CalculateMinMaxCalorieGoal().Item1) + " kcal / " + Convert.ToInt32(goals.CalculateMinMaxCalorieGoal().Item2) + " kcal\n" +
            "Min / Max Kohlenhydrate: \n" +
            "             " + Convert.ToInt32(goals.CalculateMinMaxCarbohydrateGoal().Item1) + " g / " + Convert.ToInt32(goals.CalculateMinMaxCarbohydrateGoal().Item2) + " g\n" +
            "Min / Max Eiweiß: \n" +
             "             " + Convert.ToInt32(goals.CalculateMinMaxProteinGoal().Item1) + " g / " + Convert.ToInt32(goals.CalculateMinMaxProteinGoal().Item2) + " g\n" +

            "Min / Max Fett: \n" +
            "             " + Convert.ToInt32(goals.CalculateMinMaxFatGoal().Item1) + " g / " + Convert.ToInt32(goals.CalculateMinMaxFatGoal().Item2) + " g\n";

    }
}
