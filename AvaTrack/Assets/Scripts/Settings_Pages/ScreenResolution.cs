﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenResolution : InputPage
{
    public UserDataModel userDataModel;

    public Toggle[] toggleGroupResolution;

    public void OnPageOpen()
    {
        bool isStretchAspectRationOn = userDataModel.GetUserSettings().isStretchAspectRationOn;
        if (isStretchAspectRationOn)
            toggleGroupResolution[1].isOn = true;
        else
            toggleGroupResolution[0].isOn = true;

    }

    public override void OnSubmit()
    {
        int indexToggled = getIndexOfToggled(toggleGroupResolution, false);
        bool isStretching = (indexToggled == 1) ? false : true;
        userDataModel.AdaptAspectRatio(isStretching);
        Application.Quit();
    }
}
