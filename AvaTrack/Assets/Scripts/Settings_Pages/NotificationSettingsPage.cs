﻿using UnityEngine;
using UnityEngine.UI;

public class NotificationSettingsPage : MonoBehaviour
{
    public UserDataModel userDataModel;

    public Toggle toggleNotificationSettings;

    public void OnPageOpen()
    {
        toggleNotificationSettings.isOn = userDataModel.GetUserSettings().isNotificationEnabled;
    }

    public void AdaptNotificationSettings()
    {
        userDataModel.SetNotificationSettings(toggleNotificationSettings.isOn);
    }
}
