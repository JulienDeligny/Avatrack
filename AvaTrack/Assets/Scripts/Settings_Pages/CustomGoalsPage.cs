﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class CustomGoalsPage : InputPage
{
    public Text Feedback;

    public UserDataModel userDataModel;
    public ScreenTransitions transitions;
    public GameObject goalPage;

    public override void OnSubmit()
    {
        isValid = true;

        float[] checkedInputFields = new float[4];

        for (int i = 0; i < 4; i++) 
        {
            checkedInputFields[i] = CheckInputPositiveFloat(floatInputFields[i]);
        }
        if (isValid)
        {
            userDataModel.SetCustomNutritionGoals(checkedInputFields[0], checkedInputFields[1], checkedInputFields[2], checkedInputFields[3]);

            if (SceneManager.GetActiveScene().name.Equals("MainScene"))
            {
                transitions.PageSwipeUp(goalPage);
            }
        }
    }
}
