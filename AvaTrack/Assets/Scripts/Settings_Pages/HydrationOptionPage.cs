﻿using UnityEngine;
using UnityEngine.UI;

public class HydrationOptionPage : InputPage
{
    public UserDataModel userDataModel;

    public Toggle[] toggleGroupHydration;
    public InputField goalInput;
    public GameObject TogglePanel;
    public Toggle[] toggle_EFSA;

    public void OnPageOpen()
    {
        if (!userDataModel.GetUserGoals().customHydrationGoalsActivated)
        {
            toggleGroupHydration[0].isOn = true;
        }
        else
        {
            toggleGroupHydration[1].isOn = true;
        }
        toggleGroupHydration[0].onValueChanged.AddListener(delegate { UpdateTogglePanel(); });
        toggleGroupHydration[1].onValueChanged.AddListener(delegate { UpdateTogglePanel(); });
        goalInput.text = userDataModel.GetUserGoals().hydrationGoal.ToString();
        UpdateTogglePanel();
    }

    public override void OnSubmit()
    {
        isValid = true;

        int indexOfToggled = getIndexOfToggled(toggleGroupHydration, false);

        if (indexOfToggled == 1 && isValid)
        {
            if (TogglePanel.activeSelf)
                userDataModel.SetHydrationGoal(toggle_EFSA[0].isOn , toggle_EFSA[1].isOn);
            else
                userDataModel.SetHydrationGoal();
        }
        else if (indexOfToggled == 2)
        {
            int goalInMililiters = CheckInputPositiveInt(goalInput);
            if (isValid)
                userDataModel.SetCustomHydrationGoal(goalInMililiters);
        }
    }

    public void UpdateTogglePanel()
    {
        bool isFemale = !userDataModel.GetUserGoals().isMale;
        bool isCustomNutrition = userDataModel.GetUserGoals().customNutritionGoalsActivated;
        bool isCustomHydration = toggleGroupHydration[1].isOn;
        if (isFemale && isCustomNutrition && !isCustomHydration)
            TogglePanel.SetActive(true);
        else
            TogglePanel.SetActive(false);
    }
}
