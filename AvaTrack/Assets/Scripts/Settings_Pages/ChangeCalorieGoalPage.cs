﻿using UnityEngine;

public class ChangeCalorieGoalPage : MonoBehaviour
{
    public UserDataModel userDataModel;
    public TabMan_TopBar tabMan;

    public TabButton questionnaireGoalsPageButton;
    public QuestionnaireGoalsPage questGoalsPageScript;
    public TabButton customGoalsPageButton;

    public void OnPageOpen()
    {
        //Show the page corresponding to the user settings
        if (userDataModel.GetUserGoals().customNutritionGoalsActivated)
        {
            tabMan.OnTabSelected(customGoalsPageButton);
        } else
        {
            tabMan.OnTabSelected(questionnaireGoalsPageButton);
            questGoalsPageScript.OnPageOpen();
        }
    }
}
