﻿using UnityEngine.UI;

public class ChangeBodyData : InputPage
{
    public UserDataModel userDataModel;
    public ScreenTransitions transitions;

    public Toggle[] toggleGender;
    public Text textField;

    public bool SwipeDownAfterValidInput;

    public void OnPageOpen()
    {
        int i = userDataModel.GetUserGoals().isMale ? 1 : 0;
        toggleGender[i].isOn = true;
        intInputFields[0].text = userDataModel.GetUserGoals().age.ToString();
        floatInputFields[0].text = userDataModel.GetUserGoals().weight.ToString();

        if(userDataModel.GetUserGoals().customNutritionGoalsActivated)
        {
            textField.text = "Da Sie benutzerdefinierte Ziele eingestellt haben ändern sich Ihre Ziele nach dem Speichern nicht.";

        } else
        {
            textField.text = "Ihre Ziele werden neu berechnet nachdem sie die Einstellungen speichern.";
        }
    }

    public override void OnSubmit()
    {
        isValid = true;
        
        int age = CheckInputPositiveInt(intInputFields[0]);
        float weight = CheckInputPositiveFloat(floatInputFields[0]);

        int gender = getIndexOfToggled(toggleGender, false);

        bool isMale = gender == 2 ? true : false;

        if (isValid)
        {
            userDataModel.SetUserGoals(age, weight, isMale);
            if (SwipeDownAfterValidInput)
                transitions.PageSwipeDown(this.gameObject);
        }

    }
}
