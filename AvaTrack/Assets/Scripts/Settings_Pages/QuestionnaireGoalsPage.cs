﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class QuestionnaireGoalsPage : InputPage, IGenderObserver
{
    public UserDataModel userDataModel;
    public ScreenTransitions transitions;
    public GameObject goalPage;

    public Toggle[] PAL;
    public Toggle isAthlete;
    public Toggle[] pregnant;
    public Toggle isBreastfeeding;
    public Toggle[] weightGoal;
    public Toggle increaseProteinIntake;

    public GameObject[] onlyFemaleUserQuestions;

    public void OnPageOpen()
    {
        //initiate the page with the user settings
        QuestionnaireData questionnaireData = userDataModel.GetUserGoals().questionnaireData;
        int indexToggleActivityLevel = Convert.ToInt32((questionnaireData.physicalActivityLevel - 1) / 0.2f) - 1;
        PAL[indexToggleActivityLevel].isOn = true;

        isAthlete.isOn = questionnaireData.isAthlete;

        int indexToggleWeightGoal = Convert.ToInt32(questionnaireData.weightGoalFactor + 1);
        weightGoal[indexToggleWeightGoal].isOn = true;

        increaseProteinIntake.isOn = questionnaireData.increaseProteinIntake;

        if(!userDataModel.GetUserGoals().isMale)
        {
            if(questionnaireData.pregnancyFactor > 0)
                pregnant[questionnaireData.pregnancyFactor - 1].isOn = true;
            isBreastfeeding.isOn = questionnaireData.isBreastFeeding;
        }
    }

    public override void OnSubmit()
    {
        isValid = true;

        //Get indices of selected option from the toggle groups.
        int indexToggleActivityLevel = getIndexOfToggled(this.PAL , false);
        int indexTogglePregnancy = getIndexOfToggled(this.pregnant, true);
        int indexToggleWeightGoal = getIndexOfToggled(this.weightGoal, false);

        if (isValid)
        {
            float physicalActivityLevel = 1.2f + (indexToggleActivityLevel - 1) * 0.2f;
            float weightGoalFactor = indexToggleWeightGoal - 2;

            QuestionnaireData questionnaireData = new QuestionnaireData();
            questionnaireData.physicalActivityLevel = physicalActivityLevel;
            questionnaireData.isAthlete = isAthlete.isOn;
            questionnaireData.pregnancyFactor = indexTogglePregnancy;
            questionnaireData.isBreastFeeding = isBreastfeeding.isOn;
            questionnaireData.weightGoalFactor = weightGoalFactor;
            questionnaireData.increaseProteinIntake = increaseProteinIntake.isOn;

            if (SceneManager.GetActiveScene().name.Equals("MainScene"))
            {
                transitions.PageSwipeUp(goalPage);
            }

            userDataModel.SetUserGoals(questionnaireData);
        }
    }

    public void UpdateGender()
    {
        foreach(GameObject elem in onlyFemaleUserQuestions)
        {
            elem.SetActive(!userDataModel.GetUserGoals().isMale);
        }
    }
}
