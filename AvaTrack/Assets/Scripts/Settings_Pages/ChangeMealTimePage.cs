﻿using UnityEngine;

public class ChangeMealTimePage : InputPage, IMealTimeObserver
{
    public UserDataModel userDataModel;

    public void UpdateMealTime()
    {
        for (int i = 0; i < 6; i++)
        {
            intInputFields[i].text = userDataModel.GetUserGoals().mealTime[i].ToString();
        }
    }

    public override void OnSubmit()
    {
        isValid = true;

        //check if input is valid 
        //The numbers have to be in ascending order and cannot surpass 24
        int[] input = new int[6];
        for(int i = 0; i < 6; i++)
        {
            input[i] = CheckInputPositiveInt(intInputFields[i]);
            if (i == 0 && input[0] < 0) isValid = false;
            if (0 < i && (i % 2) == 0 && input[i - 1] > input[i]) isValid = false;
            if (0 < i && (i % 2) != 0 && input[i - 1] >= input[i]) isValid = false;
            if (i == 5 && (input[5] < 0 || input[5] > 24)) isValid = false;
        }
        if (isValid)
        {
            int[] mealTime = new int[6];
            int i = 0;
            foreach (int elem in input)
            {
                mealTime[i] = elem;
                    i ++;
            }
            userDataModel.SetMealTime(mealTime);
        }
    }
}
