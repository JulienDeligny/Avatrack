﻿public interface ISwapable
{
    void ChangeStyle(int direction);
}
