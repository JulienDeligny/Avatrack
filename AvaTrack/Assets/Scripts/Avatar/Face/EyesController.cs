﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesController : ChangebleColor, ISwapable
{
    public GameObject pupils;
    public Sprite blink;
    public Sprite activeEyeSprite;
    public SpriteRenderer l_eye;
    public SpriteRenderer r_eye;
    public GameObject[] eyeGlow;

    public int indexActiveSprite;
    public List<Sprite> eyeStyle;

    public float timeClosed;
    public int minTime;
    public int maxTime;

    void Start()
    {
        StartBlink();
    }

    void StartBlink()
    {
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        yield return new WaitForSeconds(Random.Range(minTime, maxTime));

        pupils.SetActive(false);
        l_eye.sprite = blink;
        r_eye.sprite = blink;

        yield return new WaitForSeconds(timeClosed);

        l_eye.sprite = activeEyeSprite;
        r_eye.sprite = activeEyeSprite;
        pupils.SetActive(true);

        StartBlink();
    }


    public override void ChangeColor(Color color)
    {
        this.color = color;
        pupils.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = color;
        pupils.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = color;
    }

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            indexActiveSprite = direction == 1 ? (indexActiveSprite + 1) % eyeStyle.Count : (indexActiveSprite - 1) % eyeStyle.Count;
            indexActiveSprite = indexActiveSprite == -1 ? eyeStyle.Count - 1 : indexActiveSprite;
        }

        activeEyeSprite = eyeStyle[indexActiveSprite];
        l_eye.sprite = activeEyeSprite;
        r_eye.sprite = activeEyeSprite;
    }

    public void UpdateCondition(bool happy)
    {
        if (happy)
        {
            eyeGlow[0].SetActive(true);
            eyeGlow[1].SetActive(true);
        }
        else
        {
            eyeGlow[0].SetActive(false);
            eyeGlow[1].SetActive(false);
        }
    }
}
