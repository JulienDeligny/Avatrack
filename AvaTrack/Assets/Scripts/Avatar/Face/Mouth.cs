﻿using System.Collections.Generic;
using UnityEngine;

public class Mouth : MonoBehaviour, ISwapable
{
    public int indexActiveSprite;
    public List<Sprite> happyMouthSprites;
    public List<Sprite> unhappyMouthSprites;

    public SpriteRenderer mouth;
    bool isHappy = true;

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            indexActiveSprite = direction == 1 ? (indexActiveSprite + 1) % happyMouthSprites.Count : (indexActiveSprite - 1) % happyMouthSprites.Count;
            indexActiveSprite = indexActiveSprite == -1 ? happyMouthSprites.Count - 1 : indexActiveSprite;
        }
        if (isHappy)
            mouth.sprite = happyMouthSprites[indexActiveSprite];
        else
            mouth.sprite = unhappyMouthSprites[indexActiveSprite];
    }

    public void UpdateCondition(bool happy)
    {
       if (!happy)
        {
            isHappy = false;
            mouth.sprite = unhappyMouthSprites[indexActiveSprite];
        } else
        {
            isHappy = true;
            mouth.sprite = happyMouthSprites[indexActiveSprite];
        }
    }
}
