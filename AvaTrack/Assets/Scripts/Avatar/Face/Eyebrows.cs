﻿using System.Collections.Generic;
using UnityEngine;

public class Eyebrows : MonoBehaviour, ISwapable
{
    public List<SpriteRenderer> eyeBrows;
    public List<Sprite> eyeBrowsStyles;
    public int indexActivePiece = 0;

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            indexActivePiece = direction == 1 ? (indexActivePiece + 1) % eyeBrowsStyles.Count : (indexActivePiece - 1) % eyeBrowsStyles.Count;
            indexActivePiece = indexActivePiece == -1 ? eyeBrowsStyles.Count-1 : indexActivePiece;
        }
        eyeBrows[0].sprite = eyeBrowsStyles[indexActivePiece];
        eyeBrows[1].sprite = eyeBrowsStyles[indexActivePiece];

    }
}
