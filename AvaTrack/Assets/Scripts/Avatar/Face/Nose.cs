﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nose : MonoBehaviour, ISwapable
{
    public SpriteRenderer nose;
    public List<Sprite> noseStyles;
    public int indexActivePiece = 0;

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            indexActivePiece = direction == 1 ? (indexActivePiece + 1) % noseStyles.Count : (indexActivePiece - 1) % noseStyles.Count;
            indexActivePiece = indexActivePiece == -1 ? noseStyles.Count - 1 : indexActivePiece;
        }
        nose.sprite = noseStyles[indexActivePiece];

    }
}
