﻿using System.Collections.Generic;
using UnityEngine;

public class Hair : ChangebleColor, ISwapable
{
    public int indexActiveHairStyle;
    public List<Sprite> hairStyles;
    public SpriteRenderer hair;
    public List<SpriteRenderer> eyeBrows;

    public override void ChangeColor(Color newColor)
    {
        this.color = newColor;
        hair.color = newColor;
        eyeBrows[0].color = newColor;
        eyeBrows[1].color = newColor;

    }

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            indexActiveHairStyle = direction == 1 ? (indexActiveHairStyle + 1) % hairStyles.Count : (indexActiveHairStyle - 1) % hairStyles.Count;
            indexActiveHairStyle = indexActiveHairStyle == -1 ? hairStyles.Count - 1 : indexActiveHairStyle;
        }
        hair.sprite = hairStyles[ indexActiveHairStyle ];
    }
}
