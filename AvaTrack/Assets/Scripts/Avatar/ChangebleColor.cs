﻿using UnityEngine;

public abstract class ChangebleColor : MonoBehaviour
{
    public Color color;

    public abstract void ChangeColor(Color color);
}
