﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTop : ChangebleColor, IGenderObserver, ISwapable
{
    public UserDataModel userDataModel;

    public List<Sprite> topPieceMale;
    public List<Sprite> topPieceFemale;
    public bool isMale;

    List<TopCloth> topsMale;
    List<TopCloth> topsFemale;
    public int indexActivePiece;

    public SpriteRenderer topMale;
    public SpriteRenderer topFemale;
    public GameObject l_shortSleeve;
    public GameObject r_shortSleeve;
    public GameObject l_longSleeve;
    public GameObject r_longSleeve;

    public void UpdateGender()
    {
        this.isMale = userDataModel.GetUserGoals().isMale;
        ChangePiece();
    }

    void InitClothesLists()
    {
        topsMale = new List<TopCloth>();
        topsMale.Add( new TopCloth(topPieceMale[0], false, -1) ); //t-shirt male
        topsMale.Add( new TopCloth(topPieceMale[0], true, -1) ); //longsleeve male

        topsFemale = new List<TopCloth>();
        topsFemale.Add( new TopCloth(topPieceFemale[0], false, -1) ); //t-shirt female
        topsFemale.Add( new TopCloth(topPieceFemale[0], true, -1) ); //longsleeve female

    }

    public void ChangeStyle(int direction)
    {
        if (direction != 0)
        {
            if (isMale)
            {
                indexActivePiece = Math.Abs((indexActivePiece + direction)) % topsMale.Count;
            }
            else
            {
                indexActivePiece = Math.Abs((indexActivePiece + direction)) % topsFemale.Count;
            }
        }
        ChangePiece();
    }

    public void ChangePiece()
    {
        if (topsMale == null) InitClothesLists();
        AdaptVisualsToGender();
        List<TopCloth> tops = isMale ? topsMale : topsFemale;
        

        if (tops[indexActivePiece].longSleeves)
        {
            ActivateLongSleeves(true);
        }
        else
        {
            ActivateLongSleeves(false);
        }
    }

    private void AdaptVisualsToGender()
    {
        if (isMale)
        {
            topFemale.gameObject.SetActive(false);
            topMale.gameObject.SetActive(true);
            topMale.sprite = topsMale[indexActivePiece].topPiece;
        }
        else
        {
            topMale.gameObject.SetActive(false);
            topFemale.gameObject.SetActive(true);
            topFemale.sprite = topsFemale[indexActivePiece].topPiece;
        }
    }

    private void ActivateLongSleeves(bool activate)
    {
        l_shortSleeve.SetActive(!activate);
        r_shortSleeve.SetActive(!activate);
        l_longSleeve.SetActive(activate);
        r_longSleeve.SetActive(activate);
    }

    public override void ChangeColor(Color color)
    {
        this.color = color;
        topFemale.color = color;
        topMale.color = color;
        l_shortSleeve.GetComponent<SpriteRenderer>().color = color;
        r_shortSleeve.GetComponent<SpriteRenderer>().color = color;
        l_longSleeve.GetComponent<SpriteRenderer>().color = color;
        r_longSleeve.GetComponent<SpriteRenderer>().color = color;
    }
}
