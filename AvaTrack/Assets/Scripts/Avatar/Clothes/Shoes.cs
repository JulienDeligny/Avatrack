﻿using UnityEngine;
using System;
using UnityEngine.Experimental.U2D.Animation;

public class Shoes : ChangebleColor, ISwapable
{
    public int indexActivePiece;

    public int totalNumberOfSprites;
    public SpriteRenderer l_shoeRenderer;
    public SpriteRenderer r_shoeRenderer;
    public SpriteResolver l_shoeResolver;
    public SpriteResolver r_shoeResolver;

    public override void ChangeColor(Color color)
    {
        this.color = color;
        l_shoeRenderer.color = color;
        r_shoeRenderer.color = color;

    }

    public void ChangeStyle(int direction)
    {
        if (direction != 0) indexActivePiece = Math.Abs((indexActivePiece + direction) % totalNumberOfSprites);

        l_shoeResolver.SetCategoryAndLabel("Shoes", indexActivePiece.ToString());
        r_shoeResolver.SetCategoryAndLabel("Shoes", indexActivePiece.ToString());
    }
}
