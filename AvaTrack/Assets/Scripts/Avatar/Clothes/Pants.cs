﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Pants : ChangebleColor, ISwapable
{
    public Avatar avatar;
    public List<Sprite> pantLegs;

    public SpriteRenderer l_shortPantLeg;
    public SpriteRenderer r_shortPantLeg;

    public SpriteRenderer l_longPantLeg;
    public SpriteRenderer r_longPantLeg;

    public SpriteRenderer l_leg;
    public SpriteRenderer r_leg;

    public int indexActivePiece;
    List<PantCloth> pants;
    PantCloth activePiece;

    void Start()
    {
        InitList();
        indexActivePiece = 0;
        activePiece = pants[indexActivePiece];
    }

    void InitList()
    {
        pants = new List<PantCloth>();
        pants.Add(new PantCloth(true, pantLegs[0])); //long pants tucked
        pants.Add(new PantCloth(false, pantLegs[1])); //standard short pants
        pants.Add(new PantCloth(true, pantLegs[2])); //long pants #1
        pants.Add(new PantCloth(true, pantLegs[3])); //long pants #2
    }

    public override void ChangeColor(Color color)
    {
        if (activePiece != null)
        {
            if (pants == null) InitList();
            this.color = color;
            l_longPantLeg.color = color;
            r_longPantLeg.color = color;
            l_shortPantLeg.color = color;
            r_shortPantLeg.color = color;
        }
    }

    public void ChangeStyle(int direction)
    {
        if (pants == null) InitList();

        if (direction != 0)
        {
            indexActivePiece = direction == 1 ? (indexActivePiece + 1) % pants.Count : (indexActivePiece - 1) % pants.Count;
            indexActivePiece = indexActivePiece == -1 ? pants.Count - 1 : indexActivePiece;
        }

        activePiece = pants[indexActivePiece];

        if (activePiece.isLong)
        {
            l_longPantLeg.sprite = activePiece.pantLegSprite;
            r_longPantLeg.sprite = activePiece.pantLegSprite;

            l_shortPantLeg.gameObject.SetActive(false);
            r_shortPantLeg.gameObject.SetActive(false);
            l_longPantLeg.gameObject.SetActive(true);
            r_longPantLeg.gameObject.SetActive(true);
        } else
        {
            l_shortPantLeg.sprite = activePiece.pantLegSprite;
            r_shortPantLeg.sprite = activePiece.pantLegSprite;
            r_shortPantLeg.gameObject.SetActive(true);
            l_shortPantLeg.gameObject.SetActive(true);
            l_longPantLeg.gameObject.SetActive(false);
            r_longPantLeg.gameObject.SetActive(false);
        }
    }
}
