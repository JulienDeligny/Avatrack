﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantCloth
{
    public bool isLong;
    public Sprite pantLegSprite;
    public int indexInSpriteResolver;

    public PantCloth(bool isLong, Sprite pantLegSprite)
    {
        this.isLong = isLong;
        this.pantLegSprite = pantLegSprite;
    }
}
