﻿using UnityEngine;

public class TopCloth
{
    public Sprite topPiece;
    public bool longSleeves;
    public int longSleevesIndex;

    public TopCloth(Sprite topPiece, bool longSleeves, int longSleevesIndex) {
        this.topPiece = topPiece;
        this.longSleeves = longSleeves;
        this.longSleevesIndex = longSleevesIndex;
    }
}
