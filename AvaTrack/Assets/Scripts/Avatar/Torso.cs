﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;

public class Torso : MonoBehaviour, IGenderObserver
{
    public UserDataModel userDataModel;

    SpriteResolver resolver;

    public void UpdateGender()
    {
        if ( resolver == null ) resolver = this.GetComponent<SpriteResolver>();
        string gender = userDataModel.GetUserGoals().isMale ? "M" : "F";
        resolver.SetCategoryAndLabel("Torso", gender);
    }
}
