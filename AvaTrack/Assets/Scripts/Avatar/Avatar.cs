﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.U2D.Animation;

public class Avatar : MonoBehaviour, IGenderObserver
{
    public UserDataModel userDataModel;
    public Animator animator;
    public MonitorAvatarNutrition monitorClass;

    #region Body parts & Cloth
    //Changeable Color & Style
    public EyesController eyes;
    public CharacterTop top;
    public Hair hair;
    public Eyebrows eyebrows;
    public Pants pants;
    public Shoes shoes;

    //Changeable Shape
    public Nose nose;
    public Mouth mouth;
    public SpriteResolver torso;
    #endregion
    
    public GameObject[] genderObservers;

    public void UpdateGender()
    {
        foreach (GameObject observer in genderObservers)
        {
            observer.GetComponent<IGenderObserver>().UpdateGender();
        }
    }

    #region Avatar skin color

    public Color skinColor;
    public List<SpriteRenderer> bodyParts;

    public void ChangeSkinColor(Color skinColor)
    {
        this.skinColor = skinColor;
        foreach (SpriteRenderer elem in bodyParts)
        {
            elem.color = skinColor;
        }
    }
    #endregion

    #region Determine Avatar appearance & animations
    public bool isHungry;
    public bool isThirsty;
    public bool isFull;
    public bool isHot;
    public bool isUnbalanced;
    public bool challengesCompleted;

    public void PlayWinAnimation() 
    {
        monitorClass.UpdateWinScreen(true);
        animator.SetTrigger("playWinAnimation");
        mouth.UpdateCondition(true);
        eyes.UpdateCondition(true);
        ChangeWeight(1);
    }

    public void StopWinAnimation()
    {
        animator.SetTrigger("stopWinAnimation");
        monitorClass.UpdateWinScreen(false);
        UpdateCondition();
    }

    public void UpdateCondition()
    {
        ResetCondition();
        if (isHungry)
        {
            animator.SetBool("isHungry", true);
            animator.SetBool("isFull", false);
            ChangeWeight(0.88f);
        }
        else
        {
            animator.SetBool("isHungry", false);
            if (isFull)
            {
                animator.SetBool("isFull", true);
                ChangeWeight(1.12f);
            }
            else
            {
                animator.SetBool("isFull", false);
                ChangeWeight(1);
            }
        }

        if (isThirsty)
        {
            animator.SetBool("isThirsty", true);
        }
        else
        {
            animator.SetBool("isThirsty", false);
        }

        if (isUnbalanced)
        {
            animator.SetBool("isUnbalanced", true);
        }
        else
        {
            animator.SetBool("isUnbalanced", false);
        }

        if (!(isHungry || isThirsty || isFull || isUnbalanced))
        {
            mouth.UpdateCondition(true);
            eyes.UpdateCondition(true);
            ChangeWeight(1);
            if (challengesCompleted)
                animator.SetBool("challengesCompleted", true);
        }
        else
        {
            mouth.UpdateCondition(false);
            eyes.UpdateCondition(false);
        }
        if (isHot)
        {
            animator.SetBool("isHot", true);
        }
        else
        {
            animator.SetBool("isHot", false);
        }
    }

    public void ChangeWeight(float multiplier)
    {

        torso.gameObject.transform.localScale = new Vector3(multiplier, 1, 1);
        //not all children of torso should change their scale when torso scale is changed
        float compensationMultiplier = 1 - (multiplier - 1);
        bodyParts[0].gameObject.transform.localScale = new Vector3(compensationMultiplier, 1, 1); //head
        bodyParts[1].gameObject.transform.localScale = new Vector3(compensationMultiplier, 1, 1); //left arm
        bodyParts[2].gameObject.transform.localScale = new Vector3(compensationMultiplier, 1, 1); //right arm
        shoes.l_shoeRenderer.gameObject.transform.localScale = new Vector3(compensationMultiplier, 1, 1); //left shoe
        shoes.r_shoeRenderer.gameObject.transform.localScale = new Vector3(compensationMultiplier, 1, 1); //right shoe
    }

    void ResetCondition()
    {
        animator.SetBool("isHungry", false);
        animator.SetBool("isThirsty", false);
        animator.SetBool("isFull", false);
        animator.SetBool("isUnbalanced", false);
    }
    #endregion

    #region Avatar position
    private float posX;
    private float posY;
    private float scaleX;
    private float scaleY;

    void Start()
    {
        posX = torso.gameObject.transform.localPosition.x;
        posY = torso.gameObject.transform.localPosition.y;
        scaleX = this.gameObject.transform.localScale.x;
        scaleY = this.gameObject.transform.localScale.y;
    }

    public void ZoomIn()
    {
        LeanTween.moveLocalX(torso.gameObject, 0.2f, 0.3f);
        LeanTween.moveLocalY(torso.gameObject, -2, 0.3f);
        this.transform.LeanScaleX(0.9f, 0.3f);
        this.transform.LeanScaleY(0.9f, 0.3f);
    }

    public void ZoomOut()
    {
        LeanTween.moveLocalX(torso.gameObject, posX, 0.3f);
        LeanTween.moveLocalY(torso.gameObject, posY, 0.3f);
        this.transform.LeanScaleX(scaleX, 0.3f);
        this.transform.LeanScaleY(scaleY, 0.3f);
    }

    public void FaceCamera()
    {
        ResetCondition();
    }

    #endregion

    #region Save & Load Avatar customization

    public void SaveAvatar()
    {
        AvatarStorable avatar = new AvatarStorable();

        //hair
        avatar.hairStyle = hair.indexActiveHairStyle;
        avatar.hairCol = ConvertColorToArray(hair.color);
        //eyes
        avatar.eyeStyle = eyes.indexActiveSprite;
        avatar.eyeCol = ConvertColorToArray(eyes.color);
        //nose
        avatar.noseStyle = nose.indexActivePiece;
        //eyebrows
        avatar.eyebrowStyle = eyebrows.indexActivePiece;
        //mouthstyle
        avatar.mouthStyle = mouth.indexActiveSprite;
        //top
        avatar.topStyle = top.indexActivePiece;
        avatar.topCol = ConvertColorToArray(top.color);
        //pants
        avatar.pantsStyle = pants.indexActivePiece;
        avatar.pantsCol = ConvertColorToArray(pants.color);
        //shoes
        avatar.shoeStyle = shoes.indexActivePiece;
        avatar.shoeCol = ConvertColorToArray(shoes.color);
        //skinColor
        avatar.skinCol = ConvertColorToArray(skinColor);

        userDataModel.SetAvatar(avatar);
    }

    public void LoadAvatar(AvatarStorable avatar)
    {
        //skinColor
        ChangeSkinColor(ConvertArrayToColor(avatar.skinCol));
        //hair
        hair.indexActiveHairStyle = avatar.hairStyle;
        hair.ChangeStyle(0);
        hair.ChangeColor(ConvertArrayToColor(avatar.hairCol));
        //eyes
        eyes.indexActiveSprite = avatar.eyeStyle;
        eyes.ChangeStyle(0);
        eyes.ChangeColor(ConvertArrayToColor(avatar.eyeCol));
        //nose
        nose.indexActivePiece = avatar.noseStyle;
        nose.ChangeStyle(0);
        //mouth
        mouth.indexActiveSprite = avatar.mouthStyle;
        nose.ChangeStyle(0);
        //eyebrows
        eyebrows.indexActivePiece = avatar.eyebrowStyle;
        eyebrows.ChangeStyle(0);
        //top
        top.indexActivePiece = avatar.topStyle;
        top.ChangeStyle(0);
        top.ChangeColor(ConvertArrayToColor(avatar.topCol));
        //pants
        pants.indexActivePiece = avatar.pantsStyle;
        pants.ChangeStyle(0);
        pants.ChangeColor(ConvertArrayToColor(avatar.pantsCol));
        //shoes
        shoes.indexActivePiece = avatar.shoeStyle;
        shoes.ChangeStyle(0);
        shoes.ChangeColor(ConvertArrayToColor(avatar.shoeCol));

    }

    float[] ConvertColorToArray(Color color)
    {
        float[] colorRGBT = new float[] { color.r, color.g, color.b, 1 };
        return colorRGBT;
    }

    Color ConvertArrayToColor(float[] colorRGBT)
    {
        Color color = new Color();
        color.r = colorRGBT[0];
        color.g = colorRGBT[1];
        color.b = colorRGBT[2];
        color.a = colorRGBT[3];
        return color;
    }

    #endregion
    
}