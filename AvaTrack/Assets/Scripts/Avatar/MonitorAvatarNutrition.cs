﻿using UnityEngine;

public class MonitorAvatarNutrition : MonoBehaviour, IMacrosObserver, IHydrationLevelObserver, IMealTimeObserver
{
    private static int LIMIT_FOR_BEING_HOT = 30;

    public UserDataModel userDataModel;
    public Avatar avatar;
    public int hour;

    private bool winScreenPlaying;

    #region UpdateFunctions

    public void UpdateWinScreen(bool winScreenPlaying)
    {
        this.winScreenPlaying = winScreenPlaying;
    }

    public void UpdateMacroValues()
    {
        DetermineCondition();
    }

    public void UpdateHydrationLevel()
    {
        DetermineCondition();
    }

    public void UpdateMealTime()
    {
        DetermineCondition();
    }
    #endregion

    public void DetermineCondition()
    {
        hour = System.DateTime.UtcNow.ToLocalTime().Hour;

        DetermineSatiationLevel();
        DetermineIfUnbalanced();
        DetermineIfChallengesCompleted();
        DetermineIfThirsty();

        avatar.isHot = DetermineIfHot();

        if (!winScreenPlaying) 
            avatar.UpdateCondition();
    }

    private bool DetermineIfHot()
    {
        return userDataModel.GetTemperature() >= LIMIT_FOR_BEING_HOT ? true : false;
    }

    private void DetermineSatiationLevel()
    {
        float consumedCalories = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.calories);
        int calorieGoal = userDataModel.GetUserGoals().CheckCurrentCalorieGoalReached(consumedCalories);

        float currenHydrationLevel = userDataModel.GetHydrationLevel().currentHydrationLevel;
        int waterConsumptionIndex = userDataModel.GetUserGoals().CheckCurrentWaterGoal(currenHydrationLevel);
        if (calorieGoal == -1)
        {
            avatar.isHungry = true;
            avatar.isFull = false;
        }
        else if (calorieGoal == 1)
        {
            avatar.isFull = true;
            avatar.isHungry = false;
        }
        else
        {
            avatar.isHungry = false;
            avatar.isFull = false;
        }
        if (waterConsumptionIndex > 0)
        {
            avatar.isFull = true;
        }
        else 
        {
            avatar.isFull = false;
        }
    }

    private void DetermineIfUnbalanced()
    {
        float consumedCarbohydrates = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.carbohydrates);
        int carbohydratesGoal = userDataModel.GetUserGoals().CheckCurrentMacroGoalReached(consumedCarbohydrates, Macronutrient.carbohydrates);

        float consumedProtein = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.protein);
        int proteinGoal = userDataModel.GetUserGoals().CheckCurrentMacroGoalReached(consumedProtein, Macronutrient.protein);

        float consumedFat = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.fat);
        int fatGoal = userDataModel.GetUserGoals().CheckCurrentMacroGoalReached(consumedFat, Macronutrient.fat);

        if (carbohydratesGoal == 0 && proteinGoal == 0 && fatGoal == 0)
        {
            avatar.isUnbalanced = false;
        } else
        {
            avatar.isUnbalanced = true;
        }
    }

    private void DetermineIfChallengesCompleted()
    {
        float consumedCalories = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.calories);
        bool dailyCalorieGoal = userDataModel.GetUserGoals().CheckDailyCalorieGoalReached(consumedCalories);

        float consumedCarbohydrates = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.carbohydrates);
        bool dailyCarbohydratesGoal = userDataModel.GetUserGoals().CheckDailyMacroGoalReached(consumedCarbohydrates, Macronutrient.carbohydrates);

        float consumedProtein = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.protein);
        bool dailyProteinGoal = userDataModel.GetUserGoals().CheckDailyMacroGoalReached(consumedProtein, Macronutrient.protein);

        float consumedFat = userDataModel.GetConsumedFood().GetConsumed(NutritionalValueType.fat);
        bool dailyFatGoal = userDataModel.GetUserGoals().CheckDailyMacroGoalReached(consumedFat, Macronutrient.fat);

        if (dailyCalorieGoal && dailyCarbohydratesGoal && dailyProteinGoal && dailyFatGoal)
            avatar.challengesCompleted = true;
        else
            avatar.challengesCompleted = false;
    }

    private void DetermineIfThirsty()
    {
        float currenHydrationLevel = userDataModel.GetHydrationLevel().currentHydrationLevel;

        int index = userDataModel.GetUserGoals().CheckCurrentWaterGoal(currenHydrationLevel);

        if (index < 0)
            avatar.isThirsty = true;
        else
            avatar.isThirsty = false;
    }

}
